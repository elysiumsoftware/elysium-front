var gulp = require('gulp');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var del = require('del');
var connect = require('gulp-connect');
var awspublish = require('gulp-awspublish');
var realFavicon = require ('gulp-real-favicon');
var fs = require('fs');
var gutil = require('gulp-util');
var gtm  = require('gulp-gtm');
var replace = require('gulp-replace');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var filter = require('gulp-filter');
var gitBranch = require('git-branch');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var FAVICON_DATA_FILE = 'faviconData.json';
var localConfig = {
    branch: gitBranch.sync(),
    distDir: (function(){
        console.log('Initializing build on branch \'' + gitBranch.sync() + '\'.');
        switch(gitBranch.sync()){
            case 'master':
                return 'dist';
            default:
                return 'dist-' + gitBranch.sync();
        }
    })(),
    sassFiles: 'src/sass/**/*.scss',
    getConfigurations: function (branch){
        var configurations = require('./config/config');
        
        if(!configurations[branch])
            throw new Error('The current branch \'' + localConfig.branch + '\' doesn\'t have valid keys set on the config.js file.');
        
        return configurations[branch];
    }
};

// Gera todos os favicons a partir do src/images/favicon/logo.png
// Use manualmente sempre que o logo.png for atualizado
gulp.task('generate-favicons', function(done) {
	realFavicon.generateFavicon({
		masterPicture: 'src/images/favicon/logo.png',
		dest: 'src/images/favicon',
		iconsPath: '/images/favicon',
		design: {
			ios: {
				pictureAspect: 'backgroundAndMargin',
				backgroundColor: '#ffffff',
				margin: '35%',
				appName: 'Elysium'
			},
			desktopBrowser: {},
			windows: {
				pictureAspect: 'noChange',
				backgroundColor: '#ffffff',
				onConflict: 'override',
				appName: 'Elysium'
			},
			androidChrome: {
				pictureAspect: 'backgroundAndMargin',
				margin: '31%',
				backgroundColor: '#ffffff',
				themeColor: '#ffffff',
				manifest: {
					name: 'Elysium',
					startUrl: 'http://app.elysium.net.br/',
					display: 'standalone',
					orientation: 'notSet',
					onConflict: 'override',
					declared: true
				}
			},
			safariPinnedTab: {
				pictureAspect: 'silhouette',
				themeColor: '#41c970'
			}
		},
		settings: {
			compression: 5,
			scalingAlgorithm: 'Lanczos',
			errorOnImageTooSmall: false
		},
		markupFile: FAVICON_DATA_FILE
	}, function() {
		done();
	});
});

// Verifica se os favicons gerados automaticamente estão atualizados
//      de acordo com o http://realfavicongenerator.net/
gulp.task('check-favicons', function() {
	var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
	realFavicon.checkForUpdates(currentVersion, function(err) {
		if (err) {
			throw err;
		}
	});
});

gulp.task('clean', function(){
    return del(localConfig.distDir + '/**');
});

gulp.task('sassify', function(){
    return gulp.src(localConfig.sassFiles)
               .pipe(sourcemaps.init())
               .pipe(sass().on('error', sass.logError))
               .pipe(sourcemaps.write(''))
               .pipe(gulp.dest('src/sass/'));
});

gulp.task('minify', ['clean', 'sassify'], function(){
    var indexHtmlFilter = filter(['**/*', '!**/index.html'], { restore: true });

    var ApiUrl = (function(){
        var configurations = localConfig.getConfigurations(localConfig.branch);

        if(!configurations.apiUrl){
            console.log('The current git branch has no API URL attached to it on the config file. Using development API.');
            return 'https://dev.api.elysium.net.br/';
        } else return configurations.apiUrl;
    })();

    var VindiApiUrl = (function(){
        var configurations = localConfig.getConfigurations(localConfig.branch);

        if(!configurations.vindi || !configurations.vindi.publicApiKey){
            console.log('The current git branch has no Vindi API URL attached to it on the config file. Public Vindi calls won\'t work.');
            return '';
        } else return configurations.vindi.publicApiKey;
    })();

    var RavenCode = (function(){
        var configurations = localConfig.getConfigurations(localConfig.branch);

        if(!configurations.raven || !configurations.raven.environment || !configurations.raven.id){
            console.log('The current git branch has no valid Sentry configuration attached to it on the config file. Sentry won\'t be installed.');
            return '';
        }

        // noinspection JSAnnotator
        return `Raven.config('`+ configurations.raven.id + `',{environment:'` + configurations.raven.environment + `'}).addPlugin(Raven.Plugins.Angular).install()`;
    })();
    
    return gulp.src('src/index.html')
        .pipe(useref().on('error', gutil.log))
        .pipe(gulpIf('*.js', uglify()))
        .pipe(replace(/Raven\.config\(".*?"?\).*?\.install\(\)/, RavenCode))
        .pipe(replace(/elysium.constant\(['"]APIURL['"],(\s)*['"](.*?)['"]\)/, 'elysium.constant("APIURL","'+ ApiUrl + '")'))
        .pipe(replace(/elysium.constant\(['"]VINDI_KEY['"],(\s)*['"](.*?)['"]\)/, 'elysium.constant("VINDI_KEY","'+ VindiApiUrl + '")'))
        .pipe(indexHtmlFilter)
        .pipe(rev())
        .pipe(indexHtmlFilter.restore)
        .pipe(revReplace())
        .pipe(gulpIf('*.css', cssnano({ zindex: false })))
        .pipe(gulp.dest(localConfig.distDir));
});

gulp.task('insert-favicons', ['minify'], function() {
	return gulp.src([localConfig.distDir + '/index.html'])
		.pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
		.pipe(gulp.dest(localConfig.distDir));
});

gulp.task('insert-tag-manager', ['insert-favicons'], function(){
    var GTMCode = (function(){
        var configurations = localConfig.getConfigurations(localConfig.branch);

        if(!configurations.gtmCode){
            console.log('The current git branch has no Google Tag Manager code attached to it on the config file. The Tag Manager won\'t be inserted.');
        } else return configurations.gtmCode;
    })();
    
    if(GTMCode && GTMCode.length){
        gulp.src(localConfig.distDir + '/index.html')
            .pipe(gtm({containerId: GTMCode, tag: 'footer'}))
            .pipe(gulp.dest(localConfig.distDir));
    }
});

gulp.task('build', ['insert-tag-manager'], function(){
    return gulp.src([
        'src/templates/**',
		'src/app/**/*.html',
        'src/images/**',
        'src/fonts/**',
        'src/favicon.ico',
        'src/maintenance.html',
        '!src/images/_psd/**',
        '!src/images/_old/**',
        '!src/images/favicon/logo.psd',
        '!src/images/favicon/logo.png'
    ],{base:'src'})
    .pipe(gulp.dest(localConfig.distDir));
});

gulp.task('deploy', ['build'], function() {
    var configurations = localConfig.getConfigurations(localConfig.branch);
    var publisher = awspublish.create(configurations.awsKeys);
    return gulp.src(localConfig.distDir + '/**/*')
        .pipe(awspublish.gzip({ ext: '' }))
        .pipe(publisher.publish(configurations.awsHeaders))
        .pipe(publisher.cache())
        .pipe(publisher.sync())
        .pipe(awspublish.reporter());
});

gulp.task('deploy-as-is', function() {
    var configurations = localConfig.getConfigurations(localConfig.branch);
    var publisher = awspublish.create(configurations.awsKeys);
    return gulp.src(localConfig.localConfig.distDir + '/**/*')
        .pipe(awspublish.gzip({ ext: '' }))
        .pipe(publisher.publish(configurations.awsHeaders))
        .pipe(publisher.cache())
        .pipe(publisher.sync())
        .pipe(awspublish.reporter());
});

gulp.task('serve-dist', function(){
    connect.server({
        root: localConfig.distDir,
        port: 4000,
        fallback: localConfig.distDir + '/index.html'
    });
});

gulp.task('serve', ['sassify'], function(){
    gulp.watch(localConfig.sassFiles, ['sassify']);
    connect.server({
        root: 'src',
        port: 3000,
        fallback: 'src/index.html'
    });
});

gulp.task('default', ['serve']);
