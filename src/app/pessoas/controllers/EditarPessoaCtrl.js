elysium.controller('EditarPessoasCtrl', ['ElysiumAPI', '$scope', '$state', '$stateParams', 'ngToast', function(ElysiumAPI, $scope, $state, $stateParams, ngToast){
	var _this = this;

	// $scope.carregarPessoa = function(subtipo){
	// 	switch (subtipo) {
	// 		case 'cliente':
	// 			ElysiumAPI.get("clientes/"+$stateParams.id, 'pessoas').then(function(response){
	// 				_this.dados = response.data;
	// 				_this.dados.site = _this.dados.site.replace("http://", "");
	// 			},function(response){
	// 				ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
	// 			});
	// 			break;
	// 		case 'adverso':
	// 			ElysiumAPI.get("adversos/"+$stateParams.id, 'pessoas').then(function(response){
	// 				_this.dados = response.data;
	// 				_this.dados.site = _this.dados.site.replace("http://", "");
	// 			},function(response){
	// 				ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
	// 			});
	// 			break;
	// 		case 'advogado_adverso':
	// 			ElysiumAPI.get("advogadosadversos/"+$stateParams.id, 'pessoas').then(function(response){
	// 				_this.dados = response.data;
	// 			},function(response){
	// 				ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
	// 			});
	// 			break;
	// 		case 'correspondente':
	// 			ElysiumAPI.get("correspondentes/"+$stateParams.id, 'pessoas').then(function(response){
	// 				_this.dados = response.data;
	// 			},function(response){
	// 				ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
	// 			});
	// 			break;
	// 		default:
	// 		ElysiumAPI.showError('Alguma coisa deu errado ao chegar aqui!<br>Voltar e tentar novamente pode resolver', response);
	// 	};
	// };

	if($stateParams.type){
	    _this.type = $stateParams.type;
		if($stateParams.type == 'Client'){
            ElysiumAPI.get("clients/" + $stateParams.id, 'person').then(function(response){
                _this.dados = {
                    id: response.data.id,
                    nome: response.data.name,
                    telefone: response.data.telephone,
                    email: response.data.email,
                    site: response.data.site,
                    rua: response.data.street,
                    numero: response.data.number,
                    complemento: response.data.complement,
                    bairro: response.data.neighbourhood,
                    cidade: response.data.city,
                    estado: response.data.state,
                    pais: response.data.country,
                    cep: response.data.cep,
                    comentarios: response.data.comments
                };
                _this.dados.site = _this.dados.site.replace("http://", "");
            },function(response){
                ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
            });
		} else if($stateParams.type == 'Adverse'){
            ElysiumAPI.get("adverses/" + $stateParams.id, 'person').then(function(response){
                _this.dados = {
                    id: response.data.id,
                    nome: response.data.name,
                    telefone: response.data.telephone,
                    email: response.data.email,
                    site: response.data.site,
                    rua: response.data.street,
                    numero: response.data.number,
                    complemento: response.data.complement,
                    bairro: response.data.neighbourhood,
                    cidade: response.data.city,
                    estado: response.data.state,
                    pais: response.data.country,
                    cep: response.data.cep,
                    comentarios: response.data.comments
                };
                _this.dados.site = _this.dados.site.replace("http://", "");
            },function(response){
                ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
            });
        } else if($stateParams.type == 'Adverse Lawyer'){
            ElysiumAPI.get("adverse_lawyers/" + $stateParams.id, 'person').then(function(response){
                _this.dados = {
                    id: response.data.id,
                    nome: response.data.name,
                    telefone: response.data.telephone,
                    email: response.data.email,
                    numero_da_oab: response.data.oab,
                    nome_do_escritorio: response.data.office_name,
                    comentarios: response.data.comments
                };
            },function(response){
                ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
            });
        } else if($stateParams.type == 'Correspondent'){
            ElysiumAPI.get("correspondents/" + $stateParams.id, 'person').then(function(response){
                _this.dados = {
                    id: response.data.id,
                    cargo: response.data.role,
                    cnpj: response.data.cnpj,
                    nome_fantasia: response.data.fantasy_name,
                    nome: response.data.name,
                    telefone: response.data.telephone,
                    email: response.data.email,
                    site: response.data.site,
                    rua: response.data.street,
                    numero: response.data.number,
                    complemento: response.data.complement,
                    bairro: response.data.neighbourhood,
                    cidade: response.data.city,
                    estado: response.data.state,
                    pais: response.data.country,
                    cep: response.data.cep,
                    comentarios: response.data.comments
                };
                _this.dados.site = _this.dados.site.replace("http://", "");
            },function(response){
                ElysiumAPI.showError('Algo deu errado ao visualizar essa pessoa', response);
            });
        }
	} else{
		$state.go('app.pessoas.todas');
	}

	_this.estados = [{valor:"AC",nome:"Acre"},{valor:"AL",nome:"Alagoas"},{valor:"AM",nome:"Amazonas"},{valor:"AP",nome:"Amapá"},{valor:"BA",nome:"Bahia"},{valor:"CE",nome:"Ceará"},{valor:"DF",nome:"Distrito Federal"},{valor:"ES",nome:"Espírito Santo"},{valor:"GO",nome:"Goiás"},{valor:"MA",nome:"Maranhão"},{valor:"MT",nome:"Mato Grosso"},{valor:"MS",nome:"Mato Grosso do Sul"},{valor:"MG",nome:"Minas Gerais"},{valor:"PA",nome:"Pará"},{valor:"PB",nome:"Paraíba"},{valor:"PR",nome:"Paraná"},{valor:"PI",nome:"Piauí"},{valor:"RJ",nome:"Rio de Janeiro"},{valor:"RN",nome:"Rio Grande do Norte"},{valor:"RO",nome:"Rondônia"},{valor:"RS",nome:"Rio Grande do Sul"},{valor:"RR",nome:"Roraima"},{valor:"SC",nome:"Santa Catarina"},{valor:"SE",nome:"Sergipe"},{valor:"SP",nome:"São Paulo"},{valor:"TO",nome:"Tocantins"}];
	_this.estadosConfig = {
		valueField: 'valor',
		highlight: false,
		labelField: 'nome',
		searchField: ['nome'],
		maxItems: 1
	};
	_this.paises = [{valor:"África do Sul"},{valor:"Albânia"},{valor:"Alemanha"},{valor:"Andorra"},{valor:"Angola"},{valor:"Anguilla"},{valor:"Antigua"},{valor:"Arábia Saudita"},{valor:"Argentina"},{valor:"Armênia"},{valor:"Aruba"},{valor:"Austrália"},{valor:"Áustria"},{valor:"Azerbaijão"},{valor:"Bahamas"},{valor:"Bahrein"},{valor:"Bangladesh"},{valor:"Barbados"},{valor:"Bélgica"},{valor:"Benin"},{valor:"Bermudas"},{valor:"Botsuana"},{valor:"Brasil"},{valor:"Brunei"},{valor:"Bulgária"},{valor:"Burkina Fasso"},{valor:"botão"},{valor:"Cabo Verde"},{valor:"Camarões"},{valor:"Camboja"},{valor:"Canadá"},{valor:"Cazaquistão"},{valor:"Chade"},{valor:"Chile"},{valor:"China"},{valor:"Cidade do Vaticano"},{valor:"Colômbia"},{valor:"Congo"},{valor:"Coréia do Sul"},{valor:"Costa do Marfim"},{valor:"Costa Rica"},{valor:"Croácia"},{valor:"Dinamarca"},{valor:"Djibuti"},{valor:"Dominica"},{valor:"EUA"},{valor:"Egito"},{valor:"El Salvador"},{valor:"Emirados Árabes"},{valor:"Equador"},{valor:"Eritréia"},{valor:"Escócia"},{valor:"Eslováquia"},{valor:"Eslovênia"},{valor:"Espanha"},{valor:"Estônia"},{valor:"Etiópia"},{valor:"Fiji"},{valor:"Filipinas"},{valor:"Finlândia"},{valor:"França"},{valor:"Gabão"},{valor:"Gâmbia"},{valor:"Gana"},{valor:"Geórgia"},{valor:"Gibraltar"},{valor:"Granada"},{valor:"Grécia"},{valor:"Guadalupe"},{valor:"Guam"},{valor:"Guatemala"},{valor:"Guiana"},{valor:"Guiana Francesa"},{valor:"Guiné-bissau"},{valor:"Haiti"},{valor:"Holanda"},{valor:"Honduras"},{valor:"Hong Kong"},{valor:"Hungria"},{valor:"Iêmen"},{valor:"Ilhas Cayman"},{valor:"Ilhas Cook"},{valor:"Ilhas Curaçao"},{valor:"Ilhas Marshall"},{valor:"Ilhas Turks & Caicos"},{valor:"Ilhas Virgens (brit.)"},{valor:"Ilhas Virgens(amer.)"},{valor:"Ilhas Wallis e Futuna"},{valor:"Índia"},{valor:"Indonésia"},{valor:"Inglaterra"},{valor:"Irlanda"},{valor:"Islândia"},{valor:"Israel"},{valor:"Itália"},{valor:"Jamaica"},{valor:"Japão"},{valor:"Jordânia"},{valor:"Kuwait"},{valor:"Latvia"},{valor:"Líbano"},{valor:"Liechtenstein"},{valor:"Lituânia"},{valor:"Luxemburgo"},{valor:"Macau"},{valor:"Macedônia"},{valor:"Madagascar"},{valor:"Malásia"},{valor:"Malaui"},{valor:"Mali"},{valor:"Malta"},{valor:"Marrocos"},{valor:"Martinica"},{valor:"Mauritânia"},{valor:"Mauritius"},{valor:"México"},{valor:"Moldova"},{valor:"Mônaco"},{valor:"Montserrat"},{valor:"Nepal"},{valor:"Nicarágua"},{valor:"Niger"},{valor:"Nigéria"},{valor:"Noruega"},{valor:"Nova Caledônia"},{valor:"Nova Zelândia"},{valor:"Omã"},{valor:"Palau"},{valor:"Panamá"},{valor:"Papua-nova Guiné"},{valor:"Paquistão"},{valor:"Peru"},{valor:"Polinésia Francesa"},{valor:"Polônia"},{valor:"Porto Rico"},{valor:"Portugal"},{valor:"Qatar"},{valor:"Quênia"},{valor:"Rep. Dominicana"},{valor:"Rep. Tcheca"},{valor:"Reunion"},{valor:"Romênia"},{valor:"Ruanda"},{valor:"Rússia"},{valor:"Saipan"},{valor:"Samoa Americana"},{valor:"Senegal"},{valor:"Serra Leone"},{valor:"Seychelles"},{valor:"Singapura"},{valor:"Síria"},{valor:"Sri Lanka"},{valor:"St. Kitts & Nevis"},{valor:"St. Lúcia"},{valor:"St. Vincent"},{valor:"Sudão"},{valor:"Suécia"},{valor:"Suiça"},{valor:"Suriname"},{valor:"Tailândia"},{valor:"Taiwan"},{valor:"Tanzânia"},{valor:"Togo"},{valor:"Trinidad & Tobago"},{valor:"Tunísia"},{valor:"Turquia"},{valor:"Ucrânia"},{valor:"Uganda"},{valor:"Uruguai"},{valor:"Venezuela"},{valor:"Vietnã"},{valor:"Zaire"},{valor:"Zâmbia"},{valor:"Zimbábue"}];
	_this.paisesConfig = {
		valueField: 'valor',
		labelField: 'valor',
		highlight: false,
		searchField: ['valor'],
		maxItems: 1
	};

	$scope.editarCliente = function(id){
		var dados = {
			name: _this.dados.nome,
			telephone: _this.dados.telefone,
			email: _this.dados.email,
			site: _this.dados.site,
			street: _this.dados.rua,
			number: _this.dados.numero,
			complement: _this.dados.complemento,
			neighbourhood: _this.dados.bairro,
			city: _this.dados.cidade,
			state: _this.dados.estado,
			country: _this.dados.pais,
			cep: _this.dados.cep,
			comments: _this.dados.comentarios
		};
		ElysiumAPI.patch("clients/" +id, 'person', dados).then(function(response){
			$state.go('app.pessoas.detalhes', {id: response.data.id, type: 'Client'});
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao editar essa pessoa', response);
		});
	};
	$scope.editarAdverso = function(id){
		var dados = {
			name: _this.dados.nome,
			telephone: _this.dados.telefone,
			email: _this.dados.email,
			site: _this.dados.site,
			street: _this.dados.rua,
			number: _this.dados.numero,
			complement: _this.dados.complemento,
			neighbourhood: _this.dados.bairro,
			city: _this.dados.cidade,
			state: _this.dados.estado,
			country: _this.dados.pais,
			cep: _this.dados.cep,
			comments: _this.dados.comentarios
        };
		ElysiumAPI.patch("adverses/" + id, 'person', dados).then(function(response){
			$state.go('app.pessoas.detalhes', {id: response.data.id, type: 'Adverse'});
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao editar essa pessoa', response);
		});
	};
	$scope.editarAdvogado = function(id){
		var dados = {
			name: _this.dados.nome,
			telephone: _this.dados.telefone,
			email: _this.dados.email,
			oab: _this.dados.numero_da_oab,
			office_name: _this.dados.nome_do_escritorio,
			comments: _this.dados.comentarios
        };
		ElysiumAPI.patch("adverse_lawyers/" + id, 'person', dados).then(function(response){
			$state.go('app.pessoas.detalhes', {id: response.data.id, type: 'Adverse Lawyer'});
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao editar essa pessoa', response);
		});
	};
	$scope.editarCorrespondente = function(id){
		var dados = {
			name: _this.dados.nome,
			fantasy_name: _this.dados.nomefantasia,
			role: _this.dados.cargo,
			cnpj: _this.dados.cnpj,
			telephone: _this.dados.telefone,
			email: _this.dados.email,
			site: _this.dados.site,
			street: _this.dados.rua,
			number: _this.dados.numero,
			complement: _this.dados.complemento,
			neighbourhood: _this.dados.bairro,
			city: _this.dados.cidade,
			state: _this.dados.estado,
			country: _this.dados.pais,
			cep: _this.dados.cep,
			comments: _this.dados.comentarios
		};
		ElysiumAPI.patch("correspondents/" + id, 'person', dados).then(function(response){
			$state.go('app.pessoas.detalhes', {id: response.data.id, type: 'Correspondent'});
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao editar essa pessoa', response);
		});
	};
}]);
