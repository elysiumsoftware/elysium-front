elysium.controller('NovaPessoaModalCtrl', ['ElysiumAPI', '$scope', '$state', 'ClienteFactory', 'AdversoFactory', 'AdvogadoAdversoFactory', function(ElysiumAPI, $scope, $state, ClienteFactory, AdversoFactory, AdvogadoAdversoFactory){
	var _this = this;

	_this.estados = [{valor:"AC",nome:"Acre"},{valor:"AL",nome:"Alagoas"},{valor:"AM",nome:"Amazonas"},{valor:"AP",nome:"Amapá"},{valor:"BA",nome:"Bahia"},{valor:"CE",nome:"Ceará"},{valor:"DF",nome:"Distrito Federal"},{valor:"ES",nome:"Espírito Santo"},{valor:"GO",nome:"Goiás"},{valor:"MA",nome:"Maranhão"},{valor:"MT",nome:"Mato Grosso"},{valor:"MS",nome:"Mato Grosso do Sul"},{valor:"MG",nome:"Minas Gerais"},{valor:"PA",nome:"Pará"},{valor:"PB",nome:"Paraíba"},{valor:"PR",nome:"Paraná"},{valor:"PI",nome:"Piauí"},{valor:"RJ",nome:"Rio de Janeiro"},{valor:"RN",nome:"Rio Grande do Norte"},{valor:"RO",nome:"Rondônia"},{valor:"RS",nome:"Rio Grande do Sul"},{valor:"RR",nome:"Roraima"},{valor:"SC",nome:"Santa Catarina"},{valor:"SE",nome:"Sergipe"},{valor:"SP",nome:"São Paulo"},{valor:"TO",nome:"Tocantins"}];
	_this.estadosConfig = {
		valueField: 'valor',
		highlight: false,
		labelField: 'nome',
		searchField: ['nome'],
		maxItems: 1
	};
	_this.paises = [{valor:"África do Sul"},{valor:"Albânia"},{valor:"Alemanha"},{valor:"Andorra"},{valor:"Angola"},{valor:"Anguilla"},{valor:"Antigua"},{valor:"Arábia Saudita"},{valor:"Argentina"},{valor:"Armênia"},{valor:"Aruba"},{valor:"Austrália"},{valor:"Áustria"},{valor:"Azerbaijão"},{valor:"Bahamas"},{valor:"Bahrein"},{valor:"Bangladesh"},{valor:"Barbados"},{valor:"Bélgica"},{valor:"Benin"},{valor:"Bermudas"},{valor:"Botsuana"},{valor:"Brasil"},{valor:"Brunei"},{valor:"Bulgária"},{valor:"Burkina Fasso"},{valor:"botão"},{valor:"Cabo Verde"},{valor:"Camarões"},{valor:"Camboja"},{valor:"Canadá"},{valor:"Cazaquistão"},{valor:"Chade"},{valor:"Chile"},{valor:"China"},{valor:"Cidade do Vaticano"},{valor:"Colômbia"},{valor:"Congo"},{valor:"Coréia do Sul"},{valor:"Costa do Marfim"},{valor:"Costa Rica"},{valor:"Croácia"},{valor:"Dinamarca"},{valor:"Djibuti"},{valor:"Dominica"},{valor:"EUA"},{valor:"Egito"},{valor:"El Salvador"},{valor:"Emirados Árabes"},{valor:"Equador"},{valor:"Eritréia"},{valor:"Escócia"},{valor:"Eslováquia"},{valor:"Eslovênia"},{valor:"Espanha"},{valor:"Estônia"},{valor:"Etiópia"},{valor:"Fiji"},{valor:"Filipinas"},{valor:"Finlândia"},{valor:"França"},{valor:"Gabão"},{valor:"Gâmbia"},{valor:"Gana"},{valor:"Geórgia"},{valor:"Gibraltar"},{valor:"Granada"},{valor:"Grécia"},{valor:"Guadalupe"},{valor:"Guam"},{valor:"Guatemala"},{valor:"Guiana"},{valor:"Guiana Francesa"},{valor:"Guiné-bissau"},{valor:"Haiti"},{valor:"Holanda"},{valor:"Honduras"},{valor:"Hong Kong"},{valor:"Hungria"},{valor:"Iêmen"},{valor:"Ilhas Cayman"},{valor:"Ilhas Cook"},{valor:"Ilhas Curaçao"},{valor:"Ilhas Marshall"},{valor:"Ilhas Turks & Caicos"},{valor:"Ilhas Virgens (brit.)"},{valor:"Ilhas Virgens(amer.)"},{valor:"Ilhas Wallis e Futuna"},{valor:"Índia"},{valor:"Indonésia"},{valor:"Inglaterra"},{valor:"Irlanda"},{valor:"Islândia"},{valor:"Israel"},{valor:"Itália"},{valor:"Jamaica"},{valor:"Japão"},{valor:"Jordânia"},{valor:"Kuwait"},{valor:"Latvia"},{valor:"Líbano"},{valor:"Liechtenstein"},{valor:"Lituânia"},{valor:"Luxemburgo"},{valor:"Macau"},{valor:"Macedônia"},{valor:"Madagascar"},{valor:"Malásia"},{valor:"Malaui"},{valor:"Mali"},{valor:"Malta"},{valor:"Marrocos"},{valor:"Martinica"},{valor:"Mauritânia"},{valor:"Mauritius"},{valor:"México"},{valor:"Moldova"},{valor:"Mônaco"},{valor:"Montserrat"},{valor:"Nepal"},{valor:"Nicarágua"},{valor:"Niger"},{valor:"Nigéria"},{valor:"Noruega"},{valor:"Nova Caledônia"},{valor:"Nova Zelândia"},{valor:"Omã"},{valor:"Palau"},{valor:"Panamá"},{valor:"Papua-nova Guiné"},{valor:"Paquistão"},{valor:"Peru"},{valor:"Polinésia Francesa"},{valor:"Polônia"},{valor:"Porto Rico"},{valor:"Portugal"},{valor:"Qatar"},{valor:"Quênia"},{valor:"Rep. Dominicana"},{valor:"Rep. Tcheca"},{valor:"Reunion"},{valor:"Romênia"},{valor:"Ruanda"},{valor:"Rússia"},{valor:"Saipan"},{valor:"Samoa Americana"},{valor:"Senegal"},{valor:"Serra Leone"},{valor:"Seychelles"},{valor:"Singapura"},{valor:"Síria"},{valor:"Sri Lanka"},{valor:"St. Kitts & Nevis"},{valor:"St. Lúcia"},{valor:"St. Vincent"},{valor:"Sudão"},{valor:"Suécia"},{valor:"Suiça"},{valor:"Suriname"},{valor:"Tailândia"},{valor:"Taiwan"},{valor:"Tanzânia"},{valor:"Togo"},{valor:"Trinidad & Tobago"},{valor:"Tunísia"},{valor:"Turquia"},{valor:"Ucrânia"},{valor:"Uganda"},{valor:"Uruguai"},{valor:"Venezuela"},{valor:"Vietnã"},{valor:"Zaire"},{valor:"Zâmbia"},{valor:"Zimbábue"}];
	_this.paisesConfig = {
		valueField: 'valor',
		labelField: 'valor',
		highlight: false,
		searchField: ['valor'],
		maxItems: 1
	};

	$scope.criarCliente = function(){
		var dados = {
			name: _this.nome,
			cpf: _this.cpfcnpj,
			telephone: _this.telefone,
			email: _this.email,
			site: _this.site,
			street: _this.rua,
			number: _this.numero,
			complement: _this.complemento,
			neighbourhood: _this.bairro,
			city: _this.cidade,
			state: _this.estado,
			country: _this.pais,
			cep: _this.cep,
			comments: _this.comentarios
		};
		ElysiumAPI.post('clients', 'person', dados).then(function(response){
			$scope.adicionarCliente(new ClienteFactory(response.data));
			$scope.$hide();
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao criar um novo cliente', response);
		});
	};

	$scope.criarAdverso = function(){
		var dados = {
			name: _this.nome,
			cpf: _this.cpfcnpj,
			telephone: _this.telefone,
			email: _this.email,
			site: _this.site,
			street: _this.rua,
			number: _this.numero,
			complement: _this.complemento,
			neighbourhood: _this.bairro,
			city: _this.cidade,
			state: _this.estado,
			country: _this.pais,
			cep: _this.cep,
			comments: _this.comentarios
		};
		ElysiumAPI.post('adverses', 'person', dados).then(function(response){
			$scope.adicionarAdverso(new AdversoFactory(response.data));
			$scope.$hide();
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao criar um novo adverso', response);
		});
	};

	$scope.criarAdvogado = function(){
		var dados = {
			name: _this.nome,
			telephone: _this.telefone,
			email: _this.email,
			oab: _this.numero_da_oab,
			office_name: _this.nome_do_escritorio,
			comments: _this.comentarios
			};
		ElysiumAPI.post('adverse_lawyers', 'person', dados).then(function(response){
			$scope.adicionarAdvogado(new AdvogadoAdversoFactory(response.data));
			$scope.$hide();
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao criar um novo advogado adverso', response);
		});
	};
}]);
