elysium.controller('PessoaDetalhesCtrl', ['Pessoas', '$stateParams', '$state', function(Pessoas, $stateParams, $state){
	var _this = this;
    
	if($stateParams.type){
        Pessoas.getPessoa($stateParams.id, $stateParams.type).then(function(response){
            _this.dados = response;
            _this.subtipo = $stateParams.type;
        });
    } else{
	    $state.go('app.pessoas.todas');
    }

	_this.excluirPessoa = function(pessoa){
	    pessoa.type = _this.subtipo;
		Pessoas.excluirPessoa(pessoa).then(function(){
			$state.go('app.pessoas.todas');
		});
	};
}]);
