elysium.controller('PessoasCtrl', ['ElysiumAPI', '$state', function(ElysiumAPI, $state){
	var _this = this;

	_this.filtroTipo = '';

	_this.mudarTipo = function(filtro){
		if(!$state.is('app.pessoas.todas')) $state.go('app.pessoas.todas');
		_this.filtroTipo = filtro;
	};
}]);
