elysium.controller('ListarPessoasCtrl', ['Pessoas', function(Pessoas){
	var _this = this;

	/**
	 * Inicialização da lista de Pessoas
	 * Pega as pessoas da API e guarda numa variável
	 */
	Pessoas.getPessoas().then(function(response){
		_this.pessoas = response;
	});

	/**
	 * Exclui a pessoa especificada via API e remove da listagem de Pessoas
	 * @param  {Object} pessoa A Pessoa que deve ser excluída
	 */
	_this.excluirPessoa = function(pessoa){
		Pessoas.excluirPessoa(pessoa).then(function(){
			_this.pessoas.splice(_this.pessoas.indexOf(pessoa), 1);
		});
	};

}]);
