elysium.factory('AdvogadoAdversoFactory', [function(){
    var AdvogadoAdverso = function(data){
        this.id = data.id;
        this.created_at = data.created_at;
        this.update_date = data.update_date;
        this.nome = data.name;
        
        this.numero_da_oab = data.oab || null;
        this.telefone = data.telephone || null;
        this.email = data.email || null;
        this.escritorio = data.office_name || null;
        this.comentarios = data.comments || null;
    };
    
    
    return AdvogadoAdverso;
}]);
