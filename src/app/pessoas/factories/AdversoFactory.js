elysium.factory('AdversoFactory', [function(){
    var Adverso = function(data){
        this.id = data.id;
        this.created_at = data.created_at;
        this.update_date = data.update_date;
        this.nome = data.name;
        
        this.cpf_cnpj = data.cpf || null;
        this.telefone = data.telephone || null;
        this.email = data.email || null;
        this.site = data.site || null;
        this.rua = data.street || null;
        this.numero = data.number || null;
        this.complemento = data.complement || null;
        this.bairro = data.neighbourhood || null;
        this.cidade = data.city || null;
        this.estado = data.state || null;
        this.pais = data.country || null;
        this.cep = data.cep || null;
        this.comentarios = data.comments || null;
    };
    
    
    return Adverso;
}]);
