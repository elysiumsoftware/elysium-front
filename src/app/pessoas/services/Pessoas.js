elysium.service('Pessoas', ['APIService', 'Toasts', '$q', function(APIService, Toasts, $q){
    var _this = this;

    /**
     * GET
     */

    _this.getPessoas = function(){
        return APIService.GET('people').then(function(response){
            return $q.when(response.data);
        }, function(response){
            Toasts.mostrarErro('Algo deu errado ao carregar a lista de Pessoas', response);
            return $q.reject();
        });
    };

    _this.getPessoa = function(id, type){
        if(type == 'Client') var url = 'clients';
        else if(type == 'Adverse') var url = 'adverses';
        else if(type == 'Adverse Lawyer') var url = 'adverse-lawyers';
        else if(type == 'Correspondent') var url = 'correspondents';
        
        return APIService.GET('people/' + url + '/' + id).then(function(response){
            return $q.when(response.data);
        }, function(response){
            Toasts.mostrarErro('Algo deu errado ao carregar essa Pessoa', response);
            return $q.reject();
        });
    };

    /**
     * DELETE
     */

    _this.excluirPessoa = function(pessoa){
        /**
         * Descobre o subtipo da pessoa pra usar na URL
         */
        var _subtipo = resolverSubtipo(pessoa.type);
        return APIService.DELETE('person/' + _subtipo + '/' + pessoa.id).then(function(response){
            return $q.when(pessoa);
        }, function(response){
            Toasts.mostrarErro('Algo deu errado ao excluir uma Pessoa', response);
            return $q.reject();
        });
    };

    /**
     * Utilidades
     */

    var resolverSubtipo = function(tipo){
        switch (tipo){
            case 'Client':
                return 'clients';
                break;
            case 'Adverse':
                return 'adverses';
                break;
            case 'Adverse Lawyer':
                return 'adverse_lawyers';
                break;
            case 'Correspondent':
                return 'correspondents';
                break;
            default:
                return $q.reject();
        }
    };
}]);
