elysium.controller('ChangePlanController', ['_Data', '$scope', '$q', 'PaymentService', '$rootScope', function(_Data, $scope, $q, PaymentService, $rootScope){
    var _this = this;

    this.data = {
        company: _Data.company
    };

    PaymentService.getPlans().then(function(plans){
        _this.data.plans = plans;

        angular.forEach(_this.data.plans, function(plan){
            angular.forEach(plan.items, function(item){
                if(item.product.unit === 'base') plan.base_price = String(item.product.pricing_schema.price).replace('.', ',');
                else if(item.product.pricing_schema.metric === 'users') plan.user_price = item.product.pricing_schema.short_format;
            })
        })
    });


    this.selectPlan = function(id){
        PaymentService.selectPlan(id).then(function(company){
            $rootScope.$broadcast('payments:company:update', company);
            $scope.$hide();
        });
    };

    this.closeModal = function(){
        $scope.$hide();
    };
}]);
