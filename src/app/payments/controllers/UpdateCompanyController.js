elysium.controller('UpdateCompanyController', ['_Data', '$scope', '$q', 'PaymentService', '$rootScope', 'STATES', function(_Data, $scope, $q, PaymentService, $rootScope, STATES){
    var _this = this;

    this.data = {
        company: angular.copy(_Data.company),
        states: STATES
    };


    this.updateCompany = function(){
        if(!_this.data.company.state){
            $scope.$broadcast('payments:updateCompany:form:state:focus');
            return;
        }

        PaymentService.updateCompany(_this.data.company).then(function(company){
            $rootScope.$broadcast('payments:company:update', company);
            $scope.$hide();
        });
    };

    this.closeModal = function(){
        $scope.$hide();
    };
}]);
