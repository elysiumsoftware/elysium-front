elysium.controller('PaymentWizardController', ['_Data', '$scope', '$q', 'PaymentService', '$rootScope', function(_Data, $scope, $q, PaymentService, $rootScope){
    var _this = this;

    this.data = {
        company: _Data.company,
        paymentProfile: {},
        step: 'plan',
        states: [
            {code: "AC", name: "Acre"},
            {code: "AL", name: "Alagoas"},
            {code: "AM", name: "Amazonas"},
            {code: "AP", name: "Amapá"},
            {code: "BA", name: "Bahia"},
            {code: "CE", name: "Ceará"},
            {code: "DF", name: "Distrito Federal"},
            {code: "ES", name: "Espírito Santo"},
            {code: "GO", name: "Goiás"},
            {code: "MA", name: "Maranhão"},
            {code: "MT", name: "Mato Grosso"},
            {code: "MS", name: "Mato Grosso do Sul"},
            {code: "MG", name: "Minas Gerais"},
            {code: "PA", name: "Pará"},
            {code: "PB", name: "Paraíba"},
            {code: "PR", name: "Paraná"},
            {code: "PE", name: "Pernambuco"},
            {code: "PI", name: "Piauí"},
            {code: "RJ", name: "Rio de Janeiro"},
            {code: "RN", name: "Rio Grande do Norte"},
            {code: "RO", name: "Rondônia"},
            {code: "RS", name: "Rio Grande do Sul"},
            {code: "RR", name: "Roraima"},
            {code: "SC", name: "Santa Catarina"},
            {code: "SE", name: "Sergipe"},
            {code: "SP", name: "São Paulo"},
            {code: "TO", name: "Tocantins"}
        ]
    };

    PaymentService.getPlans().then(function(plans){
        _this.data.plans = plans;

        angular.forEach(_this.data.plans, function(plan){
            angular.forEach(plan.items, function(item){
                if(item.product.unit === 'base') plan.base_price = String(item.product.pricing_schema.price).replace('.', ',');
                else if(item.product.pricing_schema.metric === 'users') plan.user_price = item.product.pricing_schema.short_format;
            })
        })
    });


    this.closeWizard = function(){
        $scope.$hide();
    };

    this.selectPlan = function(id){
        PaymentService.selectPlan(id).then(function(){
            _this.data.step = 'company';
        });
    };

    this.updateCompany = function(){
        if(!_this.data.company.state){
            $scope.$broadcast('payments:wizard:form:state:focus');
            return;
        }

        PaymentService.updateCompany(_this.data.company).then(function(){
            _this.data.step = 'payment';
        });
    };

    this.createPaymentProfile = function(){
        var data = angular.copy(_this.data.paymentProfile);
        data.card_expiration = data.card_expiration.format('MM/YYYY');

        PaymentService.createPaymentProfileToken(data)
                      .then(PaymentService.createPaymentProfile)
                      .then(function(company){
                          $rootScope.$broadcast('payments:company:update', company);
                          $scope.$hide();
                      });
    };
}]);
