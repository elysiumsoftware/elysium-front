elysium.controller('ChangeCreditCardController', ['$scope', '$q', 'PaymentService', '$rootScope', function($scope, $q, PaymentService, $rootScope){
    var _this = this;

    this.data = {
        paymentProfile: {}
    };


    this.updatePaymentProfile = function(id){
        var data = angular.copy(_this.data.paymentProfile);
        data.card_expiration = data.card_expiration.format('MM/YYYY');

        PaymentService.createPaymentProfileToken(data)
                      .then(PaymentService.createPaymentProfile)
                      .then(function(company){
                          $rootScope.$broadcast('payments:company:update', company);
                          $scope.$hide();
                      });
    };

    this.closeModal = function(){
        $scope.$hide();
    };
}]);
