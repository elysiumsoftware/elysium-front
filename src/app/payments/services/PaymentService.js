elysium.service('PaymentService', ['APIService', 'NotificationFactory', '$q', 'ToastService', '$http', 'VINDI_KEY', 'VINDI_URL', function(APIService, NotificationFactory, $q, ToastService, $http, VINDI_KEY, VINDI_URL){
    this.getPlans = function(){
        return APIService.GET('payments/plans').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de planos disponíveis.', error);
            return $q.reject(error);
        });
    };

    this.selectPlan = function(id){
        return APIService.POST('payments/plan', {plan: id}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao selecionar um plano para sua empresa.', error);
            return $q.reject(error);
        });
    };

    this.updateCompany = function(data){
        return APIService.PATCH('payments/company', data).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar as informações da sua empresa.', error);
            return $q.reject(error);
        });
    };

    this.createPaymentProfileToken = function(data){
        return $http({
            method: 'POST',
            url: VINDI_URL + 'public/payment_profiles',
            data: data,
            headers: {
                'Authorization': 'Basic ' + btoa(VINDI_KEY + ':')
            },
            skipAuthorization: true
        }).then(function(response){
            return response.data.payment_profile.gateway_token;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao enviar os dados do seu cartão de crédito.', error);
            return $q.reject(error);
        });
    };

    this.createPaymentProfile = function(token){
        return APIService.POST('payments/token', {gateway_token: token}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao associar seu cartão de crédito à sua conta.', error);
            return $q.reject(error);
        });
    };

    this.getUserPrice = function(){
        return APIService.GET('payments/users/price').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar detalhes do seu plano.', error);
            return $q.reject(error);
        });
    };

    this.activateUser = function(id){
        return APIService.POST('payments/users/activate', {user: id}).then(function(response){
            return response;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao habilitar uma conta.', error);
            return $q.reject(error);
        });
    };

    this.deactivateUser = function(id){
        return APIService.POST('payments/users/deactivate', {user: id}).then(function(response){
            return response;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao desabilitar uma conta.', error);
            return $q.reject(error);
        });
    };

    this.getBillPrice = function(){
        return APIService.GET('payments/bills/price').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar o valor da nova fatura.', error);
            return $q.reject(error);
        });
    };

    this.createBrill = function(){
        return APIService.POST('payments/bills/create', {}).then(function(response){
            return response;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar uma nova fatura para sua empresa.', error);
            return $q.reject(error);
        });
    };
}]);
