elysium.factory('NotificationFactory', [function(){
    var Notification = function(data){
        this.id = data.id;
        this.created_at = new moment(data.created_at);
        this.read = data.read;
        this.archived = data.archived;
        this.level = data.level;
        this.app = data.app;
        this.action = data.action;
        
        this.title = data.title || null;
        this.description = data.description || null;
        this.data = data.data || null;
        this.timestamp = data.timestamp ? new moment(data.timestamp) : null;
        this.count = data.count || null;
        this.actor = data.actor || null;
        this.target = data.target || null;
        this.action_object = data.action_object || null;
    };
    
    return Notification;
}]);
