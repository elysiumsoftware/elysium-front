elysium.controller('NotificationsPopoverController', ['$scope', 'NotificationService', '$q', '$state', 'ModalService', function($scope, NotificationService, $q, $state, ModalService){
    var _this = this;
    
    this.data = {
        tab: 'recent',
        notifications: null,
        archivedNotifications: null
    };
    
    
    NotificationService.getNotifications().then(function(notifications){
        _this.data.notifications = notifications;
    });
    
    
    this.changeTab = function(tab){
        if(_this.data.tab !== tab){
            if(tab === 'archived'){
                _this.data.archivedNotifications = null;
        
                NotificationService.getArchivedNotifications().then(function(archivedNotifications){
                    _this.data.archivedNotifications = archivedNotifications;
                });
            } else if(tab === 'recent'){
                _this.data.notifications = null;
        
                NotificationService.getNotifications().then(function(notifications){
                    _this.data.notifications = notifications;
                });
            }
    
            _this.data.tab = tab;
        }
    };
    
    this.markAsRead = function(notification){
        NotificationService.markAsRead(notification.id).then(function(){
            _this.data.notifications[_this.data.notifications.indexOf(notification)].read = true;
            
            $scope.Header.data.notificationCount--;
        });
    };
    
    this.markAsArchived = function(notification){
        NotificationService.markAsArchived(notification.id).then(function(){
            if(!notification.read) $scope.Header.data.notificationCount--;
            
            _this.data.notifications.splice(_this.data.notifications.indexOf(notification), 1);
        });
    };
    
    this.archiveAll = function(){
        NotificationService.archiveAll().then(function(){
            _this.data.notifications = [];
            $scope.Header.data.notificationCount = 0;
        });
    };
    
    this.delete = function(notification){
        NotificationService.delete(notification.id).then(function(){
            _this.data.archivedNotifications.splice(_this.data.archivedNotifications.indexOf(notification), 1);
        });
    };
    
    this.deleteAll = function(){
        NotificationService.deleteAll().then(function(){
            _this.data.archivedNotifications = [];
        });
    };
    
    this.openProcess = function(notification, process, skipMarkAsRead){
        if(!skipMarkAsRead) NotificationService.markAsRead(notification.id);
        $scope.Header.data.notificationCount--;
        $state.go('app.processos.details', {id: process.id});
        $scope.$hide();
    };

    this.viewPublications = function(notification, date, skipMarkAsRead){
        if(!skipMarkAsRead) NotificationService.markAsRead(notification.id);
        $scope.Header.data.notificationCount--;
        $state.go('app.publications.all', {date: date});
        $scope.$hide();
    };
    
    this.openTask = function(notification, task, skipMarkAsRead){
        if(!skipMarkAsRead) NotificationService.markAsRead(notification.id);
        $scope.Header.data.notificationCount--;
        ModalService.setModal({
            locals: {
                _Data: {
                    task: task.id
                }
            },
            templateUrl: 'app/tasks/views/modals/task-details.html',
            controller: 'TaskDetailsModalController as TaskDetailsModal'
        });
        $scope.$hide();
    }
}]);
