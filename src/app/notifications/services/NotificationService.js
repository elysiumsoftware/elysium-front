elysium.service('NotificationService', ['APIService', 'NotificationFactory', '$q', 'ToastService', function(APIService, NotificationFactory, $q, ToastService){
    this.getNotificationCount = function(){
        return APIService.GET('notifications/count').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas notificações', error);
            return $q.reject(error);
        });
    };
    
    this.getNotifications = function(){
        return APIService.GET('notifications').then(function(response){
            var notificationList = [];
            
            angular.forEach(response.data, function(notification){
                notificationList.push(new NotificationFactory(notification));
            });
            
            return notificationList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas notificações', error);
            return $q.reject(error);
        });
    };
    
    this.getArchivedNotifications = function(){
        return APIService.GET('notifications/archived').then(function(response){
            var notificationList = [];
            
            angular.forEach(response.data, function(notification){
                notificationList.push(new NotificationFactory(notification));
            });
            
            return notificationList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas notificações arquivadas', error);
            return $q.reject(error);
        });
    };
    
    this.markAsRead = function(id){
        return APIService.POST('notifications/' + id + '/read', null).then(function(){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao marcar uma notificação como lida', error);
            return $q.reject(error);
        });
    };
    
    this.markAsArchived = function(id){
        return APIService.POST('notifications/' + id + '/archive', null).then(function(){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao arquivar uma notificação', error);
            return $q.reject(error);
        });
    };
    
    this.archiveAll = function(){
        return APIService.POST('notifications/archive', null).then(function(){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao marcar uma notificação como lida', error);
            return $q.reject(error);
        });
    };
    
    this.delete = function(id){
        return APIService.DELETE('notifications/' + id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar uma notificação', error);
            $q.reject(error);
        });
    };
    
    this.deleteAll = function(){
        return APIService.POST('notifications/delete').then(function(){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar as notificações arquivadas', error);
            $q.reject(error);
        });
    }
}]);
