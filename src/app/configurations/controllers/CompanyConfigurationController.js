elysium.controller('CompanyConfigurationController', ['ConfigurationService', 'ModalService', '$timeout', '$scope', '$state', 'AuthenticationService', function(ConfigurationService, ModalService, $timeout, $scope, $state, AuthenticationService){
    var _this = this;

    this.data = {};
    
    function processCompany(company){
        if(!company.plan && !company.payment_profile){
            var trial_end = moment(company.created_at).add(15, 'days');
            company.past_trial = trial_end < moment();

            $timeout(function(){_this.data.trial_expiration = trial_end.fromNow();}, 500);
        }

        if(company.payment_profile){
            company.payment_profile.card_expiration = moment(company.payment_profile.card_expiration).format('MM/YYYY');
            if(company.payment_profile.holder_name.length > 17) company.payment_profile.holder_name = company.payment_profile.holder_name.substring(0, 17) + '...';
            company.payment_profile.created_at = moment(company.payment_profile.created_at).fromNow();
        }
        
        return company;
    }

    ConfigurationService.getCompany().then(function(company){
        if(company.status !== 'inactive'){
            if($state.includes('authentication')) $state.go('app.processos.list');
        }

        _this.data.company = processCompany(company);
        
        if(company.status !== 'trial'){
            ConfigurationService.getBills().then(function(bills){
                _this.data.bills = bills;
            });
        }
    });

    $scope.$on('payments:company:update', function(event, company){
        _this.data.company = processCompany(company);
    });


    this.openPaymentWizard = function(){
        ModalService.setModal({
            locals: {_Data: {company: _this.data.company}},
            templateUrl: 'app/payments/views/modal/payment-wizard.html',
            controller: 'PaymentWizardController as PaymentWizard',
            backdrop: 'static',
            keyboard: false
        });
    };

    this.openChangePlanModal = function(){
        ModalService.setModal({
            locals: {_Data: {company: _this.data.company}},
            templateUrl: 'app/payments/views/modal/change-plan.html',
            controller: 'ChangePlanController as ChangePlan'
        });
    };

    this.openChangeCreditCardModal = function(){
        ModalService.setModal({
            templateUrl: 'app/payments/views/modal/change-credit-card.html',
            controller: 'ChangeCreditCardController as ChangeCreditCard'
        });
    };

    this.openUpdateCompanyModal = function(){
        ModalService.setModal({
            locals: {_Data: {company: _this.data.company}},
            templateUrl: 'app/payments/views/modal/update-company.html',
            controller: 'UpdateCompanyController as UpdateCompany'
        });
    };

    this.openCreateBillModal = function(){
        ModalService.setModal({
            locals: {_Data: {company: _this.data.company}},
            templateUrl: 'app/authentication/views/modal/create-bill.html',
            controller: 'CreateBillModalController as CreateBillModal'
        });
    };

    this.openPauseSubscriptionModal = function(){
        ModalService.setModal({
            locals: {_Data: {company: _this.data.company}},
            templateUrl: 'app/configurations/views/modal/pause-subscription.html',
            controller: 'PauseSubscriptionModalController as PauseSubscriptionModal'
        });
    };

    this.logout = function(){
        AuthenticationService.logout().finally(function(){
            $state.go('authentication.login');
        });
    };

    this.canCreateBill = function(){
        if(!_this.data.bills) return false;

        for(var i = 0; i < _this.data.bills.length; i++){
            if(_this.data.bills[i].status === 'pending') return false;
        }

        return true;
    }
}]);
