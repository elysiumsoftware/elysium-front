elysium.controller('PublicationNameWizardController', ['$scope', '$q', 'PublicationService', '$rootScope', 'AuthenticationService', 'ToastService', function($scope, $q, PublicationService, $rootScope, AuthenticationService, ToastService){
    var _this = this;

    this.data = {
        step: 'name',
        currentUser: AuthenticationService.getUser(),
        name: AuthenticationService.getUser().name,
        variations: [],
        exceptions: [],
        loading: false
    };


    this.closeWizard = function(){
        $scope.$hide();
    };

    this.setName = function(){
        _this.data.loading = true;

        PublicationService.generateVariations(_this.data.name).then(function(variations){
            if(_this.data.variations.length) _this.data.variations = [];

            if(variations && variations.length){
                _this.data.generatedVariations = variations;

                angular.forEach(variations, function(variation){
                    _this.data.variations.push({
                        term: variation
                    });
                })
            } else _this.data.generatedVariations = [];

            _this.data.step = 'variations';
        }).finally(function(){
            _this.data.loading = false;
        });
    };

    this.addVariation = function(){
        if(_this.data.variation){
            if(_this.data.variations.length > 99){
                ToastService.showMessage('Você atingiu o limite de 100 variações. Remova algumas variações para inserir novas.');
                return;
            }

            var _uppercaseVariation = _this.data.variation.toUpperCase();

            if(!hasTerm(_this.data.variations, _uppercaseVariation)){
                _this.data.variations.push({
                    term: _uppercaseVariation
                });
            }

            _this.data.variation = null;
        }
    };

    this.removeVariation = function(index){
        _this.data.variations.splice(index, 1);
    };

    this.createVariations = function(){
        _this.data.step = 'exceptions';
    };

    this.addException = function(){
        if(_this.data.exception){
            if(_this.data.exceptions.length > 99){
                ToastService.showMessage('Você atingiu o limite de 100 exceções. Remova algumas exceções para inserir novas.');
                return;
            }
        }

        var _uppercaseException = _this.data.exception.toUpperCase();

        if(!hasTerm(_this.data.exceptions, _uppercaseException)){
            _this.data.exceptions.push({
                term: _uppercaseException,
                is_present_in_searched_name: false
            });
        }

        _this.data.exception = null;
    };

    this.removeException = function(index){
        _this.data.exceptions.splice(index, 1);
    };

    this.createPublicationName = function(){
        var data = {
            oab: _this.data.oab,
            name: _this.data.name,
            variations: _this.data.variations,
            blocking_terms: _this.data.exceptions
        };

        PublicationService.createPublicationName(data).then(function(publicationName){
            $rootScope.$broadcast('publications:publicationName:created', publicationName);
            $scope.$hide();
        });
    };

    function hasTerm(list, term){
        for(var i = 0; i < list.length; i++){
            if(list[i].term === term){
                return true;
            }
        }
        return false;
    }
}]);
