elysium.controller('DeletePublicationNameController', ['$scope', '_Data', 'PublicationService', '$rootScope', function($scope, _Data, PublicationService, $rootScope){
    var _this = this;

    this.data = {
        publicationName: _Data.publicationName
    };

    this.delete = function(){
        PublicationService.deletePublicationName(_this.data.publicationName.id).then(function(){
            $rootScope.$broadcast('publications:publicationName:deleted');
            $scope.$hide();
        });
    };
}]);
