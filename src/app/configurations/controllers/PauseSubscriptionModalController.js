elysium.controller('PauseSubscriptionModalController', ['$scope', '_Data', 'ConfigurationService', '$rootScope', 'PaymentService', 'ToastService', function($scope, _Data, ConfigurationService, $rootScope, PaymentService, ToastService){
    var _this = this;

    this.data = {
        company: _Data.company
    };


    this.pause = function(){
        ConfigurationService.requestPause(_this.data.reason).then(function(){
            if(_this.data.company.status === 'trial'){
                var trial_end = moment(_this.data.company.created_at).add(15, 'days');
                _this.data.company.past_trial = trial_end < moment();
                _this.data.company.plan = null;
                _this.data.company.payment_profile = null;
                $rootScope.$broadcast('payments:company:update', _this.data.company);
                $scope.$hide();
            } else{
                _this.data.company.is_awaiting_pause = true;
                $rootScope.$broadcast('payments:company:update', _this.data.company);
                $scope.$hide();
            }
        });
    };

    this.unpause = function(){
        ConfigurationService.cancelPauseRequest(_this.data.reason).then(function(){
            _this.data.company.is_awaiting_pause = false;
            $rootScope.$broadcast('payments:company:update', _this.data.company);
            $scope.$hide();
        });
    };
}]);
