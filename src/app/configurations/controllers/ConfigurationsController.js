elysium.controller('ConfigurationsController', ['AuthenticationService', function(AuthenticationService){
    var _this = this;
    
    this.data = {
        currentUser: AuthenticationService.getUser()
    };

    _this.paginaSelecionada = 'contas';
    _this.voce = AuthenticationService.getUser().id;
}]);
