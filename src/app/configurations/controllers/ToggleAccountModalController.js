elysium.controller('ToggleAccountModalController', ['$scope', '_Data', 'AuthenticationService', '$rootScope', 'PaymentService', 'ToastService', function($scope, _Data, AuthenticationService, $rootScope, PaymentService, ToastService){
    var _this = this;

    this.data = {
        user: _Data.user,
        currentUser: AuthenticationService.getUser()
    };

    if(this.data.currentUser.company.status === 'active'){
        PaymentService.getUserPrice().then(function(userPrice){
            _this.data.userPrice = userPrice;
        });
    }

    if(this.data.user.active_until && moment(this.data.user.active_until) > moment()) this.data.user.already_paid = true;


    this.toggle = function(){
        if(_this.data.user.is_active){
            PaymentService.deactivateUser(_this.data.user.id).then(function(){
                $rootScope.$broadcast('accounts:account:deactivated', _this.data.user);
                $scope.$hide();
            });
        } else{
            PaymentService.activateUser(_this.data.user.id).then(function(response){
                if(response.status === 200){
                    $rootScope.$broadcast('accounts:account:activated', _this.data.user);
                    $scope.$hide();
                } else{
                    ToastService.showMessage('A cobrança foi gerada com sucesso e estamos aguardando o pagamento ser efetuado.<br><br>Ao fim do processo, você receberá uma ' +
                        'notificação e ' + _this.data.user.name + ' voltará a ter acesso à sua empresa.');
                    $scope.$hide();
                }
            });
        }
    };
}]);
