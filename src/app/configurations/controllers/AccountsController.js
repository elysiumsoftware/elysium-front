elysium.controller('AccountsController', ['ModelService', '$scope', 'AuthenticationService', 'ModalService', function(ModelService, $scope, AuthenticationService, ModalService){
    var _this = this;

    this.data = {
        currentUser: AuthenticationService.getUser()
    };

    ModelService.getSentInvitations().then(function(response){
        _this.data.sentInvitations = response;
    });

    ModelService.getDetailedUsers().then(function(response){
        _this.data.users = response;
    });

    ModelService.getGroups().then(function(response){
        _this.data.groups = response;
    });

    $scope.$on('invitations:send:sent', function(event, sentInvitation){
        _this.data.sentInvitations.push(sentInvitation);
    });

    $scope.$on('invitations:send:revoked', function(event, revokedInvitation){
        for(var i = 0; i < _this.data.sentInvitations.length; i++){
            if(_this.data.sentInvitations[i].id === revokedInvitation.id){
                _this.data.sentInvitations.splice(i, 1, revokedInvitation);
                break;
            }
        }
    });

    $scope.$on('accounts:account:deactivated', function(event, user){
        _this.data.users[_this.data.users.indexOf(user)].is_active = false;
    });

    $scope.$on('accounts:account:activated', function(event, user){
        _this.data.users[_this.data.users.indexOf(user)].is_active = true;
    });


    this.openSendInvitationModal = function(){
        ModalService.setModal({
            templateUrl: 'app/configurations/views/modal/send-invitation.html',
            controller: 'SendInvitationModalController as SendInvitationModal'
        });
    };

    this.openRevokeInvitationModal = function(invitation){
        ModalService.setModal({
            locals: {
                _Data:{
                    invitation: invitation
                }
            },
            templateUrl: 'app/configurations/views/modal/revoke-invitation.html',
            controller: 'RevokeInvitationModalController as RevokeInvitationModal'
        });
    };

    this.openToggleAccountModal = function(user){
        ModalService.setModal({
            locals: {
                _Data:{
                    user: user
                }
            },
            templateUrl: 'app/configurations/views/modal/toggle-account.html',
            controller: 'ToggleAccountModalController as ToggleAccountModal'
        });
    };

    this.canSendInvites = function(){
        if(!_this.data.sentInvitations) return false;

        var status = _this.data.currentUser.company.status;
        if(status === 'active') return true;

        var sent_today = 0;
        var yesterday = moment().subtract(1, 'days');
        angular.forEach(_this.data.sentInvitations, function(invitation){
            if(moment(invitation.sent_date) > yesterday) sent_today++;
        });
        if((status === 'trial' && sent_today >= 5) || (status === 'active' && sent_today >= 25)) return false;

        var valid_invites = _this.data.sentInvitations.filter(function(invite){
            return invite.status === 'PENDING' || invite.status === 'SENT';
        });

        return valid_invites.length < 5;
    };




    
    // function sendInvite(){
    //     // if($scope.Configurations.accounts.invited.indexOf())
    //     ModelService.sendInvite(_this.inviteData.guest_email, _this.inviteData.group).then(function(response){
    //         var inviteAdded = false;
    //         for(var invite = 0; invite < $scope.Configurations.accounts.invited.length; invite++){
    //             if($scope.Configurations.accounts.invited[invite].email === response.email){
    //                 $scope.Configurations.accounts.invited.splice(invite, 1);
    //                 $scope.Configurations.accounts.invited.push(response);
    //                 inviteAdded = true;
    //                 break;
    //             }
    //         }
    //         if(!inviteAdded) $scope.Configurations.accounts.invited.push(response);
    //     });
    // }
    //
    // function cancelInvite(user){
    //     console.debug(user);
    //     ModelService.cancelInvite(user.id).then(function(){
    //         $scope.Configurations.accounts.invited.splice($scope.Configurations.accounts.invited.indexOf(user), 1);
    //     });
    // }
}]);
