elysium.controller('ConfigurationsPopoverController', ['AuthenticationService', '$state', function(AuthenticationService, $state){
    var _this = this;

    this.data = {
        user: AuthenticationService.getUser()
    };


    this.openChat = function(){
        if(drift && drift.api) drift.api.openChat();
    };

    this.logOut = function(){
        AuthenticationService.logout().finally(function(){
            $state.go('authentication.login');
        });
    };
}]);
