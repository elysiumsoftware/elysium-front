elysium.controller('PublicationsConfigurationController', ['ConfigurationService', 'ModalService', 'PublicationService', '$scope', '$state', 'AuthenticationService', function(ConfigurationService, ModalService, PublicationService, $scope, $state, AuthenticationService){
    var _this = this;

    this.data = {
        currentUser: AuthenticationService.getUser(),
        publicationName: null
    };

    if(this.data.currentUser.publication_name){
        PublicationService.getPublicationName().then(function(publicationName){
            if(publicationName) _this.data.publicationName = publicationName;
            else _this.data.currentUser.publication_name = null;
        });
    }

    $scope.$on('publications:publicationName:created', function(event, publicationName){
        _this.data.publicationName = publicationName;
        _this.data.currentUser.publication_name = publicationName;
    });

    $scope.$on('publications:publicationName:deleted', function(event){
        _this.data.publicationName = null;
        _this.data.currentUser.publication_name = null;
    });


    this.openPublicationNameWizard = function(){
        ModalService.setModal({
            templateUrl: 'app/configurations/views/modal/publication-name-wizard.html',
            controller: 'PublicationNameWizardController as PublicationNameWizard',
            backdrop: 'static',
            keyboard: false
        });
    };

    this.openDeletePublicationNameModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    publicationName: _this.data.publicationName
                }
            },
            templateUrl: 'app/configurations/views/modal/delete-publication-name.html',
            controller: 'DeletePublicationNameController as DeletePublicationName'
        });
    };

    this.openPublicationNameVariationsModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    publicationName: _this.data.publicationName
                }
            },
            templateUrl: 'app/configurations/views/modal/publication-name-variations.html',
            controller: 'PublicationNameVariationsModalController as PublicationNameVariationsModal'
        });
    };

    this.openPublicationNameExceptionsModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    publicationName: _this.data.publicationName
                }
            },
            templateUrl: 'app/configurations/views/modal/publication-name-exceptions.html',
            controller: 'PublicationNameExceptionsModalController as PublicationNameExceptionsModal'
        });
    };
}]);
