elysium.controller('PublicationNameVariationsModalController', ['$scope', '_Data', 'PublicationService', '$rootScope', '$timeout', 'ToastService', function($scope, _Data, PublicationService, $rootScope, $timeout, ToastService){
    var _this = this;

    this.data = {
        publicationName: _Data.publicationName,
        loading: false
    };


    this.deleteVariation = function(variation, index){
        if(!_this.data.loading){
            setLoading();

            PublicationService.deleteVariation(variation.id).then(function(){
                $timeout(function(){
                    _this.data.publicationName.variations.splice(index, 1);
                    _this.data.loading = false;
                }, 5000);
            }).catch(function(){
                _this.data.loading = false;
            });
        }
    };

    this.addVariation = function(){
        if(!_this.data.loading && _this.data.variation){
            if(_this.data.publicationName.variations.length > 99){
                ToastService.showMessage('Você atingiu o limite de 100 variações. Remova algumas variações para inserir novas.');
                return;
            }

            var _uppercaseVariation = _this.data.variation.toUpperCase();

            if(!hasTerm(_this.data.publicationName.variations, _uppercaseVariation)){
                setLoading();

                PublicationService.createVariation(_this.data.publicationName, _uppercaseVariation).then(function(variation){
                    $timeout(function(){
                        _this.data.publicationName.variations.push(variation);
                        _this.data.loading = false;
                        _this.data.variation = null;
                    }, 5000);
                }).catch(function(){
                    _this.data.loading = false;
                });
            } else _this.data.variation = null;
        }
    };

    this.closeModal = function(){
        $scope.$hide();
    };

    function setLoading(){
        _this.data.loading = true;
    }

    function throttleLoad(){
        $timeout(function(){
            _this.data.loading = false;
        }, 5000);
    }

    function hasTerm(list, term){
        for(var i = 0; i < list.length; i++){
            if(list[i].term === term){
                return true;
            }
        }
        return false;
    }
}]);
