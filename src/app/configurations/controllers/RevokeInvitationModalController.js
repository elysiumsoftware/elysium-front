elysium.controller('RevokeInvitationModalController', ['$scope', '_Data', 'AuthenticationService', '$rootScope', function($scope, _Data, AuthenticationService, $rootScope){
    var _this = this;

    this.data = {
        invitation: _Data.invitation
    };

    this.revoke = function(){
        AuthenticationService.revokeInvitation(_this.data.invitation.id).then(function(invitation){
            $rootScope.$broadcast('invitations:send:revoked', invitation);
            $scope.$hide();
        });
    };
}]);
