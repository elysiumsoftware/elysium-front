elysium.controller('ProfileController', ['AuthenticationService', 'ToastService', 'Upload', 'APIURL', '$scope', 'ConfigurationService', '$timeout', function(AuthenticationService, ToastService, Upload, APIURL, $scope, ConfigurationService, $timeout){
    var _this = this;

    this.data = {
        user: AuthenticationService.getUser(),
        updated: false,
        states: [
            {code: "AC", name: "Acre"},
            {code: "AL", name: "Alagoas"},
            {code: "AM", name: "Amazonas"},
            {code: "AP", name: "Amapá"},
            {code: "BA", name: "Bahia"},
            {code: "CE", name: "Ceará"},
            {code: "DF", name: "Distrito Federal"},
            {code: "ES", name: "Espírito Santo"},
            {code: "GO", name: "Goiás"},
            {code: "MA", name: "Maranhão"},
            {code: "MT", name: "Mato Grosso"},
            {code: "MS", name: "Mato Grosso do Sul"},
            {code: "MG", name: "Minas Gerais"},
            {code: "PA", name: "Pará"},
            {code: "PB", name: "Paraíba"},
            {code: "PR", name: "Paraná"},
            {code: "PE", name: "Pernambuco"},
            {code: "PI", name: "Piauí"},
            {code: "RJ", name: "Rio de Janeiro"},
            {code: "RN", name: "Rio Grande do Norte"},
            {code: "RO", name: "Rondônia"},
            {code: "RS", name: "Rio Grande do Sul"},
            {code: "RR", name: "Roraima"},
            {code: "SC", name: "Santa Catarina"},
            {code: "SE", name: "Sergipe"},
            {code: "SP", name: "São Paulo"},
            {code: "TO", name: "Tocantins"}
        ]
    };

    $scope.$on('auth:user:update', function(event, user){
        _this.data.user = user;
        _this.data.updated = true;

        _this.data.timeout = $timeout(function(){
            _this.data.updated = false;
        }, 5100);
    });


    this.updateAvatar = function(file, invalidFile){
        if(_this.data.timeout){
            $timeout.cancel(_this.data.timeout);
            _this.data.updated = false;
        }

        if(invalidFile[0] && invalidFile[0].$error === 'pattern'){
            ToastService.showError('O arquivo selecionado tem uma extensão inválida. Apenas fotos em JPG ou PNG são aceitas.');
            return;
        } else if(invalidFile[0] && invalidFile[0].$error === 'maxSize'){
            ToastService.showError('O arquivo selecionado é grande demais para ser enviado. O tamanho máximo é de 2MB.');
            return;
        }

        if(file){
            Upload.upload({
                url: APIURL + 'auth/me/',
                method: 'PATCH',
                data: {avatar: file}
            }).then(function(response){
                AuthenticationService.setUser(response.data);
            }, function(error){
                ToastService.showError('Algo deu errado ao atualizar sua foto de perfil.', error);
            });
        }
    };

    this.updateProfile = function(){
        if(_this.data.timeout){
            $timeout.cancel(_this.data.timeout);
            _this.data.updated = false;
        }

        var data = {
            name: this.data.user.name,
            telephone: this.data.user.telephone,
            city: this.data.user.city,
            state: this.data.user.state
        };

        ConfigurationService.updateProfile(data).then(function(user){
            AuthenticationService.setUser(user);
            $scope.UpdateProfileForm.$setPristine();
        });
    };
}]);
