elysium.controller('PublicationNameExceptionsModalController', ['$scope', '_Data', 'PublicationService', '$rootScope', '$timeout', 'ToastService', function($scope, _Data, PublicationService, $rootScope, $timeout, ToastService){
    var _this = this;

    this.data = {
        publicationName: _Data.publicationName,
        loading: false
    };


    this.deleteException = function(exception, index){
        if(!_this.data.loading){
            setLoading();

            PublicationService.deleteException(exception.id).then(function(){
                $timeout(function(){
                    _this.data.publicationName.exceptions.splice(index, 1);
                    _this.data.loading = false;
                }, 5000);
            }).catch(function(){
                _this.data.loading = false;
            });
        }
    };

    this.addException = function(){
        if(!_this.data.loading && _this.data.exception){
            if(_this.data.publicationName.exceptions.length > 99){
                ToastService.showMessage('Você atingiu o limite de 100 exceções. Remova algumas exceções para inserir novas.');
                return;
            }

            var _uppercaseException = _this.data.exception.toUpperCase();

            if(!hasTerm(_this.data.publicationName.exceptions, _uppercaseException)){
                setLoading();

                PublicationService.createException(_this.data.publicationName, _uppercaseException).then(function(exception){
                    $timeout(function(){
                        _this.data.publicationName.exceptions.push(exception);
                        _this.data.loading = false;
                        _this.data.exception = null;
                    }, 5000);
                }).catch(function(){
                    _this.data.loading = false;
                });
            } else _this.data.exception = null;
        }
    };

    this.updateException = function(exception, index){
        if(!_this.data.loading){
            setLoading();

            PublicationService.updateException(exception).then(function(new_exception){
                $timeout(function(){
                    _this.data.publicationName.exceptions[index] = new_exception;
                    _this.data.loading = false;
                }, 5000);
            }).catch(function(){
                _this.data.loading = false;
            });
        }
    };

    this.closeModal = function(){
        $scope.$hide();
    };

    function setLoading(){
        _this.data.loading = true;
    }

    function hasTerm(list, term){
        for(var i = 0; i < list.length; i++){
            if(list[i].term === term){
                return true;
            }
        }
        return false;
    }
}]);
