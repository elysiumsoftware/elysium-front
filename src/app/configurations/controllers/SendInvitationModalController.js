elysium.controller('SendInvitationModalController', ['$scope', '$q', 'PaymentService', 'AuthenticationService', '$rootScope', function($scope, $q, PaymentService, AuthenticationService, $rootScope){
    var _this = this;

    this.data = {
        currentUser: AuthenticationService.getUser()
    };

    if(this.data.currentUser.company.status === 'active'){
        PaymentService.getUserPrice().then(function(userPrice){
            _this.data.userPrice = userPrice;
        });
    }


    this.sendInvitation = function(){
        data = {
            user_email: _this.data.emailAddress
            // No groups for now
            // group:
        };

        AuthenticationService.sendInvitation(data).then(function(sentInvitation){
            $rootScope.$broadcast('invitations:send:sent', sentInvitation);
            $scope.$hide();
        });
    };

    this.closeModal = function(){
        $scope.$hide();
    };
}]);
