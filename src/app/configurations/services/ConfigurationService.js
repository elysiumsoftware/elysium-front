elysium.service('ConfigurationService', ['APIService', 'NotificationFactory', '$q', 'ToastService', function(APIService, NotificationFactory, $q, ToastService){
    this.updateProfile = function(data){
        return APIService.PATCH('auth/me', data).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar seu perfil.', error);
            return $q.reject(error);
        });
    };

    this.getCompany = function(){
        return APIService.GET('payments/company').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar as informações da sua empresa.', error);
            return $q.reject(error);
        });
    };

    this.requestPause = function(reason){
        return APIService.POST('payments/pause', {reason: reason}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao fazer um pedido de pausa para sua assinatura.', error);
            return $q.reject(error);
        });
    };

    this.cancelPauseRequest = function(reason){
        return APIService.POST('payments/pause/cancel', {reason: reason}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao cancelar seu pedido de pausa de assinatura.', error);
            return $q.reject(error);
        });
    };

    this.getBills = function(){
        return APIService.GET('payments/bills').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar os pagamentos da sua empresa.', error);
            return $q.reject(error);
        });
    };
}]);
