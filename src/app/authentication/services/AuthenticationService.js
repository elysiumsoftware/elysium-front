elysium.service('AuthenticationService', ['APIService', '$q', 'LocalStorageService', '$rootScope', 'ToastService', function(APIService, $q, LocalStorageService, $rootScope, ToastService){
    var _this = this;


    $rootScope.$on('service:authentication:unauthorized', function(event){
        if(_this.getUser()){
            _this.setUser(null);
            drift.on('ready', function(){drift.reset()});
            Raven.setUserContext();

            $rootScope.$state.go('authentication.login', {sref: $rootScope.$state.current.name, params: angular.copy($rootScope.$stateParams)});
        }
    });

    // FIXME: na segunda vez que você faz login numa mesma sessão, o drift deixa de te reconhecer
    this.login = function(email, password){
        var token = btoa(email + ':' + password);
    
        return APIService.POST('auth/login', null, {headers: {'Authorization': 'Basic ' + token}, skipAuthorization: true}).then(function(response){
            LocalStorageService.setItem('Auth', 'User', response.data.user);
            LocalStorageService.setItem('Auth', 'Token', response.data.token);
            LocalStorageService.setItem('Auth', 'LoginTimeStamp', new moment());
    
            drift.on('ready', function(){
                drift.identify(response.data.user.id.toString(), {
                    email: response.data.user.email,
                    name: response.data.user.name,
                    companyId: response.data.user.company.id
                });
            });

            Raven.setUserContext({
                id: response.data.user.id,
                email: response.data.user.email,
                name: response.data.user.name,
                companyId: response.data.user.company.id,
                companyName: response.data.user.company.name
            });

            return response.data.user;
        }).catch(function(error){
            return $q.reject(error);
        });
    };
    
    this.logout = function(){
        return APIService.POST('auth/logout', null).finally(function(){
            _this.setUser(null);
    
            drift.on('ready', function(){
                drift.reset();
            });

            Raven.setUserContext();
        });
    };
    
    this.validateToken = function(){
        if(LocalStorageService.getItem('Auth', 'Token')){
            return APIService.GET('auth/me').then(function(response){
                _this.setUser(response.data);
                drift.on('ready', function(){
                    drift.identify(response.data.id.toString(), {
                        email: response.data.email,
                        name: response.data.name,
                        companyId: response.data.company.id
                    });

                    Raven.setUserContext({
                        id: response.data.id,
                        email: response.data.email,
                        name: response.data.name,
                        companyId: response.data.company.id,
                        companyName: response.data.company.name
                    });
                });
                return response;
            }).catch(function(){
                return $q.reject('The current user token is invalid/outdated.');
            });
        } else return $q.reject('The current user isn\'t logged in.');
    };

    this.register = function(data){
        return APIService.POST('auth/users/create', data, {skipAuthorization: true}).catch(function(response){
            return $q.reject(response.data);
        });
    };

    this.activateAccount = function(data){
        return APIService.POST('auth/users/activate', data, {skipAuthorization: true});
    };

    this.requestPasswordReset = function(data){
        return APIService.POST('auth/password/reset', data, {skipAuthorization: true});
    };

    this.resetPassword = function(data){
        return APIService.POST('auth/password/reset/confirm', data, {skipAuthorization: true});
    };

    this.acceptInvitation = function(data){
        return APIService.POST('invites/accept', data, {skipAuthorization: true}).catch(function(response){
            return $q.reject(response.data);
        });
    };

    this.sendInvitation = function(data){
        return APIService.POST('invites', data).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao enviar um convite.', error);
            return $q.reject(error);
        });
    };

    this.revokeInvitation = function(id){
        return APIService.POST('invites/' + id + '/revoke', {}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao cancelar um convite.', error);
            return $q.reject(error);
        });
    };

    this.getUser = function(){
        return LocalStorageService.getItem('Auth', 'User');
    };

    this.setUser = function(user){
        if(user){
            user = LocalStorageService.setItem('Auth', 'User', user);
            $rootScope.$broadcast('auth:user:update', user);
        } else{
            LocalStorageService.removeItem('Auth', 'User');
            LocalStorageService.removeItem('Auth', 'Token');
            LocalStorageService.removeItem('Auth', 'LoginTimeStamp');
        }
    };
}]);

elysium.service('AuthenticationInterceptor', ['LocalStorageService', '$rootScope', '$q', function(LocalStorageService, $rootScope, $q){
    return{
        'request': function(config){
            var user = LocalStorageService.getItem('Auth', 'User');
            var token = LocalStorageService.getItem('Auth', 'Token');
            
            if(!config.skipAuthorization && user && token){
                config.headers['Authorization'] = 'Token ' + token;
            }
            
            return config;
        },
        'responseError': function(request){
            if(request.status === 401 && !request.config.url.endsWith('/auth/login/')) $rootScope.$emit('service:authentication:unauthorized');
            return $q.reject(request);
        }
    }
}]);

elysium.config(['$httpProvider', function($httpProvider){
    $httpProvider.interceptors.push('AuthenticationInterceptor');
}]);
