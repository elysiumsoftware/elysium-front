elysium.controller('CreateBillModalController', ['$scope', '_Data', '$state', 'PaymentService', function($scope, _Data, $state, PaymentService){
    var _this = this;

    this.data = {
        company: _Data.company
    };

    PaymentService.getBillPrice().then(function(response){
        _this.data.billPrice = response;
    });


    this.createBill = function(){
        PaymentService.createBrill().then(function(response){
            if(response.status === 200) $state.go('authentication.billpaid');
            else $state.go('authentication.billpending');

            $scope.$hide();
        });
    };
}]);
