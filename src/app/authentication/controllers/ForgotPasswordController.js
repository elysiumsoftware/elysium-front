elysium.controller('ForgotPasswordController', ['AuthenticationService', '$rootScope', '$q', function(AuthenticationService, $rootScope, $q){
    var _this = this;
    
    this.data = {
        form: {},
        isLoading: true,
        success: null
    };
    
    // Checks if the Main controller already verified the current user token on bootstrap
    $q.when($rootScope.bootstrapped, function(){
        _this.data.isLoading = false;
    });
    
    
    this.requestPasswordReset = function(){
        AuthenticationService.requestPasswordReset({email: _this.data.form.email}).then(function(){
            _this.data.success = true;
        }).catch(function(){
            _this.data.success = false;
        });
    }
}]);
