elysium.controller('RegisterController', ['AuthenticationService', '$state', '$scope', '$rootScope', '$q', function(AuthenticationService, $state, $scope, $rootScope, $q){
    var _this = this;
    
    this.data = {
        form: {},
        isLoading: true,
        errors: null,
        states: [
            {code: "AC", name: "Acre"},
            {code: "AL", name: "Alagoas"},
            {code: "AM", name: "Amazonas"},
            {code: "AP", name: "Amapá"},
            {code: "BA", name: "Bahia"},
            {code: "CE", name: "Ceará"},
            {code: "DF", name: "Distrito Federal"},
            {code: "ES", name: "Espírito Santo"},
            {code: "GO", name: "Goiás"},
            {code: "MA", name: "Maranhão"},
            {code: "MT", name: "Mato Grosso"},
            {code: "MS", name: "Mato Grosso do Sul"},
            {code: "MG", name: "Minas Gerais"},
            {code: "PA", name: "Pará"},
            {code: "PB", name: "Paraíba"},
            {code: "PR", name: "Paraná"},
            {code: "PE", name: "Pernambuco"},
            {code: "PI", name: "Piauí"},
            {code: "RJ", name: "Rio de Janeiro"},
            {code: "RN", name: "Rio Grande do Norte"},
            {code: "RO", name: "Rondônia"},
            {code: "RS", name: "Rio Grande do Sul"},
            {code: "RR", name: "Roraima"},
            {code: "SC", name: "Santa Catarina"},
            {code: "SE", name: "Sergipe"},
            {code: "SP", name: "São Paulo"},
            {code: "TO", name: "Tocantins"}
        ]
    };
    
    // Checks if the Main controller already verified the current user token on bootstrap
    $q.when($rootScope.bootstrapped, function(){
        _this.data.isLoading = false;
    });
    
    
    this.createAccount = function(){
        _this.data.isLoading = true;
        _this.data.errors = null;

        if(!_this.data.passwordConfirmation){
            angular.element('#register-input-password-confirm').focus();
            _this.data.errors = {password: true};
            _this.data.isLoading = false;
            return;
        }

        if(!_this.data.form.state){
            $scope.$broadcast('authentication:register:form:state:focus');
            _this.data.errors = {state: true};
            _this.data.isLoading = false;
            return;
        }

        AuthenticationService.register(_this.data.form).then(function(){
            $state.go('authentication.registercomplete', {email: _this.data.form.email});
        }).catch(function(error){
            _this.data.isLoading = false;
            _this.data.errors = error;
        });
    }
}]);
