elysium.controller('ResetPasswordController', ['AuthenticationService', '$stateParams', '$rootScope', '$q', function(AuthenticationService, $stateParams, $rootScope, $q){
    var _this = this;
    
    this.data = {
        form: {},
        isLoading: true,
        errors: null,
        success: null
    };
    
    // Checks if the Main controller already verified the current user token on bootstrap
    $q.when($rootScope.bootstrapped, function(){
        _this.data.isLoading = false;
    });
    
    
    this.resetPassword = function(){
        _this.data.isLoading = true;
        _this.data.errors = null;
        _this.data.success = null;

        if(!_this.data.passwordConfirmation){
            angular.element('#reset-password-input-password-confirmation').focus();
            _this.data.errors = {password: true};
            _this.data.isLoading = false;
            return;
        }

        AuthenticationService.resetPassword({uid: $stateParams.uid, token: $stateParams.token, new_password: _this.data.form.new_password}).then(function(){
            _this.data.success = true;
        }).catch(function(){
            _this.data.isLoading = false;
            _this.data.success = false;
        });
    }
}]);
