elysium.controller('ConfirmRegistrationController', ['AuthenticationService', '$q', '$rootScope', '$stateParams', function(AuthenticationService, $q, $rootScope, $stateParams){
    var _this = this;

    this.data = {
        success: null
    };

    $q.when($rootScope.bootstrapped, function(){
        AuthenticationService.activateAccount({uid: $stateParams.uid, token: $stateParams.token}).then(function(){
            _this.data.success = true;
        }).catch(function(){
            _this.data.success = false;
        });
    });
}]);
