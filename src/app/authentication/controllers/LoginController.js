elysium.controller('LoginController', ['AuthenticationService', '$state', '$scope', '$rootScope', '$q', '$stateParams', 'ToastService', '$timeout',function(AuthenticationService, $state, $scope, $rootScope, $q, $stateParams, ToastService, $timeout){
    var _this = this;
    
    this.data = {
        form: {},
        isLoading: true,
        errorMessage: null,
        sref: $stateParams.sref,
        params: $stateParams.params
    };
    
    // Checks if the Main controller already verified the current user token on bootstrap
    $q.when($rootScope.bootstrapped, function(){
        _this.data.isLoading = false;
    });

    if(_this.data.sref){
        _this.data.errorMessage = 'Sua sessão expirou. Por favor, faça login novamente para continuar.'
    }


    this.login = function(){
        // Disables the submit button while we wait for the login response
        _this.data.isLoading = true;
        _this.data.errorMessage = null;
        
        AuthenticationService.login(_this.data.form.email, _this.data.form.password).then(function(user){
            if(user.company.status !== 'inactive') $state.go(_this.data.sref || 'app.processos.list', _this.data.params);
            else{
                if(user.is_owner) $state.go('authentication.company');
                else{
                    _this.data.errorMessage = 'Sua empresa está inativa. Notifique o administrador da sua empresa para resolver esse problema.';
                    _this.data.isLoading = false;
                }
            }
        }).catch(function(error){
            if(error.data.detail === 'Invalid username/password.'){
                _this.data.errorMessage = 'Usuário ou senha inválido(s).'
            } else if(error.data.detail === 'User inactive or deleted.'){
                _this.data.errorMessage = 'Você ainda não ativou sua conta, ou o administrador da sua empresa a bloqueou.'
            }
            
            // Re-enables the submit button
            _this.data.isLoading = false;
        });
    }
}]);
