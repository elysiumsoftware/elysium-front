elysium.service('PublicationService', ['APIService', '$q', 'AuthenticationService', '$rootScope', 'ToastService', 'PublicationFactory', 'PublicationNameFactory', function(APIService, $q, AuthenticationService, $rootScope, ToastService, PublicationFactory, PublicationNameFactory){
    var _this = this;

    this.get = function(date){
        var query = date ? '/?d=' + date.format('YYYY-MM-DD') : '';
        return APIService.GET('publications' + query).then(function(response){
            var _publication_list = [];

            angular.forEach(response.data, function(publication){
                _publication_list.push(new PublicationFactory(publication))
            });

            return _publication_list;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar as publicações desse dia.', error);
            return $q.reject(error);
        });
    };

    this.getArchived = function(){
        return APIService.GET('publications/archived').then(function(response){
            var _publicationList = [];

            angular.forEach(response.data, function(publication){
                _publicationList.push(new PublicationFactory(publication))
            });

            return _publicationList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas publicações arquivadas.', error);
            return $q.reject(error);
        });
    };

    this.archive = function(publication){
        return APIService.POST('publications/' + publication.id + '/archive', null).then(function(response){
            return new PublicationFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao arquivar uma publicação.', error);
            return $q.reject(error);
        });
    };

    this.unarchive = function(publication){
        return APIService.POST('publications/' + publication.id + '/unarchive', null).then(function(response){
            return new PublicationFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao desarquivar uma publicação.', error);
            return $q.reject(error);
        });
    };

    this.generateVariations = function(name){
        return APIService.POST('publications/get_variations', {term: name}).then(function(response){
            return response.data;
        }).catch(function(error){
            if(error.data.details && error.data.details === 'Your company needs to register a payment profile to access publications.'){
                var _user = AuthenticationService.getUser();
                if(_user.is_owner) ToastService.showMessage('Empresas no período de testes precisam selecionar um plano e cadastrar um método de pagamento para cadastrar nomes para buscas nos diários oficiais.<br><br>Você pode fazer isso clicando na aba "Empresa e Pagamentos" nas configurações.');
                else ToastService.showMessage('Empresas no período de testes precisam selecionar um plano e cadastrar um método de pagamento para cadastrar nomes para buscas nos diários oficiais.<br><br>Fale com o(a) administrador(a) da sua empresa para realizar esse processo.');
            } else ToastService.showError('Algo deu errado ao gerar variações para o seu nome.', error);
            return $q.reject(error);
        });
    };

    this.createPublicationName = function(data){
        return APIService.POST('publications/names', data).then(function(response){
            return new PublicationNameFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao salvar seu nome para buscas no diário.', error);
            return $q.reject(error);
        });
    };

    this.getPublicationName = function(){
        return APIService.GET('publications/name').then(function(response){
            if(response.data.length) return new PublicationNameFactory(response.data[0]);
            else return null;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas configurações de publicações.', error);
            return $q.reject(error);
        });;
    };

    this.deletePublicationName = function(id){
        return APIService.DELETE('publications/names/' + id).then(function(response){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir seu nome das buscas.', error);
            return $q.reject(error);
        });
    };

    this.deleteVariation = function(id){
        return APIService.DELETE('publications/variations/' + id).then(function(response){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao remover uma variação do seu nome.', error);
            return $q.reject(error);
        });
    };

    this.deleteException = function(id){
        return APIService.DELETE('publications/blocking_terms/' + id).then(function(response){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao remover uma exceção ao seu nome.', error);
            return $q.reject(error);
        });
    };

    this.createVariation = function(publicationName, variation){
        return APIService.POST('publications/names/' + publicationName.id + '/variation', {term: variation}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar uma variação do seu nome.', error);
            return $q.reject(error);
        });
    };

    this.createException = function(publicationName, exception){
        return APIService.POST('publications/names/' + publicationName.id + '/blocking_term', {term: exception}).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar uma exceção ao seu nome.', error);
            return $q.reject(error);
        });
    };
    
    this.updateException = function(exception){
        return APIService.PATCH('publications/blocking_terms/' + exception.id, exception).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar uma exceção ao seu nome.', error);
            return $q.reject(error);
        });
    }
}]);
