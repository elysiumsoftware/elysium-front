elysium.controller('PublicationDetailsModalController', ['_Data', '$scope', '$rootScope', 'PublicationService', function(_Data, $scope, $rootScope, PublicationService){
    var _this = this;

    this.data = {
        publication: _Data.publication,
        expired: _Data.publication.created_at.clone().add(30, 'days') < moment(),
        expiration: _Data.publication.created_at.clone().add(30, 'days').fromNow(),
        iterFunction: _Data.iterFunction,
        next: _Data.next,
        previous: _Data.previous
    };


    this.archive = function(){
        PublicationService.archive(_this.data.publication).then(function(publication){
            $rootScope.$broadcast('publications:publication:updated', _this.data.publication, publication);
            _this.data.publication = publication;
        });
    };

    this.unarchive = function(){
        PublicationService.unarchive(_this.data.publication).then(function(publication){
            $rootScope.$broadcast('publications:publication:updated', _this.data.publication, publication);
            _this.data.publication = publication;
        });
    };

    this.goToPublication = function(publication){
        _this.data.iterFunction(publication);
        $scope.$hide();
    };
}]);
