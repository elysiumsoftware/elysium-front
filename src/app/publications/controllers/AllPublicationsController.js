elysium.controller('AllPublicationsController', ['AuthenticationService', 'PublicationService', 'ModalService', '$scope', '$stateParams', function(AuthenticationService, PublicationService, ModalService, $scope, $stateParams){
    var _this = this;
    var _now = moment();

    this.data = {
        currentUser: AuthenticationService.getUser(),
        publications: null,
        date: $stateParams.date || _now,
        minDate: _now.clone().subtract(30, 'days'),
        maxDate: _now.clone(),
        loading: false
    };

    PublicationService.get(this.data.date).then(function(response){
        _this.data.publications = response;
    });

    $scope.$on('publications:publication:updated', function(event, oldPublication, newPublication){
        _this.data.publications[_this.data.publications.indexOf(oldPublication)] = newPublication;
    });


    this.getPublications = function(date){
        if(!_this.data.loading){
            _this.data.loading = true;
            PublicationService.get(date).then(function(response){
                _this.data.publications = response;
                _this.data.loading = false;
            });
        }
    };

    this.openDetailsModal = function(publication, index){
        var _next = index < _this.data.filteredPublications.length - 1 ? _this.data.filteredPublications[index + 1] : null;
        var _previous = index !== 0 ? _this.data.filteredPublications[index - 1] : null;

        ModalService.setModal({
            locals: {_Data: {publication: publication, iterFunction: _this.iterPublication, next: _next, previous: _previous}},
            templateUrl: 'app/publications/views/modal/details.html',
            controller: 'PublicationDetailsModalController as PublicationDetailsModal'
        });
    };

    this.onDateChange = function(newDate, oldDate){
        if(!_this.data.loading){
            _this.data.publications = null;

            this.getPublications(newDate);
        }
        else _this.data.date = oldDate;
    };

    this.previousDay = function(){
        if(!_this.data.loading){
            var previousDay = _this.data.date.clone().subtract(1, 'days');

            if(previousDay >= _this.data.minDate){
                _this.data.publications = null;
                _this.data.date = previousDay;

                _this.getPublications(_this.data.date);
            }
        }
    };

    this.nextDay = function(){
        if(!_this.data.loading){
            var nextDay = _this.data.date.clone().add(1, 'days');

            if(nextDay <= _this.data.maxDate){
                _this.data.publications = null;
                _this.data.date = nextDay;

                _this.getPublications(_this.data.date);
            }
        }
    };

    this.iterPublication = function(publication){
        _this.openDetailsModal(publication, _this.data.filteredPublications.indexOf(publication));
    };
}]);
