elysium.controller('ArchivedPublicationsController', ['PublicationService', 'ModalService', '$scope', function(PublicationService, ModalService, $scope){
    var _this = this;

    this.data = {
        publications: null
    };

    PublicationService.getArchived().then(function(response){
        _this.data.publications = response;
    });

    $scope.$on('publications:publication:updated', function(event, oldPublication, newPublication){
        if(!newPublication.archived) _this.data.publications.splice(_this.data.publications.indexOf(oldPublication), 1);
        else _this.data.publications.push(newPublication);
    });


    this.openDetailsModal = function(publication, index){
        var _next = index < _this.data.filteredPublications.length - 1 ? _this.data.filteredPublications[index + 1] : null;
        var _previous = index !== 0 ? _this.data.filteredPublications[index - 1] : null;

        ModalService.setModal({
            locals: {_Data: {publication: publication, iterFunction: _this.iterPublication, next: _next, previous: _previous}},
            templateUrl: 'app/publications/views/modal/details.html',
            controller: 'PublicationDetailsModalController as PublicationDetailsModal'
        });
    };

    this.iterPublication = function(publication){
        _this.openDetailsModal(publication, _this.data.filteredPublications.indexOf(publication));
    };
}]);
