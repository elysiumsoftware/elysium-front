elysium.factory('PublicationFactory', [function(){
    var Publication = function(data){
        this.id = data.id;
        this.created_at = moment(data.created_at);
        this.processes = data.processes;
        this.searched_oab = data.searched_oab;
        this.publication_date = moment(data.publication_date);
        this.availability_date = moment(data.availability_date);
        this.vara = data.vara;
        this.comarca = data.comarca;
        this.orgao = data.orgao;
        this.report_name = data.report_name;
        this.report_acronym = data.report_acronym;
        this.report_domain = data.report_domain;
        this.edition_number = data.edition_number;
        this.publication_content = data.publication_content;
        this.state = data.state;
        this.archived = data.archived;
    };

    return Publication;
}]);
