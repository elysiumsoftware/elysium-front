elysium.factory('PublicationNameFactory', [function(){
    var PublicationName = function(data){
        this.id = data.id;
        this.oab = data.oab;
        this.name = data.name;
        this.variations = data.variations || [];
        this.exceptions = data.blocking_terms || [];
    };

    return PublicationName;
}]);
