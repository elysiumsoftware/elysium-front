elysium.factory('ChartFactory', [function(){
    var Chart = function(map, colors){
        var _this = this;
        
        this.labels = [];
        this.data = [];
        this.colors = colors || null;
        this.timestamp = new moment();
        
        angular.forEach(map, function(value, key){
            if((colors && !value) || (!colors && value) || (colors && value)){
                _this.data.push(value);
                _this.labels.push(key);
            }
        });
    };
    
    
    return Chart;
}]);
