elysium.service('StatService', ['APIService', 'ToastService', '$q', 'ChartFactory', function(APIService, ToastService, $q, ChartFactory){
    var _this = this;
    
    
    _this.getDoneTasks = function(){
        return APIService.GET('process/tasks/status').then(function(response){
            return new ChartFactory(response.data, ['#1ad477', '#ff596a']);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas estatísticas.', error);
            return $q.reject();
        });
    };
    
    _this.getProcessosStatus = function(){
        return APIService.GET('process/processes/status').then(function(response){
            return new ChartFactory(response.data, ['#1ad477', '#ffa319', '#ff596a']);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas estatísticas.', error);
            return $q.reject();
        });
    };
    
    _this.getProcessosMaterias = function(){
        return APIService.GET('process/processes/subjects').then(function(response){
            return new ChartFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar suas estatísticas.', error);
            return $q.reject();
        });
    };
}]);
