elysium.controller('StatsController', ['StatService', '$scope', function(StatService, $scope){
    var _this = this;
    
    StatService.getDoneTasks().then(function(response){
        _this.doneTasks = response;
    }).catch(angular.noop);
    
    StatService.getProcessosStatus().then(function(response){
        _this.processosStatus = response;
    }).catch(angular.noop);
    
    StatService.getProcessosMaterias().then(function(response){
        _this.processosMaterias = response;
    }).catch(angular.noop);
    
    $scope.$on('chart-create', function(){
        jQuery($scope.masonryContainer).masonry('layout');
    });
}]);
