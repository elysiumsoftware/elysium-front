elysium.directive('title', ['$rootScope', '$timeout', function($rootScope, $timeout){
	return {
		link: function() {
			var listener = function(event, toState) {
				$timeout(function() {
						if(toState.data.titulo == 'Início') $rootScope.isInicio = true; else $rootScope.isInicio = false;
						$rootScope.title = (toState.data && toState.data.titulo) ? toState.data.titulo+" - Elysium" : 'Elysium';
						$rootScope.pageTitle = (toState.data && toState.data.titulo) ? toState.data.titulo : ' ';
					});
				};
			$rootScope.$on('$stateChangeSuccess', listener);
		}
	};
}]);
