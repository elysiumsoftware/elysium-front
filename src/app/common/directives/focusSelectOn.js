elysium.directive('focusSelectOn', ['$timeout', function($timeout){
    /**
     * Listens for an event and focuses the current ui-select
     * Code based on various comments at: https://github.com/angular-ui/ui-select/issues/201
     */
    return {
        require: 'uiSelect',
        link: function(scope, element, attrs, $select) {
            scope.$on(attrs.focusSelectOn, function(){
                $timeout(function(){
                    // Open the select and focus
                    $select.activate();
                    
                    // Focus the search field if the select allows multiple values
                    // $select.activate() already focuses non-multiple selects
                    if($select.multiple) $select.setFocus();
                });
            });
        }
    };
}]);
