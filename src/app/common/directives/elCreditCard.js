elysium.directive('elCreditCard', [function(){
    return{
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelController){
            var formatRegex = new RegExp(/.{1,4}/g);
            var visaRegex = new RegExp(/^4/g);
            var masterCardRegex = new RegExp(/^5[1-5]/g);
            var ignoredKeyCodes = [8, 46, 32];

            // Source: https://gist.github.com/ShirtlessKirk/2134376
            var _checkCCNumber = (function (arr) {
                return function (ccNum) {
                    var len = ccNum.length, bit = 1, sum = 0, val;

                    while (len) {
                        val = parseInt(ccNum.charAt(--len), 10);
                        sum += (bit ^= 1) ? arr[val] : val;
                    }

                    return sum && sum % 10 === 0;
                };
            }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]));

            // Adds an el-credit-card class to mark the input container for reference in CSS
            element.parent().addClass('el-credit-card-container');

            // Sets the parser which will update the model/view values and validate the parsed number
            ngModelController.$parsers.push(function(value){
                if(value){
                    var selectionPoint = value.length % 5 === 0 ? element[0].selectionStart + 1 : element[0].selectionStart;
                    var parsed = value.replace(/\s|[a-zA-Z]|\W/g, '');

                    if(parsed.length && (parsed.length <= 2 || parsed.length === 16)){
                        element.parent().removeClass('visa mastercard');

                        if(parsed.match(visaRegex)) element.parent().addClass('visa');
                        else if(parsed.match(masterCardRegex)) element.parent().addClass('mastercard');
                    }
                    if(parsed.length === 16){
                        if(!_checkCCNumber(parsed)) ngModelController.$setValidity('number', false);
                        else ngModelController.$setValidity('number', true);
                    }

                    ngModelController.$setViewValue(parsed.match(formatRegex).join(' '));
                    ngModelController.$render();

                    element[0].selectionStart = selectionPoint;
                    element[0].selectionEnd = selectionPoint;

                    return parsed;
                } else element.parent().removeClass('visa mastercard');
            });

            // Sets the $render function to always show the ngModel.$viewValue
            ngModelController.$render = function(){
                element.val(ngModelController.$viewValue);
            };
        }
    }
}]);
