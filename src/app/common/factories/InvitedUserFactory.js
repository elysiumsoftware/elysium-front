elysium.factory('InvitedUserFactory', ['UserFactory', function(UserFactory){
    var InvitedUser = function(data){
        data.email = data.user_email;
        UserFactory.call(this, data);
        this.dateInvited = new Date(data.sent_date);
        this.status = data.status;
    };

    InvitedUser.prototype = Object.create(UserFactory.prototype);


    InvitedUser.prototype.getStatus = function(){
        switch(this.status){
            case 'PENDING':
                return 'Pendente';
            case 'ACCEPTED':
                return 'Registrado';
        }
    };
    
    return InvitedUser;
}]);

