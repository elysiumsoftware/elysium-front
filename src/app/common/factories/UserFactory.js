elysium.factory('UserFactory', [function(){
    var User = function(data){
        this.id = data.id;
        this.name = data.name;
        this.email = data.email || null;
        this.avatar = data.avatar || null;
        this.company = data.company || null;
        this.user_group = data.user_group || null;
        this.publication_name = data.publication_name || null;
        this.is_owner = data.is_owner || null;
        this.is_active = data.is_active || null;
        this.is_disabled = data.is_disabled || null;
        this.cpf = data.cpf || null;
        this.city = data.city || null;
        this.state = data.state || null;
        this.telephone = data.telephone || null;
        this.is_awaiting_reactivation = data.is_awaiting_reactivation || null;
        this.active_until = data.active_until || null;
    };
    
    User.prototype.isCurrentUser = function(){
        return this.id === JSON.parse(localStorage.getItem('Elysium.Auth.User')).id;
    };
    
    return User;
}]);
