/**
 * Elysium Main Model
 *
 * This service is used to store and serve everything that is pulled from the API
 * Controllers may use it to request cached or fresh data from the API
 */
elysium.service('ModelService', ['APIService', 'ToastService', '$q', 'UserFactory', 'InvitedUserFactory', 'ClienteFactory', 'AdversoFactory', 'AdvogadoAdversoFactory', 'CorrespondenteFactory', 'RecursoFactory', function(APIService, ToastService, $q, UserFactory, InvitedUserFactory, ClienteFactory, AdversoFactory, AdvogadoAdversoFactory, CorrespondenteFactory, RecursoFactory){
    var _this = this;
    
    
    /* Processos */
    _this.getProcessos = function(){
        return APIService.GET('processes').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de processos.', error);
            return $q.reject();
        });
    };
    
    _this.getProcesso = function(id){
        return APIService.GET('processes/' + id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar um processo.', error);
            return $q.reject();
        });
    };
    
    _this.updateProcesso = function(id, data){
        return APIService.PATCH('processes/' + id, data).then(function(response){
            return response.data;
        }).catch(function(error){
            if(error) ToastService.showError('Algo deu errado ao atualizar o processo.', error);
            return $q.reject();
        });
    };
    
    _this.deleteProcesso = function(id){
        return APIService.DELETE('processes/' + id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir o processo.', error);
            return $q.reject();
        });
    };
    
    _this.getProcessoTarefas = function(id){
        return APIService.GET('processes/' + id + '/tasks').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar as tarefas de um processo.', error);
            return $q.reject();
        });
    };
    
    _this.getProcessoMovimentacoes = function(id){
        return APIService.GET('processes/' + id + '/old_updates').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar as movimentações de um processo.', error);
            return $q.reject();
        });
    };
    
    _this.getProcessoRecursos = function(id){
        return APIService.GET('processes/' + id + '/appeals').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar os recursos de um processo.', error);
            return $q.reject();
        });
    };
    
    _this.getRecurso = function(id){
        return APIService.GET('processes/appeals/' + id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar um recurso.', error);
            return $q.reject();
        });
    };
    
    /* Incidentes */
    _this.getIncidentes = function(id){
        return APIService.GET('processes/' + id + '/incidents').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de incidentes.', error);
            return $q.reject();
        });
    };
    
    
    /* Pessoas */
    
    _this.getPessoa = function(id){
        return APIService.GET('pessoas/pessoas/' + id).then(function(response){
            switch(response.data.subtipo){
                case 'cliente':
                    return new ClienteFactory(response.data);
                case 'adverso':
                    return new AdversoFactory(response.data);
                case 'advogado_adverso':
                    return new AdvogadoAdversoFactory(response.data);
                case 'correspondente':
                    return new CorrespondenteFactory(response.data);
            }
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar uma pessoa.', error);
            return $q.reject();
        });
    };
    
    _this.getClientes = function(){
        return APIService.GET('people/clients').then(function(response){
            var clientesList = [];
            
            angular.forEach(response.data, function(cliente){
                clientesList.push(new ClienteFactory(cliente));
            });
            
            return clientesList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar uma pessoa.', error);
            return $q.reject();
        });
    };
    
    _this.getAdversos = function(){
        return APIService.GET('people/adverses').then(function(response){
            var adversosList = [];
            
            angular.forEach(response.data, function(adverso){
                adversosList.push(new AdversoFactory(adverso));
            });
            
            return adversosList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar uma pessoa.', error);
            return $q.reject();
        });
    };
    
    _this.getAdvogadosAdversos = function(){
        return APIService.GET('people/adverse-lawyers').then(function(response){
            var advogadosAdversosList = [];
            
            angular.forEach(response.data, function(advogadoAdverso){
                advogadosAdversosList.push(new AdvogadoAdversoFactory(advogadoAdverso));
            });
            
            return advogadosAdversosList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar uma pessoa.', error);
            return $q.reject();
        });
    };
    
    
    /* Recursos */
    _this.getRecursos = function(id){
        return APIService.GET('processes/' + id + '/appeals').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de recursos.', error);
            return $q.reject();
        });
    };
    
    _this.createRecurso = function(recurso){
        var appeal = {
            process: recurso.processo_do_recurso,
            electronic_process: recurso.processo_eletronico,
            responsible: recurso.responsaveis,
            adverses: recurso.adversos,
            adverse_lawyers: recurso.advogados_do_adverso,
            clients: recurso.clientes,
            notification_date: recurso.data_de_citacao,
            distribution_date: recurso.data_de_distribuicao,
            status_date: recurso.data_de_status,
            process_number: recurso.numero_do_processo,
            comments: recurso.observacoes,
            judicial_organ: recurso.orgao_judicial,
            adverse_position: recurso.posicao_do_adverso,
            client_position: recurso.posicao_do_cliente,
            appeal_type: recurso.tipo_de_acao,
            title: recurso.titulo
        };
        return APIService.POST('processes/appeals', appeal).then(function(response){
            return new RecursoFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar um recurso.', error);
            return $q.reject();
        });
    };
    
    _this.updateRecurso = function(recurso){
        var appeal = {
            electronic_process: recurso.processo_eletronico,
            responsible: recurso.responsaveis,
            adverses: recurso.adversos,
            adverse_lawyers: recurso.advogados_do_adverso,
            clients: recurso.clientes,
            notification_date: recurso.data_de_citacao,
            distribution_date: recurso.data_de_distribuicao,
            status_date: recurso.data_de_status,
            process_number: recurso.numero_do_processo,
            comments: recurso.observacoes,
            judicial_organ: recurso.orgao_judicial,
            adverse_position: recurso.posicao_do_adverso,
            client_position: recurso.posicao_do_cliente,
            appeal_type: recurso.tipo_de_acao,
            title: recurso.titulo
        };
        return APIService.PATCH('processes/appeals/' + recurso.id, toJson(appeal)).then(function(response){
            return new RecursoFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao editar um recurso.', error);
            return $q.reject();
        });
    };
    
    /* Tarefas */
    _this._tarefaId = null;
    
    _this.setTarefa = function(id){
        _this._tarefaId = id;
    };
    
    _this.createTarefa = function(data){
        return APIService.POST('tasks', data).then(function(response){
            // If the current user isn't responsible for the created tarefa, show them a confirmation message
            if(response.data.responsible.indexOf(parseInt(localStorage.getItem('elysium:user:id'))) == -1){
                if(response.data.responsible.length > 1) ToastService.showMessage('A tarefa "' + response.data.title +
                    '" foi delegada para os usuários selecionados.');
                else ToastService.showMessage('A tarefa "' + response.data.title + '" foi delegada para o' +
                    ' usuário selecionado.');
            }
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar a tarefa.', error);
            return $q.reject();
        });
    };
    
    _this.getTarefa = function(id){
        if(id){
            return APIService.GET('tasks/' + id).then(function(response){
                return response.data;
            }).catch(function(error){
                ToastService.showError('Algo deu errado ao carregar a tarefa.', error);
                return $q.reject();
            });
        } else{
            return APIService.GET('tasks/' + _this._tarefaId).then(function(response){
                _this.tarefaId = null;
                return response.data;
            }).catch(function(error){
                ToastService.showError('Algo deu errado ao carregar a tarefa.', error);
                return $q.reject();
            });
        }
    };
    
    _this.updateTarefa = function(id, data){
        return APIService.PATCH('tasks/' + id, data).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar a tarefa.', error);
            return $q.reject();
        });
    };
    
    _this.deleteTarefa = function(id){
        return APIService.DELETE('tasks/' + id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir a tarefa.', error);
            return $q.reject();
        });
    };
    
    
    /* Usuários */
    _this.getUsuarios = function(){
        return APIService.GET('users').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de usuários.', error);
            return $q.reject();
        });
    };
    
    
    /* localStorage */
    
    /**
     * Gets an item from localStorage using 'elysium' as a namespace
     * @param {string} name Name of the item to get
     * @returns {(string|boolean)}  The item's value. 'true' and 'false' are converted to boolean
     */
    _this.getItem = function(name){
        var item = localStorage.getItem('elysium:' + name);
        switch(item){
            case 'false':
                return false;
            case 'true':
                return true;
            default:
                return item;
        }
    };
    
    /**
     * Sets an item on localStorage using 'elysium' as a namespace
     * @param {string} name Name of the item to set
     * @param {string|boolean} value The value to set
     */
    _this.setItem = function(name, value){
        localStorage.setItem('elysium:' + name, value);
    };
    
    /**
     * Removes an item from localStorage under the 'elysium' namespace
     * @param name The name of the item to remove
     */
    _this.removeItem = function(name){
        localStorage.removeItem('elysium:' + name);
    };
    
    /**
     * Gets a configuration option from localStorage using 'elysium:option' as a namespace
     * @param {string} name Name of the configuration to get
     * @returns {(string|boolean)}  The configuration's value. 'true' and 'false' are converted to boolean
     */
    _this.getConfig = function(name){
        return _this.getItem('config:' + name);
    };
    
    /**
     * Sets a configuration option on localStorage using 'elysium:option' as a namespace
     * @param {string} name Name of the option to set
     * @param {string|boolean} value The value to set
     */
    _this.setConfig = function(name, value){
        _this.setItem('config:' + name, value);
    };
    
    /**
     * Removes a configuration option from localStorage under the 'elysium' namespace
     * @param name The name of the option to remove
     */
    _this.removeConfig = function(name){
        localStorage.removeItem('elysium:config:' + name);
    };
    
    
    /* Users and Invites */
    
    /**
     * Gets all invited users, including the ones that accepted their invitations
     * @returns {Promise|Array<InvitedUserFactory>}
     */
    _this.getSentInvitations = function(){
        return APIService.GET('invites').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de usuários convidados', error);
            return $q.reject(error.data);
        });
    };
    
    /**
     * Gets all users from the current user's company
     * @returns {Promise|Array<UserFactory>}
     */
    _this.getUsers = function(){
        return APIService.GET('users').then(function(response){
            var usersArray = [];
            
            angular.forEach(response.data, function(user){
                usersArray.push(new UserFactory(user));
            });
            
            return usersArray;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de usuários', error);
            return $q.reject(error.data);
        });
    };

    /**
     * Gets all detailed users from the current user's company
     * @returns {Promise|Array<UserFactory>}
     */
    _this.getDetailedUsers = function(){
        return APIService.GET('payments/users').then(function(response){
            var usersArray = [];

            angular.forEach(response.data, function(user){
                usersArray.push(new UserFactory(user));
            });

            return usersArray;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de usuários', error);
            return $q.reject(error.data);
        });
    };

    _this.getGroups = function(){
        return APIService.GET('groups').then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de grupos de usuários', error);
            return $q.reject(error.data);
        });
    };
    
    /**
     * Sends an invitation email to the given email
     * @param email The email to send an invitation
     * @param group The group of the account to be invited
     * @returns {Promise|InvitedUserFactory}
     */
    _this.sendInvite = function(email, group){
        return APIService.POST('invites', {user_email: email, group: group}).then(function(response){
            return new InvitedUserFactory(response.data);
        }).catch(function(error){
            if(error.data.user_email && error.data.user_email[0] === 'The given email address is already registered') ToastService.showError('Já existe um usuário no Elysium que utiliza esse endereço de email. Você pode tentar enviar um convite para um endereço de email diferente.');
            if(error.data.non_field_errors && error.data.non_field_errors[0] === 'You can\'t send more than 5 invites at the same time') ToastService.showError('Você não pode enviar mais do que cinco convites por dia. Tente novamente amanhã.');
            else ToastService.showError('Algo deu errado ao enviar um convite para "' + email + '"', error);
            return $q.reject(error.data);
        });
    };
    
    /**
     * Cancels an invitation
     * @param id The id of the invitation to cancel
     * @return {Promise}
     */
    _this.cancelInvite = function(id){
        return APIService.DELETE('invites/' + id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao cancelar um convite', error);
            return $q.reject(error.data);
        });
    };
    
    
    function toJson(object){
        return angular.forEach(object, function(value, key, obj){
            if(value == null) delete obj[key];
        });
    }
}]);
