/**
 * Created by lucas on 26/12/2016.
 */

elysium.service('AuthService', ['APIService', 'ToastService', '$auth', '$q', function(APIService, ToastService, $auth, $q){
    var _this = this;
    
    _this.createAccount = function(data){
        return $auth.signup(data, {skipAuthorization: true}).then(function(response){
            return response.data;
        }).catch(function(error){
            return $q.reject(error.data);
        });
    };
    
    _this.verifyAccount = function(uid, token){
        return APIService.POST('auth/activate', {uid: uid, token: token}, {skipAuthorization: true}).then(function(response){
            return response.data;
        }).catch(function(error){
            return $q.reject();
        });
    };
    
    _this.requestPasswordReset = function(email){
        return APIService.POST('auth/password/reset', {email: email}).then(function(response){
            return response.data;
        }).catch(function(error){
            return $q.reject(error.data);
        });
    };
    
    _this.resetPassword = function(uid, token, password){
        return APIService.POST('auth/password/reset/confirm', {uid: uid, token: token, new_password: password}, {skipAuthorization: true}).then(function(response){
            return response.data;
        }).catch(function(error){
            return $q.reject(error.data);
        });
    };
    
    _this.acceptInvitation = function(data){
        return APIService.POST('convites/convidado', data, {skipAuthorization: true}).then(function(response){
            return response.data;
        }).catch(function(error){
            return $q.reject(error.data);
        });
    };
    
}]);
