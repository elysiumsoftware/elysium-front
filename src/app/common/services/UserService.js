elysium.service('UserService', ['APIService', '$q', 'ToastService', 'UserFactory', function(APIService, $q, ToastService, UserFactory){
    var _this = this;
    
    _this.getUsers = function(data){
        return APIService.GET('users').then(function(response){
            var usersArray = [];
    
            angular.forEach(response.data, function(user){
                usersArray.push(new UserFactory(user));
            });
    
            return usersArray;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar os usuários do Elysium', error);
            return $q.reject(error.data);
        });
    };
}]);
