/**
 * Serviço que faz as requisições gerais com a API
 */
elysium.service('ElysiumAPI', ['$http', 'ngToast', 'APIURL', function($http, ngToast, APIURL){
    var _this = this;

    /**
     * URL padrão da API
     * Só é necessário mudar essa variável caso a API mude de endereço
     * @type {String}
     */
	_this.apiURL = APIURL;

    /**
     * Faz requisições GET à API
     * @param  {String} pagina  Página que deve ser chamada
     * @param  {String} area    Área que deve ser chamada
     * @param  {Object} chamada Dados que devem ser enviados no GET
     * @param  {Boolean} barra  Se true, coloca uma barra "/" no fim da URL
     * @return {Promise}        Retorna uma HttpPromise
     */
	_this.get = function(pagina, area, chamada, barra){
		if(chamada == null){
			if(barra != true){
				return $http.get(_this.apiURL + area + "/" + pagina +"/");
			}
            else{
				return $http.get(_this.apiURL + area + "/" + pagina);
			}
		}
        else{
			if(barra != true){
				return $http.get(_this.apiURL + area + "/" + pagina +"/", chamada);
			}
			else{
				return $http.get(_this.apiURL + area + "/" + pagina, chamada);
			}
		}
	};
	_this.getCompleteUrl = function(url){
		return $http.get(url);
	};
	_this.getUrl = function(url){
		return $http.get(_this.apiURL + url);
	};
	_this.post = function(pagina, area, chamada, barra, opcoes){
		var opcoes = opcoes || {};
		if(barra != true)
		{
			return $http.post(_this.apiURL + area + "/" + pagina +"/", chamada, opcoes);
		}
		else
		{
			return $http.post(_this.apiURL + area + "/" + pagina, chamada, opcoes);
		}
	};
	_this.delete = function(pagina, area, chamada, barra){
		if(chamada == null)
		{
			if(barra != true)
			{
				return $http.delete(_this.apiURL + area + "/" + pagina +"/");
			}
			else
			{
				return $http.delete(_this.apiURL + area + "/" + pagina);
			}
		}
		else
		{
			if(barra != true)
			{
				return $http.delete(_this.apiURL + area + "/" + pagina +"/", chamada);
			}
			else
			{
				return $http.delete(_this.apiURL + area + "/" + pagina, chamada);
			}
		}
	};
	_this.put = function(pagina, area, chamada, barra){
		if(chamada == null)
		{
			if(barra != true)
			{
				return $http.put(_this.apiURL + area + "/" + pagina +"/");
			}
			else
			{
				return $http.put(_this.apiURL + area + "/" + pagina);
			}
		}
		else
		{
			if(barra != true)
			{
				return $http.put(_this.apiURL + area + "/" + pagina +"/", chamada);
			}
			else
			{
				return $http.put(_this.apiURL + area + "/" + pagina, chamada);
			}
		}
	};
	_this.patch = function(pagina, area, chamada, barra){
		if(chamada == null)
		{
			if(barra != true)
			{
				return $http.patch(_this.apiURL + area + "/" + pagina +"/");
			}
			else
			{
				return $http.patch(_this.apiURL + area + "/" + pagina);
			}
		}
		else
		{
			if(barra != true)
			{
				return $http.patch(_this.apiURL + area + "/" + pagina +"/", chamada);
			}
			else
			{
				return $http.patch(_this.apiURL + area + "/" + pagina, chamada);
			}
		}
	};
	_this.getId = function(array, idBusca){
		var i;
		for(i = 0; i < array.length; i++){
			if(array[i].id == idBusca)
				return i;
		}
		return -1;
	}
	_this.showError = function(error, response){
		if(response){
			var status = "";
			switch (response.status){
				case -1:
					status = "Houve um erro geral ao tentar fazer uma requisição aos servidores do Elysium. Verifique sua conexão. Se o erro for nos serviços do Elysium, seremos notificados sobre esse problema e o consertaremos o quanto antes.";
					break;
				case 500:
					status = "Houve um erro interno nos serviços do Elysium. Não se preocupe: nós já fomos notificados e estamos trabalhando para consertar o problema."
					break;
				default:
					status = "Algo deu errado ao tentar se comunicar com os servidores do Elysium. Não temos muitos detalhes atualmente, mas tente novamente em alguns minutos.";
					break;
			}
			ngToast.create({
				className: 'error',
				content: error + '<br><br>' + status + '<br><br>Se o erro persistir, entre em contato com o suporte.'
			});
		}
		else{
			ngToast.create({
				className: 'error',
				content: error
			});
		}
	}
	_this.showMessage = function(message){
		ngToast.create({
			className: 'message',
			content: message
		});
	}
	_this.setOption = function(option, value){
		localStorage.setItem('elysium:' + option, value);
	}
	_this.getOption = function(option){
		var op = localStorage.getItem('elysium:' + option);
		switch (op) {
			case "false":
				return false;
			case "true":
				return true;
			default:
				return op;
		}
	}
	_this.setItem = function(option, value){
		localStorage.setItem(option, value);
	}
	_this.getItem = function(option){
		var op = localStorage.getItem(option);
		switch (op) {
			case "false":
				return false;
			case "true":
				return true;
			default:
				return op;
		}
	}
}]);
