elysium.service('APIService', ['$http', '$q', 'APIURL', function($http, $q, APIURL){
    /**
     * Control variables for model-sensitive HTTP methods
     * Methods that send or modify data server-side must occur no more than once at the same time for the same method
     * These variables are used to check if any request is already being made at the time another one is requested
     *
     * Multiple calls on the same HTTP methods must never be queued, since this may cause multiple unintended
     * serialized calls. Subsequent requests while one is already being made must be discarded.
     */
    var _POST, _PATCH, _DELETE;
    
    
    /**
     * Returns a HttpPromise of a GET on the provided URL
     * This method automatically appends a slash at the URL's end if it's needed
     * @param {string} url - The URL to GET
     * @param {Object} [config] - Optional configuration object
     * @returns {Promise} The promise of the requested data
     */
    this.GET = function(url, config){
        var slash = url.indexOf('?') === -1 ? '/' : '';
        var config = config || {};
    
        return $http({
            method: 'GET',
            url: APIURL + url + slash,
            headers: config.headers,
            skipAuthorization: config.skipAuthorization
        });
    };

    /**
     * Retorna uma Promise de um POST dos dados na url chamada
     * Opcionalmente, pode-se passar um Objeto contendo configurações pro $http
     * @param {String} url URL (sem a raíz do Elysium) da chamada (exemplo: 'pessoas/pessoas/42')
     * @param {*} data Dados a serem enviados na chamada
     * @param {Object} [config]  Objeto contendo configurações para o serviço $http
     * @return {Promise} The promise of the sent data
     */
    this.POST = function(url, data, config){
        if(!_POST){
            var config = config || {};
            
            _POST =  $http({
                method: 'POST',
                url: APIURL + url + '/',
                data: data,
                headers: config.headers,
                skipAuthorization: config.skipAuthorization
            }).catch(function(error){
                return $q.reject(error);
            }).finally(function(){
                _POST = null;
            });
            
            return _POST;
        } else return $q.reject('A POST call is already being made.');
    };

    /**
     * Retorna uma Promise de um PATCH dos dados na url chamada
     * Opcionalmente, pode-se passar um Objeto contendo configurações pro $http
     * @param {string} url URL (sem a raíz do Elysium) da chamada (exemplo: 'pessoas/pessoas/42')
     * @param {*} data Dados a serem enviados na chamada
     * @param {Object} [config]  Objeto contendo configurações para o serviço $http
     */
    this.PATCH = function(url, data, config){
        if(!_PATCH){
            var config = config || {};
            
            _PATCH = $http({
                method: 'PATCH',
                url: APIURL + url + '/',
                data: data,
                headers: config.headers,
                skipAuthorization: config.skipAuthorization
            }).catch(function(error){
                return $q.reject(error);
            }).finally(function(){
                _PATCH = null;
            });

            return _PATCH;
        } else return $q.reject('A PATCH call is already being made.');
    };

    /**
     * Retorna uma Promise de um DELETE na url chamada
     * Opcionalmente, pode-se passar um Objeto contendo configurações pro $http
     * @param {String} url URL (sem a raíz do Elysium) da chamada (exemplo: 'pessoas/pessoas/42')
     * @param {Object} [config]  Objeto contendo configurações para o serviço $http
     */
    this.DELETE = function(url, config){
        if(!_DELETE){
            var config = config || {};
    
            _DELETE = $http({
                method: 'DELETE',
                url: APIURL + url + '/',
                headers: config.headers,
                skipAuthorization: config.skipAuthorization
            }).catch(function(error){
                return $q.reject(error);
            }).finally(function(){
                _DELETE = null;
            });
            
            return _DELETE;
        } else return $q.reject('A DELETE call is already being made.');
    };
}]);
