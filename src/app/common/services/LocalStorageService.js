elysium.factory('LocalStorageService', function(){
    var LocalStorage = {};
    
    LocalStorage.getItem = function(namespace, key){
        if(arguments.length < 2) throw new Error('Missing parameters. Did you set a namespace?');
        
        return angular.fromJson(localStorage.getItem('Elysium.' + namespace + '.' + key));
    };

    LocalStorage.setItem = function(namespace, key, data){
        if(arguments.length < 3) throw new Error('Missing parameters. Did you set a namespace?');

        var json = angular.toJson(data);
        localStorage.setItem('Elysium.' + namespace + '.' + key, json);

        return LocalStorage.getItem(namespace, key);
    };
    
    LocalStorage.removeItem = function(namespace, key){
        if(arguments.length < 2) throw new Error('Missing parameters. Did you set a namespace?');
      
        localStorage.removeItem('Elysium.' + namespace + '.' + key);
    };
    
    return LocalStorage;
});
