/**
 * Elysium Modal Manager
 */
elysium.service('ModalService', ['$modal', '$rootScope', function($modal, $rootScope){
    var _this = this;
    
    var _modal;
    
    
    _this.setModal = function(options){
        _modal = $modal(options);
    };
    
    _this.hideModal = function(){
        if(_modal) _modal.hide();
    };
    
    /**
     * Fires hideModal() on state change, so modals automatically close
     */
    $rootScope.$on('$stateChangeSuccess', function(){
        _this.hideModal();
    });
    
    _this.getModal = function(){
        return _modal;
    };
}]);
