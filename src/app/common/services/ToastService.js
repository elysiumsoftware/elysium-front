elysium.service('ToastService', ['ngToast', function(ngToast){
    var _this = this;

    /**
     * Mostra uma toast notification com a classe 'erro'
     * @param  {String} erro     Mensagem do erro
     * @param  {Object} resposta Objeto com a resposta da Promise que gerou o erro
     */
    _this.showError = function(erro, resposta){
        if(resposta){
			var status = '';
            //  Em geral, é conveniente mostrar uma mensagem de erro genérica
            //  junto com a mensagem de erro original da chamada
			switch (resposta.status){
				case -1:
					status = 'Houve um erro geral ao tentar fazer uma requisição aos servidores do Elysium. Verifique sua conexão. Se o erro for nos serviços do Elysium, seremos notificados sobre esse problema e o consertaremos o quanto antes.';
					break;
				case 500:
					status = 'Houve um erro interno nos serviços do Elysium. Não se preocupe: nós já fomos notificados e estamos trabalhando para consertar o problema.';
					break;
				default:
					status = 'Algo deu errado ao tentar se comunicar com os servidores do Elysium. Não temos muitos detalhes atualmente, mas tente novamente em alguns minutos.';
					break;
			}
			ngToast.create({
				className: 'error',
				content: erro + '<br><br>' + status + '<br><br>Se o erro persistir, entre em contato com o suporte.'
			});
		}
		else{
			ngToast.create({
				className: 'error',
				content: erro
			});
		}
    };

    /**
     * Mostra uma toast notification com uma mensagem simples
     * @param  {String} mensagem Mensagem a ser mostrada para o usuário
     */
    _this.showMessage = function(mensagem){
		ngToast.create({
			className: 'message',
			content: mensagem
		});
	}

}]);
