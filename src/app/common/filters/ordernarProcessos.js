elysium.filter("odernarProcessos", function(){
	return function(processos){
		if(processos){
			var retorno = [],
				comData = [],
				semData = [];
			for (var i = 0; i < processos.length; i++){
				processos[i].idProcesso = processos[i].id;
				if(processos[i].data_ultima_movimentacao) comData.push(processos[i]);
				else semData.push(processos[i]);
			}
			if(comData.length) comData.sort().reverse();
			if(semData.length) semData.sort().reverse();
			return comData.concat(semData);
		}
		else return;
	};
});
