elysium.filter("data", ['$filter', function($filter){
	return function(input){
		return $filter('date')(input, "dd' de 'LLLL' de 'yyyy', às 'HH':'mm");
	};
}]);
