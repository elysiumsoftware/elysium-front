elysium.filter("tempoAtras", function(){
	return function(input){
		return input ? moment(input).fromNow() : null;
	};
});
