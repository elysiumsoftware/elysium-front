elysium.filter("elBillStatus", function(){
    return function(input){
        if(input === 'paid') return 'Pago';
        else if(input === 'pending') return 'Pendente';
        else if(input === 'canceled') return 'Cancelado';
        else if(input === 'scheduled') return 'Agendado';
        else if(input === 'review') return 'Em revisão';
        else return 'Indeterminado';
    };
});
