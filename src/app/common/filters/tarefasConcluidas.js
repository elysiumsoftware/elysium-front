elysium.filter("tarefasConcluidas", function(){
	return function(array, opcao){
		if(array){
			if(opcao){
				var retorno = [];
				for (var i = 0; i < array.length; i++){
					if(array[i].pending) retorno.push(array[i]);
				}
				return retorno;
			}
			else return array;
		}
		else return;
	};
});
