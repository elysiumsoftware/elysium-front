elysium.filter('urlConfiavel', ['$sce', function($sce){
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);
