elysium.controller('AgendaCrtl', ['$scope', 'ElysiumAPI', '$state', '$rootScope', 'ModalService', function($scope, ElysiumAPI, $state, $rootScope, ModalService){
	var _this = this;

	if(ElysiumAPI.getOption("agenda:ocultarTarefasConcluidas") == null) _this.ocultarTarefasConcluidas = true;
	else _this.ocultarTarefasConcluidas = ElysiumAPI.getOption("agenda:ocultarTarefasConcluidas");

	_this.dataDeHoje = moment().startOf('day');
	_this.diaDeHoje = _this.dataDeHoje.date();
	_this.nomeDeHoje = _this.dataDeHoje.format('dddd');

	_this.dataDeAmanha = moment().add(1, 'days').startOf('day');
	_this.diaDeAmanha = _this.dataDeAmanha.date();
	_this.nomeDeAmanha = _this.dataDeAmanha.format('dddd');

	_this.dataDeDepoisDeAmanha = moment().add(2, 'days').startOf('day');


	$scope.carregarTarefas = function(){
		ElysiumAPI.get("range/"+_this.dataDeHoje.format('YYYY-MM-DD')+"/"+_this.dataDeAmanha.format('YYYY-MM-DD') + '/', 'tasks', null, true).then(function(response){
			_this.hoje = response.data;
			for (var i = 0; i < _this.hoje.length; i++) {
				if(moment(_this.hoje[i].start) < _this.dataDeHoje && moment(_this.hoje[i].finish) > _this.dataDeAmanha){
					_this.hoje[i].diaInteiro = true;
				}
			}
		},function(response){
			ElysiumAPI.showError('Algo deu errado ao tentar preencher a agenda', response);
		});

		ElysiumAPI.get("range/"+_this.dataDeAmanha.format('YYYY-MM-DD')+"/"+_this.dataDeDepoisDeAmanha.format('YYYY-MM-DD') + '/', 'tasks', null, true).then(function(response){
			_this.amanha = response.data;
			for (var i = 0; i < _this.amanha.length; i++) {
				if(moment(_this.amanha[i].start) < _this.dataDeHoje && moment(_this.amanha[i].finish) > _this.dataDeAmanha){
					_this.amanha[i].diaInteiro = true;
				}
			}
		},function(response){
			ElysiumAPI.showError('Algo deu errado ao tentar preencher a agenda', response);
		});
	};

	$scope.carregarTarefas();
	
	$scope.ocultarTarefasConcluidas = function(){
		if(_this.ocultarTarefasConcluidas){
			ElysiumAPI.setOption("agenda:ocultarTarefasConcluidas", false);
			_this.ocultarTarefasConcluidas = false;
		}
		else{
			ElysiumAPI.setOption("agenda:ocultarTarefasConcluidas", true);
			_this.ocultarTarefasConcluidas = true;
		}
	};
	
	_this.abrirTarefa = function(id){
	    console.log(2);
        ModalService.setModal({
            locals: {
                _Data: {
                    task: id
                }
            },
            templateUrl: 'app/tasks/views/modals/task-details.html',
            controller: 'TaskDetailsModalController as TaskDetailsModal'
        });
    };

	// Atualiza a agenda sempre que uma nova tarefa é criada, atualizada ou removida
	$rootScope.$on('tasks:task:create', function(){
		$scope.carregarTarefas();
	});
    $rootScope.$on('tasks:task:delete', function(){
        $scope.carregarTarefas();
    });
    $rootScope.$on('tasks:task:update', function(){
        $scope.carregarTarefas();
    });
    $rootScope.$on('tasks:task:drag', function(){
        $scope.carregarTarefas();
    });
}]);
