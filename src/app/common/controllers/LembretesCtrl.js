elysium.controller('LembretesCtrl', ['$scope', 'APIService', '$state', '$rootScope', 'ToastService', function($scope, APIService, $state, $rootScope, ToastService){
	var _this = this;

	_this.lembretes = [];
	_this.adicionarLembrete = false;
	_this.criandoLembrete = false;

	// Pega todos os lembretes do servidor e cria um cache
	($scope.getLembretes = function(){
		APIService.GET('reminders').then(function(response){
			_this.lembretes = response.data;
			for (var i = 0; i < _this.lembretes.length; i++) {
				_this.lembretes[i].cache = _this.lembretes[i].text;
			}
		},function(response){
			ToastService.showError('Algo deu errado ao carregar os lembretes', response);
		});
	})();

	// Abre ou fecha a caixa do novo lembrete
	$scope.alternarNovoLembrete = function(){
		_this.adicionarLembrete = !_this.adicionarLembrete;
	};

	// Cria um lembrete novo e o coloca no início da lista de lembretes
	$scope.criarLembrete = function(event, novoLembrete){
		if(!novoLembrete){
			$scope.alternarNovoLembrete();
			return;
		}

		if(event && event.shiftKey) return;
		if(event && event.charCode === 13) event.preventDefault();

		if(!_this.criandoLembrete){
			_this.criandoLembrete = true;
            novoLembrete.trim();
			var dados = {
				text: novoLembrete,
				color: 'GREEN'
			};

			APIService.POST('reminders', dados).then(function(response){
				$scope.alternarNovoLembrete();
				_this.lembretes.splice(0,0,{
					text: novoLembrete,
					cache: novoLembrete,
					created_at: response.data.creation_date,
					color: 'GREEN',
					id: response.data.id
				});
				_this.novoLembrete = "";
				_this.criandoLembrete = false;
			},function(response){
				_this.criandoLembrete = false;
				ToastService.showError('Algo deu errado ao criar um lembrete', response);
			});
		}
	};

	// Exclui o lembrete de id selecionado e recarrega a lista de lembretes
	$scope.excluirLembrete = function(id){
		APIService.DELETE('reminders/' + id).then(function(){
			$scope.getLembretes();
		},function(response){
			ToastService.showError('Algo deu errado ao excluir um lembrete', response);
		});
	};

	// Atualiza o lembrete no servidor se ele foi modificado
	$scope.atualizarLembrete = function(event, lembrete){
		if(lembrete.text !== lembrete.cache){
			lembrete.salvando = true;
			APIService.PATCH('reminders/' + lembrete.id, {text: lembrete.text}).then(function(){
				$scope.getLembretes();
			},function(response){
				lembrete.salvando = false;
				ToastService.showError('Algo deu errado ao atualizar um lembrete', response);
			});
		}
	};

	// Muda a cor de um lembrete e atualiza no servidor
	$scope.mudarCor = function(lembrete, cor){
		var corCache = lembrete.color;
		lembrete.color = cor;
		APIService.PATCH('reminders/' + lembrete.id, {color:cor}).then(function(){},function(response){
			lembrete.color = corCache;
			ToastService.showError('Algo deu errado ao mudar a cor de um lembrete', response);
		});
	};
}]);
