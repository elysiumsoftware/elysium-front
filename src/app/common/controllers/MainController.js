elysium.controller('MainController', ['Pessoas', '$scope', '$document', '$q', '$state', '$rootScope', 'ngToast', '$timeout', 'ModalService', 'ModelService', 'AuthenticationService', function(Pessoas, $scope, $document, $q, $state, $rootScope, ngToast, $timeout, ModalService, ModelService, AuthenticationService){
    $rootScope.hideModal = ModalService.hideModal;

    $rootScope.data = {};
    
    // TODO: criar um sistema pra fazer login depois do token expirar enquanto o usuário usa o sistema (parecido com o modal de login do wordpress)
    $rootScope.bootstrapped = AuthenticationService.validateToken().then(function(response){
        if(response.data.company.status === 'inactive'){
            if(response.data.is_owner) $state.go('authentication.company');
            else{
                AuthenticationService.logout().then(function(){
                    $state.go('authentication.login');
                });
            }
        } else{
            $rootScope.data.authenticatedOnBootstrap = true;
            if($state.is('authentication.login')) $state.go('app.processos.list');
        }
    }).catch(function(error){
        $q.reject(error);
    });
    
    // Executa o código sempre que o cliente mudar o $state
    $rootScope.$on('$stateChangeSuccess', function(event, toState){
        // Volta ao topo da página e limpa as notificações sempre que o $state mudar
        $document.scrollTop(0, 200);
        ngToast.dismiss();
        
        // Se o usuário não estivar no login ao mudar o $state, realizar o teste de login
        // if(!$state.includes('login') && !$rootScope.loginVerificado) verificarLogin();
        
        // Track page change
        drift.on('ready', function(){
            drift.page(toState.data.titulo);
        });
    });
}]);
