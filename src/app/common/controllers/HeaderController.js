elysium.controller('HeaderController', ['NotificationService', 'LocalStorageService', function(NotificationService, LocalStorageService){
    var _this = this;
    
    this.data = {
        user: LocalStorageService.getItem('Auth', 'User')
    };
    this.pages = [
        // {
        //     title: 'Inicial',
        //     sref: 'app.home',
        //     icon: 'home'
        // },
        {
            title: 'Processos',
            sref: 'app.processos.list',
            includes: 'app.processos',
            icon: 'processo'
        },
        {
            title: 'Atividades',
            sref: 'app.tasks',
            icon: 'calendar'
        },
        {
            title: 'Pessoas',
            sref: 'app.person.list',
            includes: 'app.person',
            icon: 'person'
        },
        {
            title: 'Publicações',
            sref: 'app.publications.all',
            includes: 'app.publications',
            icon: 'publications'
        }
        // {
        //     title: 'Relatórios',
        //     sref: 'app.stats',
        //     icon: 'stats'
        // }
    ];


    (_this.getNotificationCount = function(){
        NotificationService.getNotificationCount().then(function(count){
            _this.data.notificationCount = count;
        });
    })();
    
    
}]);
