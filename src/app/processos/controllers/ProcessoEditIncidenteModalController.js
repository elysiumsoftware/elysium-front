elysium.controller('ProcessoEditIncidenteModalController', ['ProcessoService', '_Data', 'ModelService', '$scope', '$rootScope', function(ProcessoService, _Data, ModelService, $scope, $rootScope){
    var _this = this;

    _this.incidente = angular.copy(_Data.incidente);
    _this.currentIncidente = _Data.incidente;


    _this.updateIncidente = function(){
        ProcessoService.updateIncidente(_this.incidente).then(function(updatedIncidente){
            $rootScope.$broadcast('processo:incidente:update', _this.currentIncidente, updatedIncidente);
            $scope.$hide();
        });
    };
}]);
