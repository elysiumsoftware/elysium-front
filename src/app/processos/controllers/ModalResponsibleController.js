elysium.controller('ModalResponsibleController', ['$scope', '$q', 'ModelService', 'ProcessoService', function($scope, $q, ModelService, ProcessoService){
    var _this = this;
    
    
    _this.pane = 'list';
    
    _this.openAddPane = function(){
        _this.pane = 'add';
        
        if(!_this.users){
            $q.when($scope.ProcessoDetails.processoPromise, function(){
                ModelService.getUsers().then(function(response){
                    _this.users = response;
            
                    for(var u = 0; u < _this.users.length; u++){
                        for(var r = 0; r < $scope.ProcessoDetails.processo.responsaveis.length; r++){
                            if(angular.equals(_this.users[u], $scope.ProcessoDetails.processo.responsaveis[r])){
                                _this.users.splice(_this.users.indexOf(_this.users[u--]), 1);
                                break;
                            }
                        }
                    }
                });
            });
        }
    };
    
    _this.openListPane = function(){
        _this.pane = 'list';
    };
    
    _this.addResponsible = function(user){
        var responsibleList = [];
        angular.forEach($scope.ProcessoDetails.processo.responsaveis, function(user){
            responsibleList.push(user.id);
        });
        
        responsibleList.push(user.id);
        
        ProcessoService.updateProcesso($scope.ProcessoDetails.processo, {responsible: responsibleList}).then(function(response){
            $scope.ProcessoDetails.processo.responsaveis.push(user);
            _this.users.splice(_this.users.indexOf(user), 1);
        });
    };
    
    _this.removeResponsible = function(user){
        var responsibleList = [];
        
        angular.forEach($scope.ProcessoDetails.processo.responsaveis, function(responsible){
            if(responsible.id != user.id) responsibleList.push(responsible.id);
        });
        
        ProcessoService.updateProcesso($scope.ProcessoDetails.processo, {responsible: responsibleList}).then(function(response){
            $scope.ProcessoDetails.processo.responsaveis.splice($scope.ProcessoDetails.processo.responsaveis.indexOf(user), 1);
            _this.users.push(user);
        });
    };
}]);
