elysium.controller('UpdateProcessController', ['$stateParams', 'ProcessService', 'ModalService', 'UserService', 'PersonService', '$state', 'ModelService', '$scope', 'AuthenticationService', function($stateParams, ProcessService, ModalService, UserService, PersonService, $state, ModelService, $scope, AuthenticationService){
    var _this = this;
    
    
    /**
     * Model initialization
     */
    // Data used in the form view
    _this.data = {
        process: {},
        states: [
            {code: "AC", name: "Acre"},
            {code: "AL", name: "Alagoas"},
            {code: "AM", name: "Amazonas"},
            {code: "AP", name: "Amapá"},
            {code: "BA", name: "Bahia"},
            {code: "CE", name: "Ceará"},
            {code: "DF", name: "Distrito Federal"},
            {code: "ES", name: "Espírito Santo"},
            {code: "GO", name: "Goiás"},
            {code: "MA", name: "Maranhão"},
            {code: "MT", name: "Mato Grosso"},
            {code: "MS", name: "Mato Grosso do Sul"},
            {code: "MG", name: "Minas Gerais"},
            {code: "PA", name: "Pará"},
            {code: "PB", name: "Paraíba"},
            {code: "PR", name: "Paraná"},
            {code: "PE", name: "Pernambuco"},
            {code: "PI", name: "Piauí"},
            {code: "RJ", name: "Rio de Janeiro"},
            {code: "RN", name: "Rio Grande do Norte"},
            {code: "RO", name: "Rondônia"},
            {code: "RS", name: "Rio Grande do Sul"},
            {code: "RR", name: "Roraima"},
            {code: "SC", name: "Santa Catarina"},
            {code: "SE", name: "Sergipe"},
            {code: "SP", name: "São Paulo"},
            {code: "TO", name: "Tocantins"}
        ],
        partPositions:[
            'Autor',
            'Réu',
            'Reclamante',
            'Reclamado',
            'Requerente',
            'Requerido',
            'Terceiro',
            'Apelante',
            'Apelado',
            'Agravante',
            'Agravado',
            'Recorrente',
            'Recorrido'
        ],
        successProbabilities: [
            {
                label: 'Muito provável (~100%)',
                value: 'VERY_PROBABLE'
            },
            {
                label: 'Provável (~75%)',
                value: 'PROBABLE'
            },
            {
                label: 'Possível (~50%)',
                value: 'POSSIBLE'
            },
            {
                label: 'Remota (~25%)',
                value: 'REMOTE'
            },
            {
                label: 'Inviável (~0%)',
                value: 'IMPOSSIBLE'
            }
        ],
        instanceTypes: [
            'Primeira',
            'Segunda',
            'Originária',
            'Especial'
        ],
        phaseTypes: [
            'Conhecimento',
            'Execução',
            'Cautelar'
        ],
        processTypes: [
            'Judicial',
            'Extrajudicial'
        ],
        riteTypes: [
            'Administrativo',
            'Cautelar',
            'Comum',
            'Especial'
        ],
        subjectTypes: [
            'Administrativa',
            'Ambiental',
            'Bancária',
            'Cível',
            'Empresarial',
            'Constitucional',
            'Consumidor',
            'Criminal',
            'Eleitoral',
            'Família',
            'Internacional',
            'Previdenciária',
            'Trabalhista',
            'Tributária'
        ],
        userName: AuthenticationService.getUser().name.split(' ')[0],
        userId: AuthenticationService.getUser().id,
        processId: $stateParams.id
    };
    
    
    /**
     * Form initialization
     */
    ProcessService.getProcess($stateParams.id).then(function(process){
        _this.data.process = angular.copy(process);
        
        if(_this.data.process.distribution_date) _this.data.process.distribution_date = new moment(_this.data.process.distribution_date).utc();
        if(_this.data.process.notification_date) _this.data.process.notification_date = new moment(_this.data.process.notification_date).utc();
        
        _this.data.process.state = _this.data.process.getStateCode();
    
        // Converts responsible from objects to single ids
        if(process.responsible && process.responsible.length && typeof(process.responsible[0]) === 'object'){
            _this.data.process.responsible = [];
            
            angular.forEach(process.responsible, function(responsible, key){
                _this.data.process.responsible[key] = responsible.id;
            })
        }
    
        // Converts clients from objects to single ids
        if(process.clients && process.clients.length && typeof(process.clients[0]) === 'object'){
            _this.data.process.clients = [];
        
            angular.forEach(process.clients, function(client, key){
                _this.data.process.clients[key] = client.id;
            })
        }
    
        // Converts adverses from objects to single ids
        if(process.adverses && process.adverses.length && typeof(process.adverses[0]) === 'object'){
            _this.data.process.adverses = [];
        
            angular.forEach(process.adverses, function(adverse, key){
                _this.data.process.adverses[key] = adverse.id;
            })
        }
    
        // Converts adverse lawyers from objects to single ids
        if(process.adverse_lawyers && process.adverse_lawyers.length && typeof(process.adverse_lawyers[0]) === 'object'){
            _this.data.process.adverse_lawyers = [];
        
            angular.forEach(process.adverse_lawyers, function(adverse_lawyer, key){
                _this.data.process.adverse_lawyers[key] = adverse_lawyer.id;
            })
        }
    
        // Deletes null variables sent by the backend
        for(var property in _this.data.process){
            if(_this.data.process[property] === null || _this.data.process[property] === undefined) delete _this.data.process[property];
        }
        
        _this.data.processCopy = angular.copy(_this.data.process);
    });
    UserService.getUsers().then(function(response){
        _this.data.users = response;
    });
    PersonService.getClients().then(function(response){
        _this.data.clients = response;
    });
    PersonService.getAdverses().then(function(response){
        _this.data.adverses = response;
    });
    PersonService.getAdverseLawyers().then(function(response){
        _this.data.adverseLawyers = response;
    });
    
    
    /**
     * Updates a process in the backend using values from the view's form
     */
    _this.updateProcess = function(){
        // Validate fields that ngForm can't validade on it's own
        // Specifically, fields created by ui-select
        if(!_this.data.process.state){
            $scope.$broadcast('app:processos:update:form:state:focus');
            return;
        } else if(!_this.data.process.responsible.length){
            $scope.$broadcast('app:processos:update:form:responsible:focus');
            return;
        } else if(_this.data.process.distribution_date && (!(_this.data.process.distribution_date instanceof moment) || !_this.data.process.distribution_date.isValid())){
            $scope.UpdateProcessForm.distribution_date.$setValidity('invalid', false);
            angular.element('#form-distribution_date').focus();
            return;
        } else if(_this.data.process.notification_date && (!(_this.data.process.notification_date instanceof moment) || !_this.data.process.notification_date.isValid())){
            $scope.UpdateProcessForm.notification_date.$setValidity('invalid', false);
            angular.element('#form-notification_date').focus();
            return;
        }
        
        if(_this.data.process.distribution_date && _this.data.process.distribution_date.isValid()) $scope.UpdateProcessForm.distribution_date.$setValidity('valid', true);
        
        // Creates a copy of the submitted process
        // Makes it easier to update values without messing with form binding
        var _process = angular.copy(_this.data.process),
            _copy = _this.data.processCopy;
        
        
        // Checks if the current dates are valid Moment dates and if they need to be sent to the server
        // If the process starts with any of these two dates, and then any of them are deleted, we need to send null
        // If the user inserts any non-date string in any date field, the field will be an invalid Moment
        //      object, which needs to be deleted instead of formatting it to YYYY-MM-DD
        if(_process.notification_date && _process.notification_date.isValid()) _process.notification_date = moment(_process.notification_date).format("YYYY-MM-DD");
            else if(!_process.notification_date && _copy.notification_date) _process.notification_date = null;
            else delete _process.notification_date;
        if(_process.distribution_date && _process.distribution_date.isValid()) _process.distribution_date = moment(_process.distribution_date).format("YYYY-MM-DD");
            else if(!_process.distribution_date && _copy.distribution_date) _process.distribution_date = null;
            else delete _process.distribution_date;
        
        //
        if(!_process.cause_value) _process.cause_value = null;
        if(!_process.provisioned_value) _process.provisioned_value = null;
        
        delete _process.last_update;
        delete _process.connections;
        delete _process.created_at;
        delete _process.update_date;
        
        // Format dates to YYYY-MM-DD
        // Currently, the backend won't accept ISO dates
        if(_process.distribution_date) _process.distribution_date = moment(_process.distribution_date).format("YYYY-MM-DD");
        if(_process.notification_date) _process.notification_date = moment(_process.notification_date).format("YYYY-MM-DD");

        // Formats process number to only contain numbers
        if(_process.process_number) _process.process_number = _process.process_number.replace(/[^\d]/g, '');
        
        // Updates the process and sends the user to it's page
        ProcessService.updateProcess(_process).then(function(response){
            $state.go('app.processos.details', {id: response.id});
        });
    };
    
    /**
     * Sets the current user as responsible for this process
     */
    _this.setUserAsResponsible = function(){
        var _user = AuthenticationService.getUser().id;
        
        if(_this.data.process.responsible.indexOf(_user) === -1) _this.data.process.responsible.push(_user);
    };
    
    /**
     * Sets the current date as value to a specific date field
     * @param {String} field - The form field to set: distribution or notification
     */
    _this.setToday = function(field){
        if(field === 'distribution') _this.data.process.distribution_date = new moment();
        else if(field === 'notification') _this.data.process.notification_date = new moment();
    };
    
    /**
     * Opens a modal window to add a new client inline
     * @param {String} type - The type of the person to add: client, adverse or adverse_lawyer
     */
    _this.openCreatePersonModal = function(type){
        ModalService.setModal(
            {
                locals: {_Data: {type: type, callback: addPerson}},
                scope: $scope,
                templateUrl: 'app/pessoas/views/modals/create-person.html',
                controller: 'CreatePersonModalController as CreatePersonModal',
                autoClose: true
            }
        );
    };
    
    
    /**
     * Function to be used as a callback to the create person modal
     * @param {ClientFactory | AdverseFactory | AdverseLawyerFactory} person - The person just created
     * @param {String} type - The type of the person to add: client, adverse or adverse_lawyer
     */
    function addPerson(person, type){
        if(type === 'client'){
            _this.data.clients.push(person);
            _this.data.process.clients ? _this.data.process.clients.push(person.id) : _this.data.process.clients = [person.id];
        }
        else if(type === 'adverse'){
            _this.data.adverses.push(person);
            _this.data.process.adverses ? _this.data.process.adverses.push(person.id) : _this.data.process.adverses = [person.id];
        }
        else if(type === 'adverse_lawyer'){
            _this.data.adverseLawyers.push(person);
            _this.data.process.adverse_lawyers ? _this.data.process.adverse_lawyers.push(person.id) : _this.data.process.adverse_lawyers = [person.id];
        }
    }
}]);
