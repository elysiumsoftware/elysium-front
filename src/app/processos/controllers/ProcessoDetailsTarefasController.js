elysium.controller('ProcessoDetailsTarefasController', ['ProcessoService', '$scope', '$q', 'ModalService', 'ModelService', function(ProcessoService, $scope, $q, ModalService, ModelService){
    var _this = this;
    

    $q.when($scope.ProcessoDetails.processoPromise, function(){
        ProcessoService.getTarefas($scope.ProcessoDetails.processo).then(function(response){
            _this.tarefas = response;
        });
    });
    
    $scope.$on('tasks:task:delete', function(){
        ProcessoService.getTarefas($scope.ProcessoDetails.processo).then(function(response){
            _this.tarefas = response;
        });
    });
    
    $scope.$on('tasks:task:update', function(){
        ProcessoService.getTarefas($scope.ProcessoDetails.processo).then(function(response){
            _this.tarefas = response;
        });
    });
    
    $scope.$on('tasks:task:create', function(){
        ProcessoService.getTarefas($scope.ProcessoDetails.processo).then(function(response){
            _this.tarefas = response;
        });
    });
    
    
    _this.openTarefa = function(tarefa){
        ModalService.setModal({
            locals: {
                _Data: {
                    task: tarefa.id
                }
            },
            templateUrl: 'app/tasks/views/modals/task-details.html',
            controller: 'TaskDetailsModalController as TaskDetailsModal'
        });
    };
    
    _this.openAddTarefaModal = function(processo){
        ModalService.setModal({
            locals: {
                _Data: {
                    process: processo.id
                }
            },
            templateUrl: 'app/tasks/views/modals/create-task.html',
            controller: 'CreateTaskModalController as CreateTaskModal',
            backdrop: 'static'
        });
    };
}]);
