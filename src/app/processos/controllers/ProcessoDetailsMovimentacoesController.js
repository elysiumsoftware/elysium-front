/** @namespace $scope.ProcessoDetails */
elysium.controller('ProcessoDetailsMovimentacoesController', ['ProcessoService', '$q', '$scope', 'ModelService', 'ModalService', 'ProcessService', function(ProcessoService, $q, $scope, ModelService, ModalService, ProcessService){
    var _this = this;

    this.data = {
        emptyOldUpdates: false
    };
    
    $q.when($scope.ProcessoDetails.processoPromise, function(){
        ProcessService.getUpdates($scope.ProcessoDetails.processo.id).then(function(response){
            _this.data.updates = response;
        });

        ProcessoService.getMovimentacoes($scope.ProcessoDetails.processo).then(function(response){
            if(response.length){
                _this.movimentacoesInfo = response[0];
                _this.movimentacoes = response[0].movimentacoes;

                if($scope.ProcessoDetails.processo.has_updates){
                    var notificacoes = angular.copy($scope.ProcessoDetails.processo.notificacoes);
                    notificacoes.splice(notificacoes.indexOf(Number(ModelService.getItem('user:id'))), 1);
                    ProcessoService.updateProcesso($scope.ProcessoDetails.processo, {updates: notificacoes}).then(function(response){
                        $scope.ProcessoDetails.processo = response;
                    });
                }
            } else _this.data.emptyOldUpdates = true;
        });
    });

    $scope.$on('process:update:created', function(event, update){
        _this.data.updates.push(update);
    });

    $scope.$on('process:update:deleted', function(event, update){
        _this.data.updates.splice(_this.data.updates.indexOf(update), 1);
    });


    this.openCreateUpdateModal = function(){
        ModalService.setModal({
            locals: {_Data: {process: $scope.ProcessoDetails.processo}},
            templateUrl: 'app/processos/views/modal-create-update.html',
            controller: 'ProcessoAddUpdateModalController as ProcessoAddUpdateModal',
            backdrop: 'static'
        });
    };

    this.openDeleteUpdateModal = function(update){
        ModalService.setModal({
            locals: {_Data: {update: update}},
            templateUrl: 'app/processos/views/modal-delete.html',
            controller: 'ProcessosDeleteModalController as ProcessosDeleteModal',
            autoClose: true
        });
    };
}]);
