elysium.controller('ProcessoAddRecursoModalController', ['ProcessoService', '_Data', '$q', 'ModelService', '$scope', '$rootScope', 'ModalService', function(ProcessoService, _Data, $q, ModelService, $scope, $rootScope, ModalService){
    var _this = this;

    _this.partPositions = [
        'Autor',
        'Réu',
        'Reclamante',
        'Reclamado',
        'Requerente',
        'Requerido',
        'Terceiro',
        'Apelante',
        'Apelado',
        'Agravante',
        'Agravado',
        'Recorrente',
        'Recorrido'
    ];
    
    _this.recurso = {
        processo_do_recurso: _Data.processo.id,
        numero_do_processo: _Data.processo.numero_do_processo,
        processo_eletronico: true,
        is_public: true,
        data_de_distribuicao: null,
        data_de_citacao: null,
        responsaveis: [],
        clientes: [],
        adversos: [],
        advogados_do_adverso: []
    };
    
    $q.all({
        clientes: ModelService.getClientes(),
        adversos: ModelService.getAdversos(),
        advogadosAdversos: ModelService.getAdvogadosAdversos()
    }).then(function(responses){
        _this.clientes = responses.clientes;
        _this.adversos = responses.adversos;
        _this.advogadosAdversos = responses.advogadosAdversos;
    });
    
    
    _this.createRecurso = function(){
        if(_this.recurso.data_de_citacao) _this.recurso.data_de_citacao = moment(_this.recurso.data_de_citacao).format('YYYY-MM-DD');
        if(_this.recurso.data_de_distribuicao) _this.recurso.data_de_distribuicao = moment(_this.recurso.data_de_distribuicao).format('YYYY-MM-DD');
        
        ModelService.createRecurso(_this.recurso).then(function(recurso){
            $rootScope.$broadcast('processo:recurso:create', recurso);
            $scope.$hide();
        });
    };
    
    /**
     * Sets the current date as value to a specific date field
     * @param {String} field - The form field to set: data_de_distribuicao or data_de_citacao
     */
    _this.setToday = function(field){
        if(field === 'data_de_distribuicao') _this.recurso.data_de_distribuicao = new moment();
        else if(field === 'data_de_citacao') _this.recurso.data_de_citacao = new moment();
    };
    
    /**
     * Opens a modal window to add a new client inline
     * @param {String} type - The type of the person to add: client, adverse or adverse_lawyer
     */
    _this.openCreatePersonModal = function(type){
        ModalService.setModal(
            {
                locals: {_Data: {type: type, callback: addPerson}},
                scope: $scope,
                templateUrl: 'app/pessoas/views/modals/create-person.html',
                controller: 'CreatePersonModalController as CreatePersonModal',
                autoClose: true
            }
        );
    };
    
    
    /**
     * Function to be used as a callback to the create person modal
     * @param {ClientFactory | AdverseFactory | AdverseLawyerFactory} person - The person just created
     * @param {String} type - The type of the person to add: client, adverse or adverse_lawyer
     */
    function addPerson(person, type){
        if(type === 'client'){
            _this.clientes.push({id: person.id, nome: person.name});
            _this.recurso.clientes.push(person.id);
        }
        else if(type === 'adverse'){
            _this.adversos.push({id: person.id, nome: person.name});
            _this.recurso.adversos.push(person.id);
        }
        else if(type === 'adverse_lawyer'){
            _this.advogadosAdversos.push({id: person.id, nome: person.name});
            _this.recurso.advogados_do_adverso.push(person.id);
        }
    }
}]);
