/**
 * Created by lucas on 26/01/2017.
 */
elysium.controller('PopoverChangeStatusController', ['ProcessoService', '$scope', function(ProcessoService, $scope){
    var _this = this;
    
    
    _this.changeStatus = function(processo, status){
        ProcessoService.updateProcesso(processo, {process_status: status}).then(function(response){
            processo.status_do_processo = response.status_do_processo;
            $scope.$hide();
        });
    }
}]);
