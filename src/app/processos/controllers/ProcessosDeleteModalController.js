/**
 * Created by lucas on 27/01/2017.
 */
elysium.controller('ProcessosDeleteModalController', ['ProcessoService', '_Data', '$rootScope', '$scope', 'ProcessService', function(ProcessoService, _Data, $rootScope, $scope, ProcessService){
    var _this = this;
    
    
    if(_Data.processo){
        _this.processo = _Data.processo;
        _this.type = 'processo';
    }
    else if(_Data.recurso){
        _this.recurso = _Data.recurso;
        _this.type = 'recurso';
    }
    else if(_Data.incidente){
        _this.incidente = _Data.incidente;
        _this.type = 'incidente';
    }
    else if(_Data.conexao){
        _this.conexao = _Data.conexao;
        _this.type = 'conexão';
    }
    else if(_Data.documento){
        _this.documento = _Data.documento;
        _this.type = 'documento';
    }
    else if(_Data.update){
        _this.update = _Data.update;
        _this.type = 'movimentação'
    }
    
    
    _this.delete = function(){
        if(_this.processo){
            ProcessoService.deleteProcesso(_this.processo.id).then(function(){
                $rootScope.$broadcast('processo:delete', _this.processo);
                $scope.$hide();
            });
        }
        else if(_this.recurso){
            ProcessoService.deleteRecurso(_this.recurso).then(function(){
                $rootScope.$broadcast('processo:recurso:delete', _this.recurso);
                $scope.$hide();
            });
        }
        else if(_this.incidente){
            ProcessoService.deleteIncidente(_this.incidente).then(function(){
                $rootScope.$broadcast('processo:incidente:delete', _this.incidente);
                $scope.$hide();
            });
        }
        else if(_Data.conexao){
            $scope.ProcessoDetails.processo.apensos.splice($scope.ProcessoDetails.processo.apensos.indexOf(_this.conexao.id), 1);
            ProcessoService.updateConexoes($scope.ProcessoDetails.processo).then(function(response){
                $scope.ProcessoDetails.processo = response;
                $scope.$hide();
            });
        }
        else if(_this.documento){
            ProcessoService.deleteDocumento(_this.documento).then(function(){
                $rootScope.$broadcast('processo:documento:delete', _this.documento);
                $scope.$hide();
            });
        }
        else if(_this.update){
            ProcessService.deleteUpdate(_this.update.id).then(function(){
                $rootScope.$broadcast('process:update:deleted', _this.documento);
                $scope.$hide();
            });
        }
    }
}]);
