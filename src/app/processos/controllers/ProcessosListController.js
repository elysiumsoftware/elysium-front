elysium.controller('ProcessosListController', ['ProcessoService', 'ModalService', '$popover', '$modal', '$scope', function(ProcessoService, ModalService, $popover, $modal, $scope){
    var _this = this;
    
    _this.listFilters = {};
    
    /**
     * Initializes the view by getting the processos list
     */
    ProcessoService.getProcessos().then(function(response){
        _this.list = response;
    });
    
    /**
     * Watches for processos deletion, and removes them from the list
     */
    $scope.$on('processo:delete', function(event, processo){
        _this.list.splice(_this.list.indexOf(processo), 1);
    });
    
    
    /**
     * Prepares and opens the popover meant to change a processo's status
     * @param event jQuery click event
     * @param processo Processo to change the status
     */
    _this.openStatusPopover = function(event, processo){
        var scope = $scope.$new();
        scope.processo = processo;
        
        var popover = $popover(angular.element(event.currentTarget), {scope: scope, autoClose: true, templateUrl: 'app/processos/views/popover-change-status.html', container: 'body', placement: 'left', trigger: 'manual'});
        popover.$promise.then(popover.show);
    };
    
    /**
     * Opens a modal meant to delete a processo
     * @param processo The processo to delete
     */
    _this.openDeleteModal = function(processo){
        ModalService.setModal({locals: {_Data: {processo: processo}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal', autoClose: true});
    }
}]);
