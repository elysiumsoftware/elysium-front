elysium.controller('ProcessoIncidenteModalController', ['_Data', 'ModalService', '$scope', function(_Data, ModalService, $scope){
    var _this = this;
    
    
    _this.incidente = _Data.incidente;
    
    $scope.$on('processo:incidente:delete', function(){
        $scope.$hide();
    });
    
    $scope.$on('processo:incidente:update', function(event, incidente, updatedIncidente){
        _this.incidente = updatedIncidente;
    });
    
    
    _this.openDeleteIncidenteModal = function(){
        ModalService.setModal({locals: {_Data: {incidente: _this.incidente}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal', autoClose: true});
    };
    
    _this.openEditIncidenteModal = function(){
        ModalService.setModal({locals: {_Data: {incidente: _this.incidente}}, templateUrl: 'app/processos/views/modal-edit-incidente.html', controller: 'ProcessoEditIncidenteModalController as ProcessoEditIncidenteModal'});
    };
}]);
