elysium.controller('ProcessoAddConexaoModalController', ['ProcessoService', 'ConexaoFactory', '$rootScope', '$scope', function(ProcessoService, ConexaoFactory, $rootScope, $scope){
    var _this = this;
    
    
    ProcessoService.getProcessos().then(function(response){
        // Removes the current Processo from the list
        for(var i = 0; i < response.length; i++){
            if($scope.ProcessoDetails.processo.id === response[i].id){
                response.splice(i, 1);
                break;
            }
        }
        
        // Removes current Processos' connections from the list
        if($scope.ProcessoDetails.processo.apensos){
            for(var a = 0; a < $scope.ProcessoDetails.processo.apensos.length; a++){
                for(var p = 0; p < response.length; p++){
                    if(response[p].id === $scope.ProcessoDetails.processo.apensos[a]){
                        response.splice(p, 1);
                        break;
                    }
                }
            }
        }
        
        _this.processos = response;
    });
    
    
    _this.addConexao = function(processo){
        if($scope.ProcessoDetails.processo.apensos) $scope.ProcessoDetails.processo.apensos.push(processo.id);
        else {
            $scope.ProcessoDetails.processo.apensos = [];
            $scope.ProcessoDetails.processo.apensos.push(processo.id);
        }
    
        ProcessoService.updateConexoes($scope.ProcessoDetails.processo).then(function(response){
            $scope.ProcessoDetails.processo = response;
            $scope.$hide();
        });
    };
}]);
