elysium.controller('ProcessoDetailsDocumentosController', ['ProcessoService', '$q', '$scope', 'ModalService', 'Upload', '$rootScope', '$state', function(ProcessoService, $q, $scope, ModalService, Upload, $rootScope, $state){
    var _this = this;
    
    _this.uploadingFiles = [];
    _this.aborted = false;
    _this.modal = false;
        
    $q.when($scope.ProcessoDetails.processoPromise, function(){
        ProcessoService.getDocumentos($scope.ProcessoDetails.processo).then(function(response){
            _this.documentos = response;
        });
    });
    
    $scope.$on('processo:documento:delete', function(event, documento){
        _this.documentos.splice(_this.documentos.indexOf(documento), 1);
    });
    
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
        if(Upload.isUploadInProgress() && !_this.aborted){
            _this.nextState = {
                state: toState,
                params: toParams
            };
            
            event.preventDefault();
            if(!_this.modal) ModalService.setModal({templateUrl: 'app/processos/views/modal-abort-upload.html', scope: $scope, autoClose: true});
        }
    });
    
    window.addEventListener("beforeunload", function (e) {
        if(Upload.isUploadInProgress() && !_this.aborted){
            var confirmationMessage = "Se você sair antes de terminar seus envios de arquivos, dados podem ser perdidos.";
    
            (e || window.event).returnValue = confirmationMessage;
            return confirmationMessage;
        }
    });
    
    
    _this.abortUploadsModal = function(){
        if(_this.uploadingFiles){
            angular.forEach(_this.uploadingFiles, function(upload){
                upload.aborted = true;
                upload.abort();
            });
        }
        
        _this.aborted = true;
        
        $state.go(_this.nextState.state.name, _this.nextState.params);
    };
    
    _this.abortUpload = function(upload){
        upload.aborted = true;
        upload.abort();
        
        _this.uploadingFiles.splice(_this.uploadingFiles.indexOf(upload), 1);
    };
    
    _this.uploadFiles = function(files, invalidFiles){
        ProcessoService.createDocumentos(files, invalidFiles, _this.documentos, _this.uploadingFiles, $scope.ProcessoDetails.processo);
    };
    
    _this.openDeleteModal = function(documento){
        ModalService.setModal({locals: {_Data: {documento: documento}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal', autoClose: true});
    };
}]);
