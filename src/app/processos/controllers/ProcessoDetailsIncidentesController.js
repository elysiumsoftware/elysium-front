elysium.controller('ProcessoDetailsIncidentesController', ['ProcessoService', '$q', '$scope', 'ModalService', function(ProcessoService, $q, $scope, ModalService){
    var _this = this;
    
    
    $q.when($scope.ProcessoDetails.processoPromise, function(){
        ProcessoService.getIncidentes($scope.ProcessoDetails.processo).then(function(response){
            _this.incidentes = response;
        });
    });
    
    $scope.$on('processo:incidente:create', function(event, incidente){
        _this.incidentes.push(incidente);
    });
    
    $scope.$on('processo:incidente:delete', function(event, incidente){
        _this.incidentes.splice(_this.incidentes.indexOf(incidente), 1);
    });
    
    $scope.$on('processo:incidente:update', function(event, incidente, updatedIncidente){
        _this.incidentes[_this.incidentes.indexOf(incidente)] = updatedIncidente;
    });
    
    
    _this.openIncidenteModal = function(incidente){
        ModalService.setModal({locals: {_Data: {incidente: incidente}}, templateUrl: 'app/processos/views/modal-incidente.html', controller: 'ProcessoIncidenteModalController as ProcessoIncidenteModal', autoClose: true});
    };
    
    _this.openAddIncidenteModal = function(processo){
        ModalService.setModal({locals: {_Data: {processo: processo}}, templateUrl: 'app/processos/views/modal-create-incidente.html', controller: 'ProcessoAddIncidenteModalController as ProcessoAddIncidenteModal', backdrop: 'static'});
    };
    
    _this.openDeleteIncidenteModal = function(incidente){
        ModalService.setModal({locals: {_Data: {incidente: incidente}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal', autoClose: true});
    };
    
    _this.openEditIncidenteModal = function(incidente){
        ModalService.setModal({locals: {_Data: {incidente: incidente}}, templateUrl: 'app/processos/views/modal-edit-incidente.html', controller: 'ProcessoEditIncidenteModalController as ProcessoEditIncidenteModal'});
    };
}]);
