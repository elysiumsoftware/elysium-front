elysium.controller('ProcessoDetailsRecursosController', ['ProcessoService', '$q', '$scope', 'ModalService', function(ProcessoService, $q, $scope, ModalService){
    var _this = this;
    
    
    $q.when($scope.ProcessoDetails.processoPromise, function(){
        ProcessoService.getRecursos($scope.ProcessoDetails.processo).then(function(response){
            _this.recursos = response;
        });
    });
    
    $scope.$on('processo:recurso:create', function(event, recurso){
        _this.recursos.push(recurso);
    });
    
    $scope.$on('processo:recurso:delete', function(event, recurso){
        _this.recursos.splice(_this.recursos.indexOf(recurso), 1);
    });
    
    $scope.$on('processo:recurso:update', function(event, recurso, updatedRecurso){
        _this.recursos[_this.recursos.indexOf(recurso)] = updatedRecurso;
    });
    
    
    _this.openRecursoModal = function(recurso){
        ModalService.setModal({locals: {_Data: {recurso: recurso}}, templateUrl: 'app/processos/views/modal-recurso.html', controller: 'ProcessoRecursoModalController as ProcessoRecursoModal', autoClose: true});
    };
    
    _this.openAddRecursoModal = function(processo){
        ModalService.setModal({locals: {_Data: {processo: processo}}, templateUrl: 'app/processos/views/modal-create-recurso.html', controller: 'ProcessoAddRecursoModalController as ProcessoAddRecursoModal', backdrop: 'static'});
    };
    
    _this.openDeleteRecursoModal = function(recurso){
        ModalService.setModal({locals: {_Data: {recurso: recurso}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal', autoClose: true});
    };
    
    _this.openEditRecursoModal = function(recurso){
        ModalService.setModal({locals: {_Data: {recurso: recurso}}, templateUrl: 'app/processos/views/modal-edit-recurso.html', controller: 'ProcessoEditRecursoModalController as ProcessoEditRecursoModal'});
    };
}]);
