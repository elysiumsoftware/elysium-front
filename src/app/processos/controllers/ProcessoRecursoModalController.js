elysium.controller('ProcessoRecursoModalController', ['_Data', 'ModalService', '$scope', function(_Data, ModalService, $scope){
    var _this = this;
    
    
    _this.recurso = _Data.recurso;
    
    $scope.$on('processo:recurso:delete', function(event, recurso){
        $scope.$hide();
    });
    
    $scope.$on('processo:recurso:update', function(event, recurso, updatedRecurso){
        _this.recurso = updatedRecurso;
    });
    
    
    _this.openDeleteRecursoModal = function(){
        ModalService.setModal({locals: {_Data: {recurso: _this.recurso}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal', autoClose: true});
    };
    
    _this.openEditRecursoModal = function(){
        ModalService.setModal({locals: {_Data: {recurso: _this.recurso}}, templateUrl: 'app/processos/views/modal-edit-recurso.html', controller: 'ProcessoEditRecursoModalController as ProcessoEditRecursoModal'});
    };
}]);
