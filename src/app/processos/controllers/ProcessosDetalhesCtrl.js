elysium.controller('ProcessosDetalhesCtrl', ['ElysiumAPI', '$stateParams', '$scope', '$state', 'Upload', '$timeout', '$modal', 'ModalService', 'ModelService', '$rootScope', 'ProcessoService', function(ElysiumAPI, $stateParams, $scope, $state, Upload, $timeout, $modal, ModalService, ModelService, $rootScope, ProcessoService){
	var _this = this;
	_this.tab = 'processo';
	_this.recursoTab = 'detalhes';
	_this.incidenteTab = 'detalhes';

	// Configurações do selectize
	_this.usuariosConfig = {
		valueField: 'id',
		labelField: 'name',
		highlight: false,
		searchField: ['name','email']
	};
	_this.clientesConfig = {
		valueField: 'id',
		labelField: 'nome',
		highlight: false,
		searchField: ['nome']
	};
	_this.adversosConfig = {
		valueField: 'id',
		labelField: 'nome',
		highlight: false,
		searchField: ['nome']
	};
	_this.advogadosConfig = {
		valueField: 'id',
		highlight: false,
		labelField: 'nome',
		searchField: ['nome']
	};

	// Tarefas
    _this.carregandoTarefas = false;
	
	// Conexões
	_this.carregandoConexoes = false;
	_this.fimDaListaDeConexoes = false;
	_this.carregandoProcessos = false;

	// Incidentes
	_this.carregandoIncidentes = false;
	_this.incidente = {
		responsaveis: []
	};

	// Recursos
	_this.carregandoRecursos = false;
	_this.recurso = {
		processo_eletronico: true,
		clientes: [],
		adversos: [],
		advogado_do_adverso: [],
		responsaveis: []
	};

	// Movimentações
	_this.movimentacoesStatus = "inicial";
    
	ElysiumAPI.get("processos/"+$stateParams.id, 'processos').then(function(response){
	    _this.dados = response.data;
		_this.recurso.numero_do_processo = _this.dados.numero_do_processo;

		// Cria a lista de usuários
	    ElysiumAPI.getUrl('users/users/?all=true').then(function(response){
	        _this.usuarios = response.data;
	    }, function(response){
			ElysiumAPI.showError('Não foi possível carregar a lista de Usuários', response);
	    });
		// Cria a lista de clientes
	    ElysiumAPI.getUrl('pessoas/clientes/?all=true').then(function(response){
	        _this.clientes = response.data;
	    }, function(response){
			ElysiumAPI.showError('Erro ao carregar a lista de Clientes', response);
	    });
	    // Cria a lista de adversos
	    ElysiumAPI.getUrl('pessoas/adversos/?all=true').then(function(response){
	        _this.adversos = response.data;
	    }, function(response){
			ElysiumAPI.showError('Erro ao carregar a lista de Adversos', response);
	    });
		// Cria a lista de advogados adversos
		ElysiumAPI.getUrl('pessoas/advogadosadversos/?all=true').then(function(response){
			_this.advogados = response.data;
		}, function(response){
			ElysiumAPI.showError('Erro ao carregar a lista de Advogados Adversos', response);
		});
	},function(response){
	    ElysiumAPI.showError('Não foi possível carregar esse processo', response);
	});

	/* Tarefas */
	$scope.carregarTarefas = function(force){
	    _this.tab = 'tarefas';
	    
	    if((!_this.tarefas && !_this.carregandoTarefas) || force){
            _this.carregandoTarefas = true;
            ElysiumAPI.get("processos/" + $stateParams.id + "/tarefas", 'processos').then(function(response){
                _this.tarefas = response.data;
            }, function(){
                ElysiumAPI.showError('Algo deu errado ao carregar a lista de tarefas', response);
            });
        }
    };
	
	$scope.exibirTarefa = function(tarefa){
        ModelService.setTarefa(tarefa.id);
        ModalService.setModal({title: tarefa.assunto, content: '', templateUrl: 'templates/modal/atividades-tarefa.html'});
    };
	
	$scope.adicionarTarefa = function(){
        ModalService.setModal({locals: {_Config: {processo: _this.dados.id}}, title: 'Nova Tarefa', content: '', backdrop: 'static', controller: 'NewTarefaModalController as NewTarefa', templateUrl: 'templates/modal/atividades-adicionar-tarefa.html'});
    };
    
    $rootScope.$on('atividades:atualizarTarefas', function(){
        $scope.carregarTarefas(true);
    });
	
	/* Conexões */
	$scope.carregarConexoes = function(){
		_this.tab = 'conexoes';
		if(!_this.conexoes && !_this.carregandoConexoes){
			_this.carregandoConexoes = true;
			ElysiumAPI.get('apensos/'+$stateParams.id+'/?all=true', 'processos').then(function(response){
				_this.conexoes = response.data.results;
				if(response.data.next) _this.proximaPaginaDeConexoes = response.data.next;
				else _this.fimDaListaDeConexoes = true;
				_this.carregandoConexoes = false;
			},function(){
				ElysiumAPI.showError('Algo deu errado ao carregar a lista de conexões', response);
			});
		}
		if(!_this.processos && !_this.carregandoProcessos){
			_this.carregandoProcessos = true;
			ElysiumAPI.getUrl('processos/processos/?all=true').then(function(response){
				_this.processos = response.data;

				// Retira o processo atual da lista de processos
				for(i = 0; i < _this.processos.length; i++){
					if(_this.processos[i].id === _this.dados.id || _this.dados.apensos.indexOf(_this.processos[i].id) != -1){
						_this.processos.splice(i--, 1);
						break;
					}
				}

				// Retira os processos já conectados da lista de processos
				for(var i = 0; i < _this.processos.length; i++){
					for(var a = 0; a < _this.dados.apensos.length; a++){
						if(_this.processos[i].id == _this.dados.apensos[a].id){
							_this.processos.splice(i, 1);
							break;
						}
					}
				}
				_this.carregandoProcessos = false;
			},function(){
				ElysiumAPI.showError('Algo deu errado ao carregar a lista de processos', response);
			});
		}
	};

	$scope.carregarMaisConexoes = function(){
		if(!_this.carregandoConexoes){
			_this.carregandoConexoes = true;
			ElysiumAPI.getCompleteUrl(_this.proximaPaginaDeConexoes).then(function(response){
				for(i = 0; i < response.data.results.length;i++){
					_this.conexoes.push(response.data.results[i]);
				};
				if(response.data.next) _this.proximaPaginaDeConexoes = response.data.next;
				else _this.fimDaListaDeConexoes = true;
				_this.carregandoConexoes = false;
			}, function(response){
				ElysiumAPI.showError('Algo deu errado ao carregar mais conexões', response);
			});
		}
	};

	$scope.recarregarConexoes = function(){
		_this.carregandoConexoes = true;
		_this.fimDaListaDeConexoes = false;
		_this.conexoes = [];
		ElysiumAPI.get('apensos/'+$stateParams.id, 'processos').then(function(response){
			_this.conexoes = response.data.results;
			if(response.data.next) _this.proximaPaginaDeConexoes = response.data.next;
			else _this.fimDaListaDeConexoes = true;
			_this.carregandoConexoes = false;
		},function(){
			ElysiumAPI.showError('Algo deu errado ao carregar a lista de conexões', response);
		});
	};

	$scope.adicionarConexao = function(id){
		ElysiumAPI.get("processos/"+_this.dados.id, 'processos').then(function successCallback(response){
			var conexoesAux = response.data.apensos;
			var conexoes = [];
			for(i = 0; i < conexoesAux.length; i++){
				conexoes.push(conexoesAux[i].id);
			}
			conexoes.push(id);
			var dados = {apensos: conexoes};
			ElysiumAPI.patch("processos/"+_this.dados.id, 'processos', dados).then(function(response){
				$scope.recarregarConexoes();
			});
		});
	};

	$scope.removerConexao = function(_id){
		ElysiumAPI.get("processos/"+_this.dados.id, 'processos').then(function successCallback(response){
			var conexoesAux = response.data.apensos;
			var conexoes = [];
			for(i = 0; i < conexoesAux.length; i++){
				conexoes.push(conexoesAux[i].id);
			}
			conexoes.splice(conexoes.indexOf(_id), 1);
			var dados = {apensos: conexoes};
			ElysiumAPI.patch("processos/"+_this.dados.id, 'processos', dados).then(function(response){
				$scope.recarregarConexoes();
			});
		});
	};

	/* Incidentes */
	$scope.adicionarIncidente = function(){
		if(!_this.incidente.responsaveis.length){_this.incidente.responsaveis = [ModelService.getItem('user:id')];}
		var dados = {
	        titulo: _this.incidente.titulo,
			responsaveis: _this.incidente.responsaveis,
			tipo_de_acao: _this.incidente.tipo_de_acao,
			observacoes: _this.incidente.observacoes,
			processo_do_incidente: _this.dados.id
	    };
	    ElysiumAPI.post('incidentes', 'processos', dados).then(function(response){
			$scope.adicionarIncidenteModal.hide();
			$scope.carregarIncidentes(true);
	    }, function(response){
	        ElysiumAPI.showError('Algo deu errado ao criar o incidente', response)
	    });
	};

	$scope.carregarIncidentes = function(forcarRecarga){
		_this.tab = 'incidentes';
		if((!_this.incidentes && !_this.carregandoIncidentes) || forcarRecarga){
			_this.carregandoIncidentes = true;
			ElysiumAPI.get("proincidentes/"+$stateParams.id, 'processos').then(function(response){
				_this.incidentes = response.data;
				_this.carregandoIncidentes = false;
			},function(response){
				ElysiumAPI.showError('Algo deu errado ao carregar os incidentes', response);
				_this.incidentes = false;
			});
		}
	};

	$scope.excluirIncidente = function(id){
		if(!_this.excluindoIncidente){
			_this.excluindoIncidente = true;
			ElysiumAPI.delete("incidentes/" + id, 'processos').then(function(response){
				_this.excluindoIncidente = false;
				$scope.incidenteModal.hide();
				_this.incidenteTab = 'detalhes';
				$scope.carregarIncidentes(true);
			},function(response){
				ElysiumAPI.showError('Algo deu errado ao excluir esse incidente', response);
				_this.excluindoIncidente = false;
			});
		}
	};

	$scope.exibirIncidente = function(incidente){
		_this.incidenteCarregado = incidente;
		$scope.incidenteModal = $modal({scope: $scope, backdrop: 'static', templateUrl: 'templates/modal/processos-detalhes-incidente.html'});
	};

	$scope.prepararAdicaoIncidente = function(){
		$scope.adicionarIncidenteModal = $modal({scope: $scope, backdrop: 'static', templateUrl: 'templates/modal/processos-adicionar-incidente.html'});
	};

	$scope.prepararEdicaoIncidente = function(){
		_this.incidenteTab = 'editar';
		_this.incidenteEditando = {
			id: _this.incidenteCarregado.id,
			observacoes: _this.incidenteCarregado.observacoes,
			responsaveis: _this.incidenteCarregado.responsaveis,
			tipo_de_acao: _this.incidenteCarregado.tipo_de_acao,
			titulo: _this.incidenteCarregado.titulo
		}
	};

	$scope.editarIncidente = function(){
		ElysiumAPI.patch("incidentes/"+_this.incidenteCarregado.id, 'processos', _this.incidenteEditando).then(function(response){
			$scope.incidenteModal.hide();
			_this.incidenteTab = 'detalhes';
			$scope.carregarIncidentes(true);
		},function(response){
			ElysiumAPI.showError('Algo deu errado ao editar o incidente', response);
		});
	};

	/* Recursos */
	$scope.carregarRecursos = function(forcarRecarga){
	    _this.tab = 'recursos';
	    if((!_this.recursos && !_this.carregandoRecursos) || forcarRecarga){
	        _this.carregandoRecursos = true;
	        ElysiumAPI.get("prorecursos/"+$stateParams.id, 'processos').then(function(response){
	            _this.recursos = response.data;
				_this.carregandoRecursos = false;
	        },function(response){
	            ElysiumAPI.showError('Algo deu errado ao carregar os recursos', response);
	            _this.recursos = false;
	        });
	    }
	};

	$scope.adicionarRecurso = function(){
		if(_this.recurso.data_de_citacao) var data_de_citacao = new Date(_this.recurso.data_de_citacao).toJSON().slice(0,10);
		if(_this.recurso.data_de_distribuicao) var data_de_distribuicao = new Date(_this.recurso.data_de_distribuicao).toJSON().slice(0,10);

		if(!_this.recurso.responsaveis.length){_this.recurso.responsaveis = [ModelService.getItem('user:id')];}

		var dados = {
			adversos: _this.recurso.adversos,
			advogados_do_adverso: _this.recurso.advogado_do_adverso,
			clientes: _this.recurso.clientes,
			data_de_citacao: data_de_citacao,
			data_de_distribuicao: data_de_distribuicao,
			numero_do_processo: _this.dados.numero_do_processo,
			observacoes: _this.recurso.observacoes,
			orgao_judicial: _this.recurso.orgao_judicial,
			posicao_do_adverso: _this.recurso.posicao_do_adverso,
			posicao_do_cliente: _this.recurso.posicao_do_cliente,
			processo_eletronico: _this.recurso.processo_eletronico,
			responsaveis: _this.recurso.responsaveis,
			tipo_de_acao: _this.recurso.tipo_de_acao,
			titulo: _this.recurso.titulo,
			processo_do_recurso: _this.dados.id
		};
		ElysiumAPI.post("recursos", 'processos', dados).then(function(response){
			$scope.adicionarRecursoModal.hide();
			$scope.carregarRecursos(true);
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao criar o recurso', response);
		});
	};

	$scope.excluirRecurso = function(id){
		if(!_this.excluindoRecurso){
			_this.excluindoRecurso = true;
			ElysiumAPI.delete("recursos/" + id, 'processos').then(function(response){
				_this.excluindoRecurso = false;
				$scope.recursoModal.hide();
				_this.recursoTab = 'detalhes';
				$scope.carregarRecursos(true);
			},function(response){
	            ElysiumAPI.showError('Algo deu errado ao excluir esse recurso', response);
	            _this.excluindoRecurso = false;
	        });
		}
	};

	$scope.exibirRecurso = function(recurso){
		_this.recursoCarregado = recurso;
		$scope.recursoModal = $modal({scope: $scope, backdrop: 'static', templateUrl: 'templates/modal/processos-detalhes-recurso.html'});
	};

	$scope.prepararAdicaoRecurso = function(){
		$scope.adicionarRecursoModal = $modal({scope: $scope, backdrop: 'static', templateUrl: 'templates/modal/processos-adicionar-recurso.html'});
	};

	$scope.prepararEdicaoRecurso = function(){
		_this.recursoTab = 'editar';
		_this.recursoEditando = {
			id: _this.recursoCarregado.id,
			adversos: _this.recursoCarregado.adversos,
			advogados_do_adverso: _this.recursoCarregado.advogados_do_adverso,
			clientes: _this.recursoCarregado.clientes,
			data_de_citacao: _this.recursoCarregado.data_de_citacao,
			data_de_distribuicao: _this.recursoCarregado.data_de_distribuicao,
			numero_do_processo: _this.dados.numero_do_processo,
			observacoes: _this.recursoCarregado.observacoes,
			orgao_judicial: _this.recursoCarregado.orgao_judicial,
			posicao_do_adverso: _this.recursoCarregado.posicao_do_adverso,
			posicao_do_cliente: _this.recursoCarregado.posicao_do_cliente,
			processo_eletronico: _this.recursoCarregado.processo_eletronico,
			responsaveis: _this.recursoCarregado.responsaveis,
			tipo_de_acao: _this.recursoCarregado.tipo_de_acao,
			titulo: _this.recursoCarregado.titulo
		}
	};

	$scope.editarRecurso = function(){
		ElysiumAPI.patch("recursos/"+_this.recursoCarregado.id, 'processos', _this.recursoEditando).then(function(response){
			$scope.recursoModal.hide();
			_this.recursoTab = 'detalhes';
			$scope.carregarRecursos(true);
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao editar o recurso', response);
		});
	};

	/* Movimentações */
	$scope.carregarMovimentacoes = function(forcarRecarga){
	    _this.tab = 'movimentacoes';
		if(forcarRecarga == "recarregar" || !forcarRecarga){
			if(_this.movimentacoesStatus == "inicial" || forcarRecarga){
				_this.movimentacoesStatus = "carregando";
				_this.movimentacoes = undefined;
				ElysiumAPI.get("promovimentacoes/"+$stateParams.id, 'updates').then(function(response){
		            if(response.data[0]){
		                _this.movimentacoes = response.data[0].movimentacoes;
						_this.movimentacoesUltimaVerificacao = moment(response.data[0].data_ultima_verificacao);
						_this.movimentacoesUltimaAtualizacao = response.data[0].data_ultima_atualizacao;
						_this.movimentacoesStatus = "carregado";
						$scope.visualizarNotificacoes();
		            } else if(response.data.error){
						_this.movimentacoesStatus = "invalido";
					}
		            else{
		                _this.movimentacoesStatus = "vazio"
		            }
		        },function(response){
		            ElysiumAPI.showError('Algo deu errado ao carregar as movimentações', response);
		        });
			}
		} else if(forcarRecarga == "atualizar"){
			_this.movimentacoesStatus = "carregando";
			_this.movimentacoes = undefined;
			ElysiumAPI.get("proforcemovimentacoes/"+$stateParams.id, 'updates').then(function(response){
				if(response.data[0]){
					_this.movimentacoes = response.data[0].movimentacoes;
					_this.movimentacoesUltimaVerificacao = moment(response.data[0].data_ultima_verificacao);
					_this.movimentacoesUltimaAtualizacao = response.data[0].data_ultima_atualizacao;
					_this.movimentacoesStatus = "carregado";
					$scope.visualizarNotificacoes();
				} else if(response.data.error){
					if(response.data.error != "['Invalid Process Number']"){
						$scope.carregarMovimentacoes("recarregar");
						ElysiumAPI.showMessage('As movimentações de um processo só podem ser atualizadas uma vez a cada 60 minutos.<br><br>Tente novamente mais tarde.');
					} else _this.movimentacoesStatus = "invalido";
				}
				else{
					_this.movimentacoesStatus = "vazio"
				}
			},function(response){
				ElysiumAPI.showError('Algo deu errado ao carregar as movimentações', response);
			});
		}
	};

	$scope.visualizarNotificacoes = function(){
		var index = _this.dados.notificacoes.indexOf(Number(ModelService.getItem("user:id")));
		if(index != -1){
			_this.dados.notificacoes.splice(index, 1)
			ElysiumAPI.patch("processos/"+_this.dados.id, 'processos', {notificacoes: _this.dados.notificacoes}).then(function(){
				$scope.atualizarNotificacoes();
			},function(response){
				ElysiumAPI.showError('Algo deu errado ao tentar atualizar as notificações desse processo', response);
				_this.dados.notificacoes.push(_this.dados.notificacoes[index]);
			});
		};
	};

	/* Documentos */
	$scope.carregarDocumentos = function(forcarRecarga){
	    _this.tab = 'documentos';
	    if(!_this.documentos || forcarRecarga){
	        ElysiumAPI.get("prodocumentos/"+$stateParams.id, 'documentos').then(function successCallback(response){
	            _this.documentos = response.data;
				for (var i = 0; i < _this.documentos.length; i++) {
					_this.documentos[i].ext = _this.documentos[i].nome.slice(_this.documentos[i].nome.search(/(\.\w+$)/i));
				}
	        });
	    }
	};

	$scope.enviarDocumentos = function(arquivosValidos, arquivosInvalidos){
	    _this.arquivos = arquivosValidos;
	    _this.arquivosInvalidos = arquivosInvalidos;

	    if(_this.arquivosInvalidos[0]){
	        angular.forEach(arquivosInvalidos, function(arquivoInvalido){
				switch (arquivoInvalido.$error) {
					case "pattern":
						ElysiumAPI.showError("O arquivo \"" + arquivoInvalido.name + "\" tem uma extensão inválida e não será enviado.<br><br>As extensões válidas são: .pdf, .docx, .txt, .doc, .odt, .xls, .xlsx, .jpg, .png, .bmp e .gif.");
						break;
					default:
						ElysiumAPI.showError("O arquivo \"" + arquivoInvalido.name + "\" é inválido e não será enviado.<br><br>Tente recarregar a página e enviá-lo novamente. Se o erro persistir, entre em contato com o suporte.");
						break;
				}
	        });
	    }

	    angular.forEach(arquivosValidos, function(arquivo) {
			if(arquivo.name.length > 255){
				var _formato = arquivo.name.slice(arquivo.name.search(/(\.\w+$)/i));
				var _nome = arquivo.name.slice(0,(255 - _formato.length)) + _formato;
				Upload.rename(arquivo, _nome);
				ElysiumAPI.showMessage('O nome de um dos arquivos selecionados tem mais do que 255 caracteres<br><br>Esse arquivo foi automaticamente renomeado para:<br>' + _nome);
			} else var _nome = arquivo.name;
	        arquivo.upload = Upload.upload({
	            url: ElysiumAPI.apiURL+"documentos/documentos/",
	            data: {
	                nome: _nome,
	                arquivo: arquivo,
	                processo: _this.dados.id
	            }
	        });

	        arquivo.upload.then(function(response){
	            $timeout(function(){
	                arquivo.result = response.data;
	                if(!Upload.isUploadInProgress()){
	                    _this.arquivos = null;
	                    $scope.carregarDocumentos(true);
	                }
	            });
	        }, function(response){
	            if (response.status > 0) return;
	        }, function(evt){
	            arquivo.progresso = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
	        });
	    });
	};

	$scope.deletarDocumento = function(id){
	    ElysiumAPI.delete("documentos/"+id, 'documentos').then(function successCallback(response){
	        ElysiumAPI.get("prodocumentos/"+$stateParams.id, 'documentos').then(function successCallback(response){
	            _this.documentos = response.data;
	        });
	    });
	};

	/* Modais */
	$scope.adicionarCliente = function(dados){
		_this.clientes.push({
			id: dados.id,
			email: dados.email,
			nome: dados.nome,
			subtipo: dados.subtipo
		});
		_this.recurso.clientes.push(dados.id);
	};

	$scope.adicionarAdverso = function(dados){
		_this.adversos.push({
			id: dados.id,
			email: dados.email,
			nome: dados.nome,
			subtipo: dados.subtipo
		});
		_this.recurso.adversos.push(dados.id);
	};

	$scope.adicionarAdvogado = function(dados){
		_this.advogados.push({
			id: dados.id,
			email: dados.email,
			nome: dados.nome,
			subtipo: dados.subtipo
		});
		_this.recurso.advogado_do_adverso.push(dados.id);
	};
}]);
