elysium.controller('ProcessosEditarCtrl', ['ElysiumAPI', '$scope', '$state', '$stateParams', 'ModelService', 'ProcessoFactory', function(ElysiumAPI, $scope, $state, $stateParams, ModelService, ProcessoFactory){
	var _this = this;
	ElysiumAPI.get('processes/' + $stateParams.id, 'process').then(function successCallback(response){
		_this.dados = new ProcessoFactory(response.data);

		var _responsaveis = [];
		for(var i = 0; i <  _this.dados.responsaveis.length; i++){
			_responsaveis.push(_this.dados.responsaveis[i].id);
		}
		_this.dados.responsaveis = _responsaveis;

		if(_this.dados.clientes){
			var _clientes = [];
			for(var i = 0; i <  _this.dados.clientes.length; i++){
				_clientes.push(_this.dados.clientes[i].id);
			}
			_this.dados.clientes = _clientes;
		}

		if(_this.dados.adversos){
			var _adversos = [];
			for(var i = 0; i <  _this.dados.adversos.length; i++){
				_adversos.push(_this.dados.adversos[i].id);
			}
			_this.dados.adversos = _adversos;
		}

		if(_this.dados.advogados_do_adverso){
			var _advogados = [];
			for(var i = 0; i <  _this.dados.advogados_do_adverso.length; i++){
				_advogados.push(_this.dados.advogados_do_adverso[i].id);
			}
			_this.dados.advogados_do_adverso = _advogados;
		}
		
		_this.dados.estado = _this.dados.getStateCode();
	});

	// Opções e config das probabilidades de êxito
	_this.probabilidadesConfig = {
        valueField: 'value',
        labelField: 'label',
        highlight: false,
        searchField: ['label'],
		maxItems: 1
    };
	_this.probabilidades = [
		{value:'IMPOSSIBLE',label:'Inviável'},
		{value:'REMOTE',label:'Remota (1%~20%)'},
		{value:'POSSIBLE',label:'Possível (21%~50%)'},
		{value:'PROBABLE',label:'Provável (51%~80%)'},
		{value:'VERY_PROBABLE',label:'Muito Provável (81%~100%)'}
	];
    
    _this.estados = [{valor:"AC",nome:"Acre"},{valor:"AL",nome:"Alagoas"},{valor:"AM",nome:"Amazonas"},{valor:"AP",nome:"Amapá"},{valor:"BA",nome:"Bahia"},{valor:"CE",nome:"Ceará"},{valor:"DF",nome:"Distrito Federal"},{valor:"ES",nome:"Espírito Santo"},{valor:"GO",nome:"Goiás"},{valor:"MA",nome:"Maranhão"},{valor:"MT",nome:"Mato Grosso"},{valor:"MS",nome:"Mato Grosso do Sul"},{valor:"MG",nome:"Minas Gerais"},{valor:"PA",nome:"Pará"},{valor:"PB",nome:"Paraíba"},{valor:"PR",nome:"Paraná"},{valor:"PI",nome:"Piauí"},{valor:"RJ",nome:"Rio de Janeiro"},{valor:"RN",nome:"Rio Grande do Norte"},{valor:"RO",nome:"Rondônia"},{valor:"RS",nome:"Rio Grande do Sul"},{valor:"RR",nome:"Roraima"},{valor:"SC",nome:"Santa Catarina"},{valor:"SE",nome:"Sergipe"},{valor:"SP",nome:"São Paulo"},{valor:"TO",nome:"Tocantins"}];
    _this.estadosConfig = {
        valueField: 'valor',
        highlight: false,
        labelField: 'nome',
        searchField: ['nome'],
        maxItems: 1
    };
	
    // Cria a lista de clientes
	_this.clientesConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name']
    };
    ElysiumAPI.getUrl('person/clients/').then(function(response){
        _this.clientes = response.data;
    }, function(response){
		ElysiumAPI.showError('Erro ao carregar a lista de Clientes', response);
    });

    // Cria a lista de adversos
	_this.adversosConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name']
    };
    ElysiumAPI.getUrl('person/adverses/').then(function(response){
        _this.adversos = response.data;
    }, function(response){
		ElysiumAPI.showError('Erro ao carregar a lista de Adversos', response);
    });

    // Cria a lista de responsáveis
    _this.usuariosConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name','email']
    };
    ElysiumAPI.getUrl('users/users/?all=true').then(function(response){
        _this.usuarios = response.data;
    }, function(response){
        ElysiumAPI.showError('Erro ao carregar a lista de Responsáveis', response);
    });

	// Cria a lista de advogados adversos
	_this.advogadosConfig = {
		valueField: 'id',
		highlight: false,
		labelField: 'name',
		searchField: ['name']
	};
	ElysiumAPI.getUrl('person/adverse_lawyers/').then(function(response){
		_this.advogados = response.data;
	}, function(response){
		ElysiumAPI.showError('Erro ao carregar a lista de Advogados', response);
	});


	$scope.salvarProcesso = function(){
		if(_this.dados.data_de_citacao) var data_de_citacao = new Date(_this.dados.data_de_citacao).toJSON().slice(0,10);
        if(_this.dados.data_de_distribuicao) var data_de_distribuicao = new Date(_this.dados.data_de_distribuicao).toJSON().slice(0,10);

		if(_this.dados.responsaveis === []){
			_this.dados.responsaveis = [ModelService.getItem('user:id')];
		}

		var dados = {
			adverses: _this.dados.adversos,
            adverse_lawyers: _this.dados.advogados_do_adverso,
            clients: _this.dados.clientes,
            notification_date: data_de_citacao,
            distribution_date: data_de_distribuicao,
            instance: _this.dados.instancia,
            process_number: _this.dados.numero_do_processo,
            comments: _this.dados.observacoes,
            judicial_organ: _this.dados.orgao_judicial,
            adverse_position: _this.dados.posicao_do_adverso,
            client_position: _this.dados.posicao_do_cliente,
            is_public: Boolean(Number(_this.dados.is_public)),
            probability_of_success: _this.dados.probabilidades_de_exito,
            electronic_process: Boolean(Number(_this.dados.processo_eletronico)),
            responsible: _this.dados.responsaveis,
			subject: _this.dados.materia,
            action_type: _this.dados.tipo_de_acao,
            phase: _this.dados.tipo_de_fase,
            process_type: _this.dados.tipo_de_processo,
            rite: _this.dados.tipo_de_rito,
            title: _this.dados.titulo,
            cause_value: _this.dados.valor_da_causa,
            provisioned_value: _this.dados.valor_provisionado,
            state: _this.dados.estado
		};
		
		angular.forEach(dados, function(value, key){
			if(dados[key] === null) delete dados[key];
        });
		
		ElysiumAPI.patch("processes/" + $stateParams.id, 'process', dados).then(function(response){
			$state.go('app.processos.details', {id: response.data.id});
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao editar esse processo', response);
		});
	};
}]);
