elysium.controller('ProcessoDetailsController', ['ProcessoService', '$stateParams', '$popover', '$scope', 'ModalService', '$state', function(ProcessoService, $stateParams, $popover, $scope, ModalService, $state){
    var _this = this;
    
    
    _this.processoPromise = ProcessoService.getProcesso($stateParams.id).then(function(response){
        _this.processo = response;
    });
    
    $scope.$on('processo:delete', function(){
        $state.go('app.processos.list');
    });
    
    
    /**
     * Prepares and opens the popover meant to change a processo's status
     * @param event jQuery click event
     * @param processo Processo to change the status
     */
    _this.openStatusPopover = function(event, processo){
        var scope = $scope.$new();
        scope.processo = processo;
        
        var popover = $popover(angular.element(event.currentTarget), {
            scope: scope,
            autoClose: true,
            templateUrl: 'app/processos/views/popover-change-status.html',
            container: 'body',
            placement: 'left',
            trigger: 'manual'
        });
        popover.$promise.then(popover.show);
    };
    
    /**
     * Opens a modal meant to delete a processo
     * @param processo The processo to delete
     */
    _this.openDeleteModal = function(processo){
        ModalService.setModal({
            locals: {_Data: {processo: processo}},
            autoClose: true,
            templateUrl: 'app/processos/views/modal-delete.html',
            controller: 'ProcessosDeleteModalController as ProcessosDeleteModal'
        });
    };
    
    _this.openResponsibleModal = function(){
        ModalService.setModal({
            scope: $scope.$new(),
            autoClose: true,
            templateUrl: 'app/processos/views/modal-responsible.html',
            controller: 'ModalResponsibleController as ModalResponsible'
        });
    }
}]);
