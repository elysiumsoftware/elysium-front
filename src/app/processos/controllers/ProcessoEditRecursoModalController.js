elysium.controller('ProcessoEditRecursoModalController', ['ProcessoService', '_Data', '$q', 'ModelService', '$scope', '$rootScope', 'ModalService', function(ProcessoService, _Data, $q, ModelService, $scope, $rootScope, ModalService){
    var _this = this;
    
    
    _this.recurso = angular.copy(_Data.recurso);
    if(_this.recurso.data_de_citacao) _this.recurso.data_de_citacao = new moment(_this.recurso.data_de_citacao).utc();
    if(_this.recurso.data_de_distribuicao) _this.recurso.data_de_distribuicao = new moment(_this.recurso.data_de_distribuicao).utc();
    
    _this.currentRecurso = _Data.recurso;

    _this.partPositions = [
            'Autor',
            'Réu',
            'Reclamante',
            'Reclamado',
            'Requerente',
            'Requerido',
            'Terceiro',
            'Apelante',
            'Apelado',
            'Agravante',
            'Agravado',
            'Recorrente',
            'Recorrido'
        ];
    
    if(_this.recurso.clientes){
        var clienteList = [];
        angular.forEach(_this.recurso.clientes, function(cliente){
            clienteList.push(cliente.id);
        });
        _this.recurso.clientes = clienteList;
    }
    
    if(_this.recurso.adversos){
        var adversoList = [];
        angular.forEach(_this.recurso.adversos, function(adverso){
            adversoList.push(adverso.id);
        });
        _this.recurso.adversos = adversoList;
    }
    
    if(_this.recurso.advogados_do_adverso){
        var advogadoAdversoList = [];
        angular.forEach(_this.recurso.advogados_do_adverso, function(advogadoAdverso){
            advogadoAdversoList.push(advogadoAdverso.id);
        });
        _this.recurso.advogados_do_adverso = advogadoAdversoList;
    }
    
    $q.all({
        clientes: ModelService.getClientes(),
        adversos: ModelService.getAdversos(),
        advogadosAdversos: ModelService.getAdvogadosAdversos()
    }).then(function(responses){
        _this.clientes = responses.clientes;
        _this.adversos = responses.adversos;
        _this.advogadosAdversos = responses.advogadosAdversos;
    });
    
    
    _this.updateRecurso = function(){
        if(_this.recurso.data_de_citacao) _this.recurso.data_de_citacao = moment(_this.recurso.data_de_citacao).format('YYYY-MM-DD');
        if(_this.recurso.data_de_distribuicao) _this.recurso.data_de_distribuicao = moment(_this.recurso.data_de_distribuicao).format('YYYY-MM-DD');
        
        ModelService.updateRecurso(_this.recurso).then(function(updatedRecurso){
            $rootScope.$broadcast('processo:recurso:update', _this.currentRecurso, updatedRecurso);
            $scope.$hide();
        });
    };
    
    /**
     * Sets the current date as value to a specific date field
     * @param {String} field - The form field to set: data_de_distribuicao or data_de_citacao
     */
    _this.setToday = function(field){
        if(field === 'data_de_distribuicao') _this.recurso.data_de_distribuicao = new moment();
        else if(field === 'data_de_citacao') _this.recurso.data_de_citacao = new moment();
    };
    
    /**
     * Opens a modal window to add a new client inline
     * @param {String} type - The type of the person to add: client, adverse or adverse_lawyer
     */
    _this.openCreatePersonModal = function(type){
        ModalService.setModal(
            {
                locals: {_Data: {type: type, callback: addPerson}},
                scope: $scope,
                templateUrl: 'app/pessoas/views/modals/create-person.html',
                controller: 'CreatePersonModalController as CreatePersonModal',
                autoClose: true
            }
        );
    };
    
    
    /**
     * Function to be used as a callback to the create person modal
     * @param {ClientFactory | AdverseFactory | AdverseLawyerFactory} person - The person just created
     * @param {String} type - The type of the person to add: client, adverse or adverse_lawyer
     */
    function addPerson(person, type){
        if(type === 'client'){
            _this.clientes.push({id: person.id, nome: person.name});
            _this.recurso.clientes.push(person.id);
        }
        else if(type === 'adverse'){
            _this.adversos.push({id: person.id, nome: person.name});
            _this.recurso.adversos.push(person.id);
        }
        else if(type === 'adverse_lawyer'){
            _this.advogadosAdversos.push({id: person.id, nome: person.name});
            _this.recurso.advogados_do_adverso.push(person.id);
        }
    }
}]);
