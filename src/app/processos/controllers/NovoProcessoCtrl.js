elysium.controller('NovoProcessoCtrl', ['ElysiumAPI', '$scope', '$state', 'ModelService', function(ElysiumAPI, $scope, $state, ModelService){
    var _this = this;

	_this.criandoProcesso = false;

    _this.is_public = true;
	_this.processo_eletronico = true;
	_this.cliente = [];
	_this.adverso = [];
	_this.responsaveis = [];
	_this.advogado_do_adverso = [];
	_this.posicao_do_cliente = "";

	// Opções e config das probabilidades de êxito
	_this.probabilidadesConfig = {
        valueField: 'value',
        labelField: 'label',
        highlight: false,
        searchField: ['label'],
		maxItems: 1
    };
	_this.probabilidades = [
		{value:'IMPOSSIBLE',label:'Inviável'},
		{value:'REMOTE',label:'Remota (1%~20%)'},
		{value:'POSSIBLE',label:'Possível (21%~50%)'},
		{value:'PROBABLE',label:'Provável (51%~80%)'},
		{value:'VERY_PROBABLE',label:'Muito Provável (81%~100%)'}
	];
    
    _this.estados = [{valor:"AC",nome:"Acre"},{valor:"AL",nome:"Alagoas"},{valor:"AM",nome:"Amazonas"},{valor:"AP",nome:"Amapá"},{valor:"BA",nome:"Bahia"},{valor:"CE",nome:"Ceará"},{valor:"DF",nome:"Distrito Federal"},{valor:"ES",nome:"Espírito Santo"},{valor:"GO",nome:"Goiás"},{valor:"MA",nome:"Maranhão"},{valor:"MT",nome:"Mato Grosso"},{valor:"MS",nome:"Mato Grosso do Sul"},{valor:"MG",nome:"Minas Gerais"},{valor:"PA",nome:"Pará"},{valor:"PB",nome:"Paraíba"},{valor:"PR",nome:"Paraná"},{valor:"PI",nome:"Piauí"},{valor:"RJ",nome:"Rio de Janeiro"},{valor:"RN",nome:"Rio Grande do Norte"},{valor:"RO",nome:"Rondônia"},{valor:"RS",nome:"Rio Grande do Sul"},{valor:"RR",nome:"Roraima"},{valor:"SC",nome:"Santa Catarina"},{valor:"SE",nome:"Sergipe"},{valor:"SP",nome:"São Paulo"},{valor:"TO",nome:"Tocantins"}];
    _this.estadosConfig = {
        valueField: 'valor',
        highlight: false,
        labelField: 'nome',
        searchField: ['nome'],
        maxItems: 1
    };

    // Cria a lista de clientes
	_this.clientesConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name']
    };
    ElysiumAPI.getUrl('person/clients/').then(function(response){
        _this.clientes = response.data;
    }, function(response){
		ElysiumAPI.showError('Erro ao carregar a lista de Clientes', response);
    });

    // Cria a lista de adversos
	_this.adversosConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name']
    };
    ElysiumAPI.getUrl('person/adverses/').then(function(response){
        _this.adversos = response.data;
    }, function(response){
		ElysiumAPI.showError('Erro ao carregar a lista de Adversos', response);
    });

    // Cria a lista de responsáveis
    _this.usuariosConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name','email']
    };
    ElysiumAPI.getUrl('users/users/?all=true').then(function(response){
        _this.usuarios = response.data;
    }, function(response){
        ElysiumAPI.showError('Erro ao carregar a lista de Responsáveis', response);
    });

	// Cria a lista de advogados adversos
	_this.advogadosConfig = {
		valueField: 'id',
		highlight: false,
		labelField: 'name',
		searchField: ['name']
	};
	ElysiumAPI.getUrl('person/adverse_lawyers/').then(function(response){
		_this.advogados = response.data;
	}, function(response){
		ElysiumAPI.showError('Erro ao carregar a lista de Advogados', response);
	});

	$scope.adicionarCliente = function(dados){
		_this.clientes.push({
			id: dados.id,
			email: dados.email,
			name: dados.nome
		});
		console.debug(dados, _this.clientes);
		_this.cliente.push(dados.id);
	};

	$scope.adicionarAdverso = function(dados){
		_this.adversos.push({
			id: dados.id,
			email: dados.email,
			name: dados.nome
		});
		_this.adverso.push(dados.id);
	};

	$scope.adicionarAdvogado = function(dados){
		_this.advogados.push({
			id: dados.id,
			email: dados.email,
			name: dados.nome
		});
		_this.advogado_do_adverso.push(dados.id);
	};

    $scope.criarProcesso = function(){
		if(!_this.criandoProcesso){
			_this.criandoProcesso = true;
	        if(_this.data_de_citacao) var data_de_citacao = new Date(_this.data_de_citacao).toJSON().slice(0,10);
	        if(_this.data_de_distribuicao) var data_de_distribuicao = new Date(_this.data_de_distribuicao).toJSON().slice(0,10);

			if(_this.responsaveis.length == 0){
				_this.responsaveis = [ModelService.getItem('user:id')];
			}

			if(_this.valor_da_causa == "") _this.valor_da_causa = undefined;
			if(_this.valor_provisionado == "") _this.valor_provisionado = undefined;

	        var dados = {
	            adverses: _this.adverso,
	            adverse_lawyers: _this.advogado_do_adverso,
	            clients: _this.cliente,
	            notification_date: data_de_citacao,
	            distribution_date: data_de_distribuicao,
	            instance: _this.instancia,
	            process_number: _this.numero_do_processo,
	            comments: _this.observacoes,
	            judicial_organ: _this.orgao_judicial,
	            adverse_position: _this.posicao_do_adverso,
	            client_position: _this.posicao_do_cliente,
	            is_public: Boolean(Number(_this.is_public)),
                probability_of_success: _this.probabilidades_de_exito,
	            electronic_process: Boolean(Number(_this.processo_eletronico)),
	            responsible: _this.responsaveis,
                process_status: 'Ativo',
                subject: _this.materia,
	            action_type: _this.tipo_de_acao,
	            phase: _this.tipo_de_fase,
	            process_type: _this.tipo_de_processo,
	            rite: _this.tipo_de_rito,
	            title: _this.titulo,
				cause_value: _this.valor_da_causa,
                provisioned_value: _this.valor_provisionado,
				updates: [],
                state: _this.estado
	        };
	        ElysiumAPI.post("processes", 'process', dados).then(function(response){
	            $state.go('app.processos.details', {id: response.data.id});
	        }, function(response){
				_this.criandoProcesso = false;
				ElysiumAPI.showError('Algo deu errado ao criar um novo Processo', response);
	        });
		}
    };
}]);
