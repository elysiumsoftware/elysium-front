elysium.controller('ProcessosCtrl', ['ElysiumAPI', '$scope', '$state', 'ModelService', function(ElysiumAPI, $scope, $state, ModelService){
	var _this = this;
	_this.filtroStatus = '';

	$scope.excluirProcesso = function(id){
		ElysiumAPI.delete("processos/" + id, 'processos').then(function(response){
			if($state.is('app.processos.ativos')) $state.reload();
			else $state.go('app.processos.ativos');
		});
	};

	$scope.mudarStatus = function(processo, status){
		ElysiumAPI.patch("processos/"+processo.id, 'processos', {status_do_processo: status}).then(function(response){
			processo.status_do_processo = status;
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao mudar o status do processo', response);
		});
	};

	$scope.mudarFiltro = function(filtro){
		if(!$state.is('app.processos.ativos')) $state.go('app.processos.ativos');
		_this.filtroStatus = filtro;
	};

	$scope.temNotificacao = function(processo){
		if(!processo) return;
		if(processo.notificacoes.indexOf(Number(ModelService.getItem("user:id"))) != -1) return true;
		else return false;
	};
}]);
