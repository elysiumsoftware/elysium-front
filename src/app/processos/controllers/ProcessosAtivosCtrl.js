elysium.controller('ProcessosAtivosCtrl', ['ElysiumAPI', '$scope', function(ElysiumAPI, $scope){
	// var todos = this;
	// ElysiumAPI.get('processos', 'processos').then(function(response){todos.lista = response.data});

	var _this = this;
	_this.carregando = false;
	_this.fimDaLista = false;

	ElysiumAPI.get('processos', 'processos').then(function(response){
		_this.lista = response.data.results;
		if(response.data.next) _this.proximaPagina = response.data.next;
		else _this.fimDaLista = true;
	});

	$scope.carregarMais = function(){
		if(_this.fimDaLista) return;
		_this.carregando = true;
		ElysiumAPI.getCompleteUrl(_this.proximaPagina).then(function(response){
			for(i = 0; i < response.data.results.length;i++){
				_this.lista.push(response.data.results[i]);
			};
			if(response.data.next) _this.proximaPagina = response.data.next;
			else _this.fimDaLista = true;
			_this.carregando = false;
		}, function(response){
			ngToast.create({
				className: 'error',
				content: 'Algo deu errado ao carregar mais processos<br>Erro: ' + response.statusText
			});
			_this.carregando = false;
		});
	};
}]);
