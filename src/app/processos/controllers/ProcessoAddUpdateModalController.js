elysium.controller('ProcessoAddUpdateModalController', ['ProcessService', '_Data', 'ModelService', '$scope', '$rootScope', function(ProcessService, _Data, ModelService, $scope, $rootScope){
    var _this = this;

    this.data = {
        update: {},
        process: _Data.process
    };
    
    
    this.createUpdate = function(){
        var _data = {
            instance: _this.data.update.instance,
            date: _this.data.update.date.format('YYYY-MM-DD'),
            description: _this.data.update.description
        };

        ProcessService.createUpdate(_this.data.process.id, _data).then(function(update){
            $rootScope.$broadcast('process:update:created', update);
            $scope.$hide();
        });
    };

    this.setToday = function(){
        _this.data.update.date = new moment()
    };
}]);
