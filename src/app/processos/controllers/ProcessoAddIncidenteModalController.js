elysium.controller('ProcessoAddIncidenteModalController', ['ProcessoService', '_Data', 'ModelService', '$scope', '$rootScope', function(ProcessoService, _Data, ModelService, $scope, $rootScope){
    var _this = this;
    
    
    _this.incidente = {
        processo_do_incidente: _Data.processo.id
    };
    
    
    _this.createIncidente = function(){
        ProcessoService.createIncidente(_this.incidente).then(function(incidente){
            $rootScope.$broadcast('processo:incidente:create', incidente);
            $scope.$hide();
        });
    };
}]);
