elysium.controller('ProcessoDetailsPublicationsController', ['ProcessService', '$q', '$scope', 'ModalService', function(ProcessService, $q, $scope, ModalService){
    var _this = this;

    this.data = {};

    $q.when($scope.ProcessoDetails.processoPromise, function(){
        ProcessService.getPublications($scope.ProcessoDetails.processo.id).then(function(response){
            _this.data.publications = response;
        });
    });

    $scope.$on('publications:publication:updated', function(event, oldPublication, newPublication){
        _this.data.publications[_this.data.publications.indexOf(oldPublication)] = newPublication;
    });


    this.openDetailsModal = function(publication, index){
        var _next = index < _this.data.filteredPublications.length - 1 ? _this.data.filteredPublications[index + 1] : null;
        var _previous = index !== 0 ? _this.data.filteredPublications[index - 1] : null;

        ModalService.setModal({
            locals: {_Data: {publication: publication, iterFunction: _this.iterPublication, next: _next, previous: _previous}},
            templateUrl: 'app/publications/views/modal/details.html',
            controller: 'PublicationDetailsModalController as PublicationDetailsModal'
        });
    };

    this.iterPublication = function(publication){
        _this.openDetailsModal(publication, _this.data.filteredPublications.indexOf(publication));
    };
}]);
