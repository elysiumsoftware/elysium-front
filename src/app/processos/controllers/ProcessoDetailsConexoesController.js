elysium.controller('ProcessoDetailsConexoesController', ['ProcessoService', 'ModalService', '$scope', '$q', '$rootScope', function(ProcessoService, ModalService, $scope, $q, $rootScope){
    var _this = this;
    
    
    _this.openAddConexaoModal = function(){
        ModalService.setModal({scope: $scope.$new(), templateUrl: 'app/processos/views/modal-add-conexao.html', controller: 'ProcessoAddConexaoModalController as ProcessoAddConexaoModal'});
    };
    
    _this.openRemoveConexaoModal = function(conexao){
        ModalService.setModal({scope: $scope.$new(), locals: {_Data: {conexao: conexao}}, templateUrl: 'app/processos/views/modal-delete.html', controller: 'ProcessosDeleteModalController as ProcessosDeleteModal'});
    };
}]);
