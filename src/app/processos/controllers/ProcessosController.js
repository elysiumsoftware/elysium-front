elysium.controller('ProcessosController', ['ProcessoService', '$state', '$window', '$stateParams', function(ProcessoService, $state, $window, $stateParams){
    var _this = this;
    
    _this.processosFilter = '';
    
    
    _this.changeFilter = function(filter){
        switch(filter){
            case 'all':
                _this.processosFilter = '';
                break;
            case 'active':
                _this.processosFilter = 'ACTIVE';
                break;
            case 'suspended':
                _this.processosFilter = 'SUSPENDED';
                break;
            case 'archived':
                _this.processosFilter = 'ARCHIVED';
                break;
        }
        
        if(!$state.is('app.processos.list')) $state.go('app.processos.list');
    };
    
    _this.goBack = function(){
        if($state.is('app.processos.update')) $state.go('app.processos.details', {id: $state.params.id});
        else $state.go('app.processos.list');
    };
}]);
