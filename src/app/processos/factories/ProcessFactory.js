elysium.factory('ProcessFactory', ['ModelService', 'UserFactory', 'ConexaoFactory', function(ModelService, UserFactory, ConexaoFactory){
    var Process = function(data){
        var _this = this;
        
        // Some items don't need to be checked for compatibility since
        // Python already send them JavaScript-compliant
        this.id = data.id;
        this.is_public = data.is_public;
        this.creation_date = new Date(data.created_at);
        this.update_date = new Date(data.update_date);
        this.electronic_process = data.electronic_process;
        this.cause_value = data.cause_value;
        this.provisioned_value = data.provisioned_value;
        this.process_status = data.process_status;
        this.title = data.title;
        this.process_number = data.process_number;
        this.state = convertEstado(data.state);
        
        // Everything else must be checked
        this.client_position = (data.client_position.length) ? data.client_position : null;
        this.adverse_position = (data.adverse_position.length) ? data.adverse_position : null;
        this.action_type = (data.action_type.length) ? data.action_type : null;
        this.rite = (data.rite.length) ? data.rite : null;
        this.phase = (data.phase.length) ? data.phase : null;
        this.instance = (data.instance.length) ? data.instance : null;
        this.process_type = (data.process_type.length) ? data.process_type : null;
        this.judicial_organ = (data.judicial_organ.length) ? data.judicial_organ : null;
        this.distribution_date = (data.distribution_date) ? new Date(data.distribution_date) : null;
        this.notification_date = (data.notification_date) ? new Date(data.notification_date) : null;
        this.probability_of_success = (data.probability_of_success.length) ? data.probability_of_success : null;
        this.subject = (data.subject.length) ? data.subject : null;
        this.comments = (data.comments.length) ? data.comments : null;
        this.last_update = (data.last_update) ? new Date(data.last_update) : null;
        this.clients = (data.clients.length) ? data.clients : null;
        this.adverses = (data.adverses.length) ? data.adverses : null;
        this.adverse_lawyers = (data.adverse_lawyers.length) ? data.adverse_lawyers : null;
        
        
        if(data.connections.length){
            var conexaoList = [];
            angular.forEach(data.connections, function(conexao){
                conexaoList.push(new ConexaoFactory(conexao));
            });
            this.connections = conexaoList;
        } else this.connections = [];
        
        // Sets has_updates
        this.has_updates = !!(this.updates && this.updates.indexOf(Number(ModelService.getItem("user:id"))) !== -1);
        
        // Sets apensos array
        if(data.connections.length){
            if(typeof data.connections[0] === 'object'){
                var apensosList = [];
    
                angular.forEach(data.connections, function(apenso){
                    apensosList.push(apenso.id);
                });
    
                this.connections = apensosList;
            } else this.connections = data.connections;
        } else this.connections = null;
        
        // Sets last_update
        this.last_update = this.last_update ? moment(this.last_update).fromNow() : null;
        
        // Populates the responsible array with User objects
        this.responsible = [];
        angular.forEach(data.responsible, function(user){
            _this.responsible.push(new UserFactory(user));
        })
    };
    
    
    // Converts between state names and codes
    function convertEstado(estado){
        switch(estado){
            case 'AC':
                return 'Acre';
            case 'Acre':
                return 'AC';
            case 'AL':
                return 'Alagoas';
            case 'Alagoas':
                return 'AL';
            case 'AP':
                return 'Amapá';
            case 'Amapá':
                return 'AP';
            case 'AM':
                return 'Amazonas';
            case 'Amazonas':
                return 'AM';
            case 'BA':
                return 'Bahia';
            case 'Bahia':
                return 'BA';
            case 'CE':
                return 'Ceará';
            case 'Ceará':
                return 'CE';
            case 'DF':
                return 'Distrito Federal';
            case 'Distrito Federal':
                return 'DF';
            case 'ES':
                return 'Espírito Santo';
            case 'Espírito Santo':
                return 'ES';
            case 'GO':
                return 'Goiás';
            case 'Goiás':
                return 'GO';
            case 'MA':
                return 'Maranhão';
            case 'Maranhão':
                return 'MA';
            case 'MT':
                return 'Mato Grosso';
            case 'Mato Grosso':
                return 'MT';
            case 'MS':
                return 'Mato Grosso do Sul';
            case 'Mato Grosso do Sul':
                return 'MS';
            case 'MG':
                return 'Minas Gerais';
            case 'Minas Gerais':
                return 'MG';
            case 'PA':
                return 'Pará';
            case 'Pará':
                return 'PA';
            case 'PB':
                return 'Paraíba';
            case 'Paraíba':
                return 'PB';
            case 'PR':
                return 'Paraná';
            case 'Paraná':
                return 'PR';
            case 'PE':
                return 'Pernambuco';
            case 'Pernambuco':
                return 'PE';
            case 'PI':
                return 'Piauí';
            case 'Piauí':
                return 'PI';
            case 'RJ':
                return 'Rio de Janeiro';
            case 'Rio de Janeiro':
                return 'RJ';
            case 'RN':
                return 'Rio Grande do Norte';
            case 'Rio Grande do Norte':
                return 'RN';
            case 'RS':
                return 'Rio Grande do Sul';
            case 'Rio Grande do Sul':
                return 'RS';
            case 'RO':
                return 'Rondônia';
            case 'Rondônia':
                return 'RO';
            case 'RR':
                return 'Roraima';
            case 'Roraima':
                return 'RR';
            case 'SC':
                return 'Santa Catarina';
            case 'Santa Catarina':
                return 'SC';
            case 'SP':
                return 'São Paulo';
            case 'São Paulo':
                return 'SP';
            case 'SE':
                return 'Sergipe';
            case 'Sergipe':
                return 'SE';
            case 'TO':
                return 'Tocantins';
            case 'Tocantins':
                return 'TO';
        }
    }
        
    
    Process.prototype.isActive = function(){
        return this.process_status === 'ACTIVE';
    };
    
    Process.prototype.isSuspended = function(){
        return this.process_status === 'SUSPENDED';
    };
    
    Process.prototype.isArchived = function(){
        return this.process_status === 'ARCHIVED';
    };
    
    Process.prototype.lastUpdate = function(){
        return this.last_update ? moment(this.last_update).fromNow() : false;
    };
    
    Process.prototype.getResponsaveis = function(){
        return this.responsible.length === 1 ? '1 responsável' : this.responsible.length + ' responsáveis';
    };
    
    Process.prototype.getStateCode = function(){
        return convertEstado(this.state);
    };
    
    return Process;
}]);
