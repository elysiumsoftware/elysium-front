elysium.factory('RecursoFactory', ['ClienteFactory', 'AdversoFactory', 'AdvogadoAdversoFactory', function(ClienteFactory, AdversoFactory, AdvogadoAdversoFactory){
    var Recurso = function(data){
        this.id = data.id;
        this.creation_date = new Date(data.created_at);
        this.update_date = new Date(data.update_date);
        this.processo_do_recurso = data.process;
        this.processo_eletronico = data.electronic_process;
        
        this.data_de_citacao = data.notification_date ? new Date(data.notification_date) : null;
        this.data_de_distribuicao = data.distribution_date ? new Date(data.distribution_date) : null;
        this.data_de_status = data.status_date ? new Date(data.status_date) : null;
        this.numero_do_processo = data.process_number || null;
        this.observacoes = data.comments || null;
        this.orgao_judicial = data.judicial_organ || null;
        this.posicao_do_adverso = data.adverse_position || null;
        this.posicao_do_cliente = data.client_position || null;
        this.tipo_de_acao = data.appeal_type || null;
        
        this.clientes = (function(){
            if(data.clients){
                var clientList = [];
                angular.forEach(data.clients, function(client){
                    clientList.push(new ClienteFactory(client));
                });
                
                return clientList;
            } else return null;
        })();
    
        this.adversos = (function(){
            if(data.adverses){
                var adverseList = [];
                angular.forEach(data.adverses, function(adverse){
                    adverseList.push(new AdversoFactory(adverse));
                });
            
                return adverseList;
            } else return null;
        })();
    
        this.advogados_do_adverso = (function(){
            if(data.adverse_lawyers){
                var adverseLawyerList = [];
                angular.forEach(data.adverse_lawyers, function(adverseLawyer){
                    adverseLawyerList.push(new AdvogadoAdversoFactory(adverseLawyer));
                });
            
                return adverseLawyerList;
            } else return null;
        })();
    };
    
    
    return Recurso;
}]);
