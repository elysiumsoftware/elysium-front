elysium.factory('IncidenteFactory', [function(){
    var Incidente = function(data){
        this.id = data.id;
        this.creation_date = new Date(data.created_at);
        this.update_date = new Date(data.update_date);
        this.processo_do_incidente = data.process;
        
        this.observacoes = data.comments || null;
        this.titulo = data.incident_type || null;
    };
    
    
    return Incidente;
}]);
