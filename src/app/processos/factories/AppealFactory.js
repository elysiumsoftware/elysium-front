elysium.factory('AppealFactory', ['ClientFactory', 'AdverseFactory', 'AdverseLawyerFactory', function(ClientFactory, AdverseFactory, AdverseLawyerFactory){
    var Appeal = function(data){
        this.id = data.id;
        this.created_at = new moment(data.created_at);
        this.update_date = new moment(data.update_date);
        this.process = data.process;
        this.electronic_process = data.electronic_process;
        
        this.notification_date = data.notification_date ? new moment(data.notification_date) : null;
        this.distribution_date = data.distribution_date ? new moment(data.distribution_date) : null;
        this.status_date = data.status_date ? new moment(data.status_date) : null;
        this.process_number = data.process_number || null;
        this.comments = data.comments || null;
        this.judicial_organ = data.judicial_organ || null;
        this.adverse_position = data.adverse_position || null;
        this.client_position = data.client_position || null;
        this.appeal_type = data.appeal_type || null;
        
        this.clients = (function(){
            if(data.clients){
                var clientList = [];
                angular.forEach(data.clients, function(client){
                    clientList.push(new ClientFactory(client));
                });
                
                return clientList;
            } else return null;
        })();
    
        this.adverses = (function(){
            if(data.adverses){
                var adverseList = [];
                angular.forEach(data.adverses, function(adverse){
                    adverseList.push(new AdverseFactory(adverse));
                });
            
                return adverseList;
            } else return null;
        })();
    
        this.adverse_lawyers = (function(){
            if(data.adverse_lawyers){
                var adverseLawyerList = [];
                angular.forEach(data.adverse_lawyers, function(adverseLawyer){
                    adverseLawyerList.push(new AdverseLawyerFactory(adverseLawyer));
                });
            
                return adverseLawyerList;
            } else return null;
        })();
    };
    
    
    return Appeal;
}]);
