elysium.factory('ProcessoMinimalFactory', [function(){
    var ProcessoMinimal = function(data){
        this.id = data.id;
        this.is_public = data.is_public;
        this.status_do_processo = data.process_status;
        this.process_status = data.process_status;
        this.titulo = data.title;
        this.title = data.title;
        this.data_ultima_movimentacao = (data.last_update) ? new Date(data.last_update) : null;
        this.last_update = this.data_ultima_movimentacao;
        
        // Sets last_update
        this.last_update = this.data_ultima_movimentacao ? moment(this.data_ultima_movimentacao).fromNow() : null;
    };
    
    ProcessoMinimal.prototype.isActive = function(){
        return this.status_do_processo === 'ACTIVE';
    };
    
    ProcessoMinimal.prototype.isSuspended = function(){
        return this.status_do_processo === 'SUSPENDED';
    };
    
    ProcessoMinimal.prototype.isArchived = function(){
        return this.status_do_processo === 'ARCHIVED';
    };
    
    ProcessoMinimal.prototype.lastUpdate = function(){
        return this.data_ultima_movimentacao ? moment(this.data_ultima_movimentacao).fromNow() : false;
    };

    
    return ProcessoMinimal;
}]);
