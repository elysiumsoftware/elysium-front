elysium.factory('UpdateFactory', [function(){
    var Update = function(data){
        this.id = data.id;
        this.date = data.date;
        this.owner = data.owner;
        this.instance = data.instance;
        this.description = data.description;
        this.created_at = new moment(data.created_at);
    };

    return Update;
}]);
