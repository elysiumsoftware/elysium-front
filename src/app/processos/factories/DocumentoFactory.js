elysium.factory('DocumentoFactory', [function(){
    var Documento = function(data){
        this.id = data.id;
        this.nome = data.name;
        this.arquivo = data.file;
        this.processo = data.process;
        this.creation_date = data.created_at;
        this.update_date = data.updated_at;
        this.extensao = data.name.slice(data.name.search(/\.\w+$/i))
    };
    
    
    return Documento;
}]);
