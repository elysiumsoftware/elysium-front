elysium.factory('ProcessoFactory', ['ModelService', 'UserFactory', 'ConexaoFactory', function(ModelService, UserFactory, ConexaoFactory){
    var Processo = function(data){
        var _this = this;
        
        // Some items don't need to be checked for compatibility since
        // Python already send them JavaScript-compliant
        this.id = data.id;
        this.is_public = data.is_public;
        this.creation_date = new Date(data.created_at);
        this.update_date = new Date(data.update_date);
        this.processo_eletronico = data.electronic_process;
        this.valor_da_causa = data.cause_value;
        this.valor_provisionado = data.provisioned_value;
        this.status_do_processo = data.process_status;
        this.titulo = data.title;
        this.numero_do_processo = data.process_number;
        this.estado = convertEstado(data.state);
        
        // Everything else must be checked
        this.posicao_do_cliente = (data.client_position.length) ? data.client_position : null;
        this.posicao_do_adverso = (data.adverse_position.length) ? data.adverse_position : null;
        this.tipo_de_acao = (data.action_type.length) ? data.action_type : null;
        this.tipo_de_rito = (data.rite.length) ? data.rite : null;
        this.tipo_de_fase = (data.phase.length) ? data.phase : null;
        this.instancia = (data.instance.length) ? data.instance : null;
        this.tipo_de_processo = (data.process_type.length) ? data.process_type : null;
        this.orgao_judicial = (data.judicial_organ.length) ? data.judicial_organ : null;
        this.data_de_distribuicao = (data.distribution_date) ? new Date(data.distribution_date) : null;
        this.data_de_citacao = (data.notification_date) ? new Date(data.notification_date) : null;
        this.probabilidades_de_exito = (data.probability_of_success.length) ? data.probability_of_success : null;
        this.materia = (data.subject.length) ? data.subject : null;
        this.observacoes = (data.comments.length) ? data.comments : null;
        this.data_ultima_movimentacao = (data.last_update) ? new Date(data.last_update) : null;
        this.clientes = (data.clients.length) ? data.clients : null;
        this.adversos = (data.adverses.length) ? data.adverses : null;
        this.advogados_do_adverso = (data.adverse_lawyers.length) ? data.adverse_lawyers : null;
        
        
        if(data.connections.length){
            var conexaoList = [];
            angular.forEach(data.connections, function(conexao){
                conexaoList.push(new ConexaoFactory(conexao));
            });
            this.conexoes = conexaoList;
        } else this.conexoes = [];
        
        // Sets has_updates
        this.has_updates = !!(this.notificacoes && this.notificacoes.indexOf(Number(ModelService.getItem("user:id"))) != -1);
        
        // Sets apensos array
        if(data.connections.length){
            if(typeof data.connections[0] === 'object'){
                var apensosList = [];
    
                angular.forEach(data.connections, function(apenso){
                    apensosList.push(apenso.id);
                });
    
                this.apensos = apensosList;
            } else this.apensos = data.connections;
        } else this.apensos = null;
        
        // Sets last_update
        this.last_update = this.data_ultima_movimentacao ? moment(this.data_ultima_movimentacao).fromNow() : null;
        
        // Populates the responsible array with User objects
        this.responsaveis = [];
        angular.forEach(data.responsible, function(user){
            _this.responsaveis.push(new UserFactory(user));
        })
    };
    
    
    // Converts between state names and codes
    function convertEstado(estado){
        switch(estado){
            case 'AC':
                return 'Acre';
            case 'Acre':
                return 'AC';
            case 'AL':
                return 'Alagoas';
            case 'Alagoas':
                return 'AL';
            case 'AP':
                return 'Amapá';
            case 'Amapá':
                return 'AP';
            case 'AM':
                return 'Amazonas';
            case 'Amazonas':
                return 'AM';
            case 'BA':
                return 'Bahia';
            case 'Bahia':
                return 'BA';
            case 'CE':
                return 'Ceará';
            case 'Ceará':
                return 'CE';
            case 'DF':
                return 'Distrito Federal';
            case 'Distrito Federal':
                return 'DF';
            case 'ES':
                return 'Espírito Santo';
            case 'Espírito Santo':
                return 'ES';
            case 'GO':
                return 'Goiás';
            case 'Goiás':
                return 'GO';
            case 'MA':
                return 'Maranhão';
            case 'Maranhão':
                return 'MA';
            case 'MT':
                return 'Mato Grosso';
            case 'Mato Grosso':
                return 'MT';
            case 'MS':
                return 'Mato Grosso do Sul';
            case 'Mato Grosso do Sul':
                return 'MS';
            case 'MG':
                return 'Minas Gerais';
            case 'Minas Gerais':
                return 'MG';
            case 'PA':
                return 'Pará';
            case 'Pará':
                return 'PA';
            case 'PB':
                return 'Paraíba';
            case 'Paraíba':
                return 'PB';
            case 'PR':
                return 'Paraná';
            case 'Paraná':
                return 'PR';
            case 'PE':
                return 'Pernambuco';
            case 'Pernambuco':
                return 'PE';
            case 'PI':
                return 'Piauí';
            case 'Piauí':
                return 'PI';
            case 'RJ':
                return 'Rio de Janeiro';
            case 'Rio de Janeiro':
                return 'RJ';
            case 'RN':
                return 'Rio Grande do Norte';
            case 'Rio Grande do Norte':
                return 'RN';
            case 'RS':
                return 'Rio Grande do Sul';
            case 'Rio Grande do Sul':
                return 'RS';
            case 'RO':
                return 'Rondônia';
            case 'Rondônia':
                return 'RO';
            case 'RR':
                return 'Roraima';
            case 'Roraima':
                return 'RR';
            case 'SC':
                return 'Santa Catarina';
            case 'Santa Catarina':
                return 'SC';
            case 'SP':
                return 'São Paulo';
            case 'São Paulo':
                return 'SP';
            case 'SE':
                return 'Sergipe';
            case 'Sergipe':
                return 'SE';
            case 'TO':
                return 'Tocantins';
            case 'Tocantins':
                return 'TO';
        }
    }
        
    
    Processo.prototype.isActive = function(){
        return this.status_do_processo === 'ACTIVE';
    };
    
    Processo.prototype.isSuspended = function(){
        return this.status_do_processo === 'SUSPENDED';
    };
    
    Processo.prototype.isArchived = function(){
        return this.status_do_processo === 'ARCHIVED';
    };
    
    Processo.prototype.lastUpdate = function(){
        return this.data_ultima_movimentacao ? moment(this.data_ultima_movimentacao).fromNow() : false;
    };
    
    Processo.prototype.getResponsaveis = function(){
        return this.responsaveis.length === 1 ? '1 responsável' : this.responsaveis.length + ' responsáveis';
    };
    
    Processo.prototype.getStateCode = function(){
        return convertEstado(this.estado);
    };
    
    return Processo;
}]);
