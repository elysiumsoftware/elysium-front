elysium.factory('IncidentFactory', [function(){
    var Incident = function(data){
        this.id = data.id;
        this.created_at = new moment(data.created_at);
        this.update_date = new moment(data.update_date);
        this.process = data.process;
        
        this.comments = data.comments || null;
        this.incident_type = data.incident_type || null;
    };
    
    
    return Incident;
}]);
