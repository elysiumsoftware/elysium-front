elysium.factory('ConexaoFactory', [function(){
    var Conexao = function(data){
        this.id = data.id;
        this.is_public = data.is_public;
        this.status_do_processo = data.process_status || null;
        this.titulo = data.title || null;
    };
    
    
    Conexao.prototype.isActive = function(){
        return this.status_do_processo === 'ACTIVE';
    };
    
    Conexao.prototype.isSuspended = function(){
        return this.status_do_processo === 'SUSPENDED';
    };
    
    Conexao.prototype.isArchived = function(){
        return this.status_do_processo === 'ARCHIVED';
    };
    
    
    return Conexao;
}]);
