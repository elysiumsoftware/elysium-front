elysium.service('ProcessService', ['APIService', 'ModelService', 'ProcessFactory', 'AppealFactory', 'ToastService', 'IncidentFactory', '$q', 'ConexaoFactory', 'DocumentoFactory', 'Upload', 'ProcessoMinimalFactory', 'PublicationFactory', 'UpdateFactory', function(APIService, ModelService, ProcessFactory, AppealFactory, ToastService, IncidentFactory, $q, ConexaoFactory, DocumentoFactory, Upload, ProcessoMinimalFactory, PublicationFactory, UpdateFactory){
    this.getProcess = function(id){
        return APIService.GET('processes/' + id).then(function(response){
            return new ProcessFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar um processo', error);
            return $q.reject(error);
        });
    };

    this.getProcesses = function(){
        return APIService.GET('processes').then(function(response){
            var _processList = [];

            angular.forEach(response.data, function(process){
                _processList.push(new ProcessoMinimalFactory(process));
            });

            return _processList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar um processo', error);
            return $q.reject(error);
        });
    };

    this.createProcess = function(process){
        return APIService.POST('processes', process).then(function(response){
            return new ProcessFactory(response.data);
        }).catch(function(error){
            if(error.data.process_number && error.data.process_number[0] === 'A process with this number already exists.'){
                ToastService.showMessage('Já existe um processo cadastrado com esse mesmo número na sua empresa. Pesquise por ele na lista de processos ou altere o número do ' +
                    'processo que você está tentando cadastrar.', error);
            }
            else ToastService.showError('Algo deu errado ao criar um processo', error);
            return $q.reject(error);
        });
    };

    this.updateProcess = function(process){
        return APIService.PATCH('processes/' + process.id, process).then(function(response){
            return new ProcessFactory(response.data);
        }).catch(function(error){
            if(error.data.process_number && error.data.process_number[0] === 'A process with this number already exists.'){
                ToastService.showMessage('Já existe um processo cadastrado com esse mesmo número na sua empresa. Pesquise por ele na lista de processos ou altere o número do ' +
                    'processo que você está tentando cadastrar.', error);
            }
            else ToastService.showError('Algo deu errado ao modificar um processo', error);
            return $q.reject(error);
        });
    };

    this.getUpdates = function(id){
        return APIService.GET('processes/' + id + '/updates').then(function(response){
            var _updateList = [];

            angular.forEach(response.data, function(appeal){
                _updateList.push(new UpdateFactory(appeal));
            });

            return _updateList
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de movimentações desse processo', error);
            return $q.reject(error);
        });
    };

    this.createUpdate = function(id, update){
        return APIService.POST('processes/' + id + '/updates', update).then(function(response){
            return new UpdateFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar uma movimentação manualmente', error);
            return $q.reject(error);
        });
    };

    this.deleteUpdate = function(id){
        return APIService.DELETE('updates/' + id).then(function(){
            return true;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar uma movimentação', error);
            return $q.reject(error);
        });
    };

    this.updateUpdate = function(id, update){
        return APIService.PATCH('updates/' + id, update).then(function(response){
            return UpdateFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar uma movimentação', error);
            return $q.reject(error);
        });
    };

    this.getAppeals = function(id){
        return APIService.GET('processes/' + id + '/appeals').then(function(response){
            var _appealList = [];

            angular.forEach(response.data, function(appeal){
                _appealList.push(new AppealFactory(appeal));
            });

            return _appealList
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de recursos desse processo', error);
            return $q.reject(error);
        });
    };

    this.getIncidents = function(id){
        return APIService.GET('processes/' + id + '/incidents').then(function(response){
            var _incidentList = [];

            angular.forEach(response.data, function(incident){
                _incidentList.push(new IncidentFactory(incident));
            });

            return _incidentList
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de incidentes desse processo', error);
            return $q.reject(error);
        });
    };

    this.getPublications = function(id){
        return APIService.GET('processes/' + id + '/publications').then(function(response){
            var _publicationList = [];

            angular.forEach(response.data, function(publication){
                _publicationList.push(new PublicationFactory(publication))
            });

            return _publicationList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar as publicações desse processo.', error);
            return $q.reject(error);
        });
    }
}]);
