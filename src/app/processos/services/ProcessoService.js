elysium.service('ProcessoService', ['APIService', 'ModelService', 'ProcessoFactory', 'TarefaFactory', 'RecursoFactory', 'ToastService', 'IncidenteFactory', '$q', 'ConexaoFactory', 'DocumentoFactory', 'Upload', 'ProcessoMinimalFactory', 'APIURL', function(APIService, ModelService, ProcessoFactory, TarefaFactory, RecursoFactory, ToastService, IncidenteFactory, $q, ConexaoFactory, DocumentoFactory, Upload, ProcessoMinimalFactory, APIURL){
    var _this = this;
    
    _this.getProcesso = function(id){
        return ModelService.getProcesso(id).then(function(response){
            return new ProcessoFactory(response);
        });
    };
    
    _this.getProcessos = function(){
        return ModelService.getProcessos().then(function(response){
            var list = [];
            
            angular.forEach(response, function(processo){
                list.push(new ProcessoMinimalFactory(processo));
            });
            
            return list;
        });
    };

    _this.updateProcesso = function(processo, data){
        return ModelService.updateProcesso(processo.id, data).then(function(response){
            return new ProcessoFactory(response);
        });
    };
    
    _this.deleteProcesso = function(id){
        return ModelService.deleteProcesso(id).then(function(response){
            return response;
        });
    };
    
    _this.getTarefas = function(processo){
        return ModelService.getProcessoTarefas(processo.id).then(function(response){
            var tarefaList = [];
            
            angular.forEach(response, function(tarefa){
                tarefaList.push(new TarefaFactory(tarefa));
            });
            
            return tarefaList;
        });
    };
    
    _this.getMovimentacoes = function(processo){
        return ModelService.getProcessoMovimentacoes(processo.id).then(function(response){
            return response;
        });
    };
    
    _this.refreshMovimentacoes = function(processo){
        return APIService.GET('updates/proforcemovimentacoes/' + processo.id).then(function(response){
            return response.data;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar as movimentações de um processo.', error);
            return $q.reject();
        });
    };
    
    _this.getRecursos = function(processo){
        return ModelService.getProcessoRecursos(processo.id).then(function(response){
            var recursosList = [];
            
            angular.forEach(response, function(recurso){
                recursosList.push(new RecursoFactory(recurso));
            });
            
            return recursosList;
        });
    };
    
    _this.getIncidentes = function(processo){
        return APIService.GET('processes/' + processo.id + '/incidents').then(function(response){
            var incidentesList = [];
            
            angular.forEach(response.data, function(incidente){
                incidentesList.push(new IncidenteFactory(incidente));
            });
            
            return incidentesList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar os incidentes de um processo.', error);
            return $q.reject();
        });
    };
    
    _this.getRecurso = function(recurso){
        return ModelService.getRecurso(recurso.id).then(function(response){
            return new RecursoFactory(response);
        });
    };
    
    _this.deleteRecurso = function(recurso){
        return APIService.DELETE('processes/appeals/' + recurso.id).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar um recurso.', error);
            return $q.reject();
        });
    };
    
    _this.deleteIncidente = function(incidente){
        return APIService.DELETE('processes/incidents/' + incidente.id).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar um incidente.', error);
            return $q.reject();
        });
    };
    
    _this.createIncidente = function(incidente){
        var incident = {
            process: incidente.processo_do_incidente,
            responsible: incidente.responsaveis,
            comments: incidente.observacoes,
            incident_type: incidente.titulo
        };
        return APIService.POST('processes/incidents', incident).then(function(response){
            return new IncidenteFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar um incidente.', error);
            return $q.reject();
        });
    };
    
    _this.updateIncidente = function(incidente){
        var incident = {
            responsible: incidente.responsaveis,
            comments: incidente.observacoes,
            incident_type: incidente.titulo
        };
        return APIService.PATCH('processes/incidents/' + incidente.id, toJson(incident)).then(function(response){
            return new IncidenteFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao editar um incidente.', error);
            return $q.reject();
        });
    };
        
    _this.updateConexoes = function(processo){
        return APIService.PATCH('processes/' + processo.id, {connections: processo.apensos}).then(function(response){
            return new ProcessoFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao editar as conexões.', error);
            return $q.reject();
        });
    };
    
    _this.getDocumentos = function(processo){
        return APIService.GET('processes/' +  processo.id + '/documents').then(function(response){
            return fabricate(DocumentoFactory, response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar os documentos.', error);
            return $q.reject();
        });
    };
    
    _this.createDocumentos = function(files, invalidFiles, filesArray, uploadingFilesArray, processo){
        angular.forEach(invalidFiles, function(file){
            if(file.$error == 'pattern') ToastService.showError('O arquivo ' + file.name + ' não tem uma extensão válida e não será enviado.<br><br>As extensões válidas são: .pdf, .docx, .txt, .doc, .odt, .xls, .xlsx, .jpg, .png, .bmp e .gif.');
            else ToastService.showError('O arquivo ' + file.name + ' é inválido e não será enviado.<br><br>Tente recarregar a página e enviá-lo novamente. Se o erro persistir, entre em contato com o suporte.');
        });
        
        angular.forEach(files, function(file){
            // Checks if the file name is bigger than 200 characters
            if(file.name.length > 200){
                file.ext = file.name.slice(file.name.search(/\.\w+$/i));
                Upload.rename(file, file._name = file.name.slice(0, 196) + file.ext);
                
                ToastService.showMessage('Um dos arquivos selecionados tem o nome maior do que 250 caracteres e foi renomeado.<br><br>O novo nome do arquivo é: "' + file.name + '"');
            }
    
            var upload = Upload.upload({
                url: APIURL + "documents/",
                data: {
                    name: file.name,
                    file: file,
                    process: processo.id
                }
            });
            
            upload.file = file;
            uploadingFilesArray.push(upload);
            
            upload.then(function(response){
                uploadingFilesArray.splice(uploadingFilesArray.indexOf(upload), 1);
                filesArray.push(new DocumentoFactory(response.data))
            }, function(error){
                if(!upload.aborted) ToastService.showError('Algo deu errado ao enviar um arquivo.', error);
            }, function(event){
                file.progress = Math.min(100, parseInt(100.0 * event.loaded / event.total));
            });
        });
    };
    
    _this.deleteDocumento = function(documento){
        return APIService.DELETE('documents/' + documento.id).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar um documento.', error);
            return $q.reject();
        });
    };
    
    function fabricate(factory, data){
        var returnList = [];
        
        angular.forEach(data, function(object){
            returnList.push(new factory(object));
        });
        
        return returnList;
    }
    
    function toJson(object){
        return angular.forEach(object, function(value, key, obj){
            if(value == null) delete obj[key];
        });
    }
}]);
