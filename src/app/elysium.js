/*
 ** Elysium v0.2.0
 ** Todos os direitos reservados
 */

// Inicialização do módulo principal
var elysium = angular.module('elysium', [
    'ngRaven',
    'angular-loading-bar',
    'angular-medium-editor',
    'chart.js',
    'duScroll',
    'idf.br-filters',
    'mgcrea.ngStrap',
    'monospaced.elastic',
    'ngAnimate',
    'ngFileUpload',
    'ngSanitize',
    'ngToast',
    'perfect_scrollbar',
    'satellizer',
    'selectize',
    'ui.calendar',
    'ui.gravatar',
    'ui.router',
    'ui.utils.masks',
    'wu.masonry',
    'ui.select',
    'moment-picker'
]);

// Elysium's main API endpoint
// DO NOT CHANGE ANYTHING ON THE LINE BELOW, EXCEPT THE URL PARAMETER
// Gulp will replace the URL with the real Elysium API endpoint using regex on build
// The used regex is: /elysium.constant\(['"]APIURL['"],(\s)*['"](.*?)['"]\)/
elysium.constant('APIURL', 'http://127.0.0.1:8000/');

// Vindi's public API key
// DO NOT CHANGE ANYTHING ON THE LINE BELOW, EXCEPT THE KEY PARAMETER
// Gulp will replace the key on build if necessary
// The used regex is: /elysium.constant\(['"]VINDI_KEY['"],(\s)*['"](.*?)['"]\)/
elysium.constant('VINDI_KEY', 'hK-iWHdQLX-XD3A3zciUq6IwGvFUVeSXCJFGsSh5XZ4');
elysium.constant('VINDI_URL', 'https://app.vindi.com.br/api/v1/');

// Other useful constants
elysium.constant('AUTH_TOKEN_EXPIRATION', '16'); // Token expiration time, in hours

// Configurações do módulo principal
elysium.config([
    '$httpProvider',
    '$stateProvider',
    '$urlRouterProvider',
    'cfpLoadingBarProvider',
    'ngToastProvider',
    'ChartJsProvider',
    'gravatarServiceProvider',
    '$locationProvider',
    '$datepickerProvider',
    '$timepickerProvider',
    'uiSelectConfig',
    'momentPickerProvider',
    function(
        $httpProvider,
        $stateProvider,
        $urlRouterProvider,
        cfpLoadingBarProvider,
        ngToastProvider,
        ChartJsProvider,
        gravatarServiceProvider,
        $locationProvider,
        $datepickerProvider,
        $timepickerProvider,
        uiSelectConfig,
        momentPickerProvider
    ) {
    $httpProvider.interceptors.push('AuthenticationInterceptor');
    
    // Desabilita o hashtag # das urls
    $locationProvider.html5Mode(true);
    
    // Configuração simples para esconder o pequeno 'spinner' do plugin angular-loading-bar
    cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.latencyThreshold = 500;
    
    ChartJsProvider.setOptions({
        chartColors: ['#1ad477', '#16a9ff', '#ff9324', '#f566ed', '#f3e147', '#8f7fe2', '#4a4efb', '#555555', '#ff5161'],
        scaleFontFamily: "'Lato', Helvetica, Arial, sans-serif",
        bezierCurveTension : 0.5,
        animationSteps: 40,
        animationEasing: "easeOutQuart"
    });
    
    // Configurações padrões do Gravatar
    gravatarServiceProvider.defaults = {
        size: 70,
        "default": 'blank'
    };
    
    // Configurações do ui-select
    uiSelectConfig.theme = 'bootstrap';
    
    // Padrões do ngToast
    ngToastProvider.configure({
        animation: 'slide',
        timeout: 10000
    });
    
    // Padrões do Datepicker (angular-strap)
    angular.extend($datepickerProvider.defaults, {
        templateUrl: 'templates/bootstrap/datepicker.html'
    });
    
    // Padrões do Timepicker (angular-strap)
    angular.extend($timepickerProvider.defaults, {
        templateUrl: 'templates/bootstrap/timepicker.html',
        minuteStep: 15
    });
    
    // Padrões do Moment Picker
    momentPickerProvider.options({
        locale: 'pt-BR'
    });
    
    // Rota padrão do $router
    // Caso o usuário entre em uma rota que não existe ou é abstrata, ele será redirecionado para essa rota
    $urlRouterProvider.otherwise('/processos');
    
    // Routes processos/{id} to it's default child state, app.processos.details.processo
    $urlRouterProvider.when('/processos/{id:int}', '/processos/{id:int}/');
    
    // Template para uso rápido: .state('', {url: "", templateUrl: "", controller: ""})
    // Aqui são registradas todas as rotas do site, com seus templates e seus controladores
    $stateProvider
    .state('authentication', {url: '/', abstract: true, templateUrl: 'app/authentication/views/main.html'})
        .state('authentication.login', {url: '', params: {sref: null, params: null}, templateUrl: 'app/authentication/views/login/form.html', data: {titulo: 'Entrar'}})
        .state('authentication.register', {url: 'cadastro', templateUrl: 'app/authentication/views/register/form.html', data: {titulo: 'Crie sua conta'}})
        .state('authentication.registercomplete', {url: 'cadastro/concluido', params: {email: null}, templateUrl: 'app/authentication/views/register/complete.html', data: {titulo: 'Falta pouco...'}})
        .state('authentication.confirm', {url: 'cadastro/confirmar/{uid}/{token}', templateUrl: 'app/authentication/views/confirm/main.html', data: {titulo: 'Ativando sua conta'}})
        .state('authentication.forgotpassword', {url: 'senha/esqueci', templateUrl: 'app/authentication/views/password/forgot.html', data: {titulo: 'Esqueceu sua senha?'}})
        .state('authentication.resetpassword', {url: 'senha/nova/{uid}/{token}', templateUrl: 'app/authentication/views/password/reset.html', data: {titulo: 'Crie uma nova senha'}})
        .state('authentication.invitation', {url: 'convite/id/{key}', templateUrl: 'app/authentication/views/invitation/form.html', data: {titulo: 'Aceite seu convite'}})
        .state('authentication.company', {url: 'empresa', templateUrl: 'app/authentication/views/company.html', data: {titulo: 'Sua empresa'}})
        .state('authentication.billpaid', {url: 'empresa/fatura/paga', templateUrl: 'app/authentication/views/bill-paid.html', data: {titulo: 'Fatura paga'}})
        .state('authentication.billpending', {url: 'empresa/fatura/pendente', templateUrl: 'app/authentication/views/bill-pending.html', data: {titulo: 'Fatura pendente'}})
    .state('app', {url: '', abstract: true, templateUrl: "templates/main-body.html"})
        //.state('app.home', {url: "/inicio", templateUrl: "templates/main-inicio.html", data:{titulo: 'Início'}, controller:'HomeController as Home'})
        .state('app.processos', {url: "/processos", abstract: true, templateUrl: "app/processos/views/main.html", controller:"ProcessosController as Processos", data:{titulo: 'Processos'}})
            .state('app.processos.list', {url: '', templateUrl: 'app/processos/views/list.html', controller: 'ProcessosListController as ProcessosList'})
            .state('app.processos.details', {url: '/{id:int}', templateUrl: 'app/processos/views/details.html', controller: 'ProcessoDetailsController as ProcessoDetails'})
                .state('app.processos.details.processo', {url: '/', templateUrl: 'app/processos/views/processo.html', controller: 'ProcessoDetailsProcessoController as ProcessoDetailsProcesso'})
                .state('app.processos.details.tarefas', {url: '/tarefas', templateUrl: 'app/processos/views/tarefas.html', controller: 'ProcessoDetailsTarefasController as ProcessoDetailsTarefas'})
                .state('app.processos.details.movimentacoes', {url: '/movimentacoes', templateUrl: 'app/processos/views/movimentacoes.html', controller: 'ProcessoDetailsMovimentacoesController as ProcessoDetailsMovimentacoes'})
                .state('app.processos.details.publications', {url: '/publicacoes', templateUrl: 'app/processos/views/publications.html', controller: 'ProcessoDetailsPublicationsController as ProcessoDetailsPublications'})
                .state('app.processos.details.recursos', {url: '/recursos', templateUrl: 'app/processos/views/recursos.html', controller: 'ProcessoDetailsRecursosController as ProcessoDetailsRecursos'})
                .state('app.processos.details.incidentes', {url: '/incidentes', templateUrl: 'app/processos/views/incidentes.html', controller: 'ProcessoDetailsIncidentesController as ProcessoDetailsIncidentes'})
                .state('app.processos.details.conexoes', {url: '/conexoes', templateUrl: 'app/processos/views/conexoes.html', controller: 'ProcessoDetailsConexoesController as ProcessoDetailsConexoes'})
                .state('app.processos.details.documentos', {url: '/documentos', templateUrl: 'app/processos/views/documentos.html', controller: 'ProcessoDetailsDocumentosController as ProcessoDetailsDocumentos'})
            .state('app.processos.create', {url: '/novo', templateUrl: "app/processos/views/create.html", controller: "CreateProcessoController as CreateProcesso"})
            .state('app.processos.update', {url: '/editar/{id}', templateUrl: "app/processos/views/update/update.html", controller: "UpdateProcessController as UpdateProcess"})
        .state('app.person', {url: '/pessoas', templateUrl: "app/person/views/person.html", abstract: true, controller: "PersonController as Person", data:{titulo: 'Pessoas'}})
            .state('app.person.list', {url: '', templateUrl: "app/person/views/list.html", controller: "PersonListController as PersonList"})
            .state('app.person.create', {url: '/nova', params: {type: null}, templateUrl: "app/person/views/create.html", controller: "CreatePersonController as CreatePerson"})
            .state('app.person.update', {url: '/editar', params: {id: null, type: null}, templateUrl: "app/person/views/update.html", controller: "UpdatePersonController as UpdatePerson"})
            .state('app.person.client', {url: '/cliente/{id}', params: {client: null}, templateUrl: "app/person/views/client.html", controller: "ClientDetailsController as ClientDetails"})
            .state('app.person.adverse', {url: '/adverso/{id}', params: {adverse: null}, templateUrl: "app/person/views/adverse.html", controller: "AdverseDetailsController as AdverseDetails"})
            .state('app.person.adverselawyer', {url: '/advadverso/{id}', params: {adverse_lawyer: null}, templateUrl: "app/person/views/adverselawyer.html", controller: "AdverseLawyerDetailsController as AdverseLawyerDetails"})
            .state('app.person.correspondent', {url: '/correspondente/{id}', params: {correspondent: null}, templateUrl: "app/person/views/correspondent.html", controller: "CorrespondentDetailsController as CorrespondentDetails"})
        .state('app.tasks', {url: '/atividades', templateUrl: 'app/tasks/views/tasks.html', data: {titulo: 'Atividades'}, controller: 'TasksController as Tasks'})
        .state('app.publications', {url: '/publicacoes', abstract: true, templateUrl: 'app/publications/views/main.html', data: {titulo: 'Publicações'}})
            .state('app.publications.all', {url: '', params: {date: null}, templateUrl: 'app/publications/views/all.html', controller:'AllPublicationsController as AllPublications'})
            .state('app.publications.archived', {url: '/arquivadas', templateUrl: 'app/publications/views/archived.html', controller:'ArchivedPublicationsController as ArchivedPublications'})
        .state('app.configurations', {url: "/configuracoes", abstract: true, templateUrl: "app/configurations/views/main.html", data:{titulo: 'Configurações'}})
            .state('app.configurations.profile', {url: "/perfil", templateUrl: "app/configurations/views/profile.html"})
            .state('app.configurations.company', {url: "/empresa", templateUrl: "app/configurations/views/company.html"})
            .state('app.configurations.accounts', {url: "/contas", templateUrl: "app/configurations/views/accounts.html"})
            .state('app.configurations.publications', {url: "/publicacoes", templateUrl: "app/configurations/views/publications.html"})
        .state('app.stats', {url: "/estatisticas", templateUrl: "app/stats/views/main.html", controller: "StatsController as Stats", data:{titulo: 'Estatísticas'}})
    ;
}]);

// Configurações do Selectize
elysium.value('selectizeConfig', {
    onFocus: function() {
        $(this.$wrapper[0]).addClass('selectize-focus');
    },
    onBlur: function() {
        $(this.$wrapper[0]).removeClass('selectize-focus');
    }
});

// Código necessário para definir algumas classes CSS em alguns botões ativos do site
elysium.run(['$rootScope', '$state', '$stateParams', '$location', '$window', function ($rootScope, $state, $stateParams, $location, $window) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    
    //Código que notifica o Google Analytics sobre mudanças de página
    $rootScope.$on('$stateChangeSuccess', function(){
        if (!$window.ga) return;
        $window.ga('send', 'pageview', { page: $location.path() });
    });
}]);
