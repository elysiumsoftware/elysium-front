elysium.controller('TaskDetailsModalController',
    ['_Data', 'TaskService', 'UserService', 'ModalService', '$scope', '$rootScope',
    function(_Data, TaskService, UserService, ModalService, $scope, $rootScope)
{
    var _this = this;
    
    this.data = {
        task: {}
    };
    
    if(typeof _Data.task === 'object') _this.data.task = _Data.task;
    else TaskService.getTask(_Data.task).then(function(task){_this.data.task = task});
    
    UserService.getUsers().then(function(users){
        _this.data.users = users;
    });
    
    $scope.$on('tasks:task:delete', function(){
        $scope.$hide();
    });
    $scope.$on('tasks:task:update', function(event, task){
        _this.data.task = {};
        TaskService.getTask(task.id).then(function(newTask){_this.data.task = newTask;});
    });
    
    
    this.generateGoogleMapsUrl = function(){
        if(_this.data.task.location) return "https://www.google.com.br/maps/search/" + _this.data.task.location;
    };
    
    this.openDeleteTaskModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    task: _this.data.task
                }
            },
            templateUrl: 'app/tasks/views/modals/delete-task.html',
            controller: 'DeleteTaskModalController as DeleteTaskModal'
        });
    };
    
    this.openUpdateTaskModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    task: _this.data.task
                }
            },
            templateUrl: 'app/tasks/views/modals/update-task.html',
            controller: 'UpdateTaskModalController as UpdateTaskModal'
        });
    };
    
    this.togglePending = function(){
        if(_this.data.task.pending){
            TaskService.setComplete(_this.data.task).then(function(){
                _this.data.task.pending = false;
                $rootScope.$broadcast('tasks:task:update', _this.data.task);
            });
        } else{
            TaskService.setPending(_this.data.task).then(function(){
                _this.data.task.pending = true;
                $rootScope.$broadcast('tasks:task:update', _this.data.task);
            });
        }

    }
}]);
