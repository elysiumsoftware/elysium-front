elysium.controller('CreateTaskModalController',
    ['_Data', 'UserService', 'ProcessService', 'ModelService', '$scope', 'TaskService', '$rootScope', 'ToastService', 'AuthenticationService',
    function(_Data, UserService, ProcessService, ModelService, $scope, TaskService, $rootScope, ToastService, AuthenticationService)
{
    var _this = this;
    
    
    this.data = {
        task: {
            responsible: [],
            priority: 'NORMAL',
            start: _Data.date ? new moment(_Data.date).hours(9).minutes(0) : undefined,
            finish: _Data.date ? new moment(_Data.date).hours(10).minutes(0) : undefined
        },
        userName: AuthenticationService.getUser().name.split(' ')[0],
        userId: AuthenticationService.getUser().id,
        appeals: [],
        incidents: [],
        taskTypes: [
            'Audiência',
            'Compromisso',
            'Prazo'
        ],
        taskPriorities: [
            {value: "LOW", label:"Baixa"},
            {value: "NORMAL", label:"Normal"},
            {value: "HIGH", label:"Alta"}
        ],
        date: _Data.date || false
    };
    
    UserService.getUsers().then(function(users){
        _this.data.users = users;
    });
    ProcessService.getProcesses().then(function(processes){
        _this.data.processes = processes;
    });
    
    if(_Data.process){
        _this.data.task.process = _Data.process;
    
        ProcessService.getAppeals(_this.data.task.process).then(function(appeals){
            _this.data.appeals = appeals;
        });
        ProcessService.getIncidents(_this.data.task.process).then(function(incidents){
            _this.data.incidents = incidents;
        });
    }
    
    
    this.createTask = function(){
        if(!_this.data.task.responsible.length){
            $scope.$broadcast('app:tasks:create:form:responsible:focus');
            return;
        } else if(!_this.data.task.process){
            $scope.$broadcast('app:tasks:create:form:process:focus');
            return;
        } else if(!_this.data.task.start || (!(_this.data.task.start instanceof moment) || !_this.data.task.start.isValid())){
            angular.element('#form-start').focus();
            return;
        } else if(!_this.data.task.finish || (!(_this.data.task.finish instanceof moment) || !_this.data.task.finish.isValid()) || _this.data.task.start > _this.data.task.finish || _this.data.task.start.toISOString() === _this.data.task.finish.toISOString()){
            angular.element('#form-finish').focus();
            return;
        }
        
        var _task = angular.copy(_this.data.task);
        
        _task.start = _task.start.format('YYYY-MM-DD[T]HH:mm:ss.sss[Z]');
        _task.finish = _task.finish.format('YYYY-MM-DD[T]HH:mm:ss.sss[Z]');
        
        TaskService.createTask(_task).then(function(task){
            var _notify = false;

            for(var i = 0; i < task.responsible.length; i++) if(task.responsible[i].id !== _this.data.userId) _notify = true;
            if(_notify) ToastService.showMessage('A tarefa "' + task.title + '" foi criada com sucesso. Os usuários responsáveis poderão vê-la em seus calendários.');

            $rootScope.$broadcast('tasks:task:create', task);
            $scope.$hide();
        });
    };
    
    this.updateProcess = function(){
        _this.data.appeals = [], _this.data.incidents = [], _this.data.task.appeal = undefined, _this.data.task.incident = undefined;
        
        ProcessService.getAppeals(_this.data.task.process).then(function(appeals){
            _this.data.appeals = appeals;
        });
        ProcessService.getIncidents(_this.data.task.process).then(function(incidents){
            _this.data.incidents = incidents;
        });
    };
    
    this.setFinish = function(){
        _this.data.task.finish = new moment(_this.data.task.start).add(1, 'hour');
    };
    
    /**
     * Sets the current user as responsible for this process
     */
    this.setUserAsResponsible = function(){
        if(_this.data.task.responsible.indexOf(_this.data.userId) === -1) _this.data.task.responsible.push(_this.data.userId);
    };
    
    this.setStartAsToday = function(){
        if(_this.data.task.start && _this.data.task.start){
            var _hours = _this.data.task.start.hours();
            var _minutes = _this.data.task.start.minutes();
    
            _this.data.task.start = new moment().utcOffset(0, true).hours(_hours).minutes(_minutes);
        } else{
            _this.data.task.start = new moment().utcOffset(0, true).hours(9).minutes(0);
        }
    };
    
    this.setStartAs9AM = function(){
        if(_this.data.task.start && _this.data.task.start.isValid()) _this.data.task.start.hours(9).minutes(0);
        else _this.setStartAsToday();
    };
    
    this.setFinishAsStart = function(){
        if(_this.data.task.start && _this.data.task.start.isValid()) _this.data.task.finish = new moment(_this.data.task.start).add(1, 'hour');
    };
    
    this.add1DayToFinish = function(){
        if(_this.data.task.finish.isValid()) _this.data.task.finish.add(1, 'day');
    };
    
    this.add1HourToFinish = function(){
        if(_this.data.task.finish && _this.data.task.finish.isValid()) _this.data.task.finish.add(1, 'hour');
        else if(_this.data.task.start && _this.data.task.start.isValid()) _this.data.task.finish = new moment(_this.data.task.start).add(1, 'hour');
    };
}]);
