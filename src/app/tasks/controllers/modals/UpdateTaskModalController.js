elysium.controller('UpdateTaskModalController',
    ['_Data', 'UserService', 'ProcessService', 'ModelService', '$scope', 'TaskService', '$rootScope', 'ToastService', '$q', 'AuthenticationService',
    function(_Data, UserService, ProcessService, ModelService, $scope, TaskService, $rootScope, ToastService, $q, AuthenticationService)
{
    var _this = this;
    
    
    this.data = {
        task: angular.copy(_Data.task),
        userName: AuthenticationService.getUser().name.split(' ')[0],
        userId: AuthenticationService.getUser().id,
        appeals: [],
        incidents: [],
        taskTypes: [
            'Audiência',
            'Compromisso',
            'Prazo'
        ],
        taskPriorities: [
            {value: "LOW", label:"Baixa"},
            {value: "NORMAL", label:"Normal"},
            {value: "HIGH", label:"Alta"}
        ],
        originalTask: angular.copy(_Data.task),
        loading: true
    };
    
    if(_this.data.task.responsible.length){
        var _responsibleList = [];
        
        angular.forEach(_this.data.task.responsible, function(responsible){
            _responsibleList.push(responsible.id);
        });
    
        _this.data.task.responsible = _responsibleList;
    }
    _this.data.task.process = _this.data.task.process.id;
    if(_this.data.task.appeal) _this.data.task.appeal = _this.data.task.appeal.id;
    if(_this.data.task.incident) _this.data.task.incident = _this.data.task.incident.id;
    _this.data.task.start.utc();
    _this.data.task.finish.utc();
    
    $q.all({
        users: UserService.getUsers(),
        processes: ProcessService.getProcesses(),
        appeals: ProcessService.getAppeals(_this.data.task.process),
        incidents: ProcessService.getIncidents(_this.data.task.process)
    }).then(function(responses){
        _this.data.users = responses.users;
        _this.data.processes = responses.processes;
        _this.data.appeals = responses.appeals;
        _this.data.incidents = responses.incidents;
        _this.data.loading = false;
    }).catch(function(error){
        ToastService.showError('Algo deu errado ao carregar as informações necessárias para editar essa tarefa', error);
    });
    
    
    
    this.updateTask = function(){
        if(!_this.data.task.responsible.length){
            $scope.$broadcast('app:tasks:create:form:responsible:focus');
            return;
        } else if(!_this.data.task.process){
            $scope.$broadcast('app:tasks:create:form:process:focus');
            return;
        } else if(!_this.data.task.start || (!(_this.data.task.start instanceof moment) || !_this.data.task.start.isValid())){
            angular.element('#form-start').focus();
            return;
        } else if(!_this.data.task.finish || (!(_this.data.task.finish instanceof moment) || !_this.data.task.finish.isValid()) || _this.data.task.start > _this.data.task.finish){
            angular.element('#form-finish').focus();
            return;
        }
        
        var _task = angular.copy(_this.data.task);
        
        if(_task.appeal) _task.incident = null;
        if(_task.incident) _task.appeal = null;
        if(_task.description === null) delete _task.description;
        if(_task.location === null) delete _task.location;
        if(_task.type === null) delete _task.type;
        
        if(_task.pending !== undefined) delete _task.pending;
        
        _task.start = _task.start.format('YYYY-MM-DD[T]HH:mm:ss.sss[Z]');
        _task.finish = _task.finish.format('YYYY-MM-DD[T]HH:mm:ss.sss[Z]');
        
        TaskService.updateTask(_this.data.originalTask, _task).then(function(task){
            var _notify = false;

            for(var i = 0; i < task.responsible.length; i++) if(task.responsible[i].id !== _this.data.userId) _notify = true;
            if(_notify) ToastService.showMessage('A tarefa "' + task.title + '" foi atualizada com sucesso. Os usuários responsáveis poderão vê-la em seus calendários.');

            $rootScope.$broadcast('tasks:task:update', task);
            $scope.$hide();
        });
    };
    
    this.updateProcess = function(){
        _this.data.appeals = [], _this.data.incidents = [], _this.data.task.appeal = undefined, _this.data.task.incident = undefined;
        
        ProcessService.getAppeals(_this.data.task.process).then(function(appeals){
            _this.data.appeals = appeals;
        });
        ProcessService.getIncidents(_this.data.task.process).then(function(incidents){
            _this.data.incidents = incidents;
        });
    };
    
    this.setFinish = function(){
        _this.data.task.finish = new moment(_this.data.task.start).add(1, 'hour');
    };
    
    /**
     * Sets the current user as responsible for this process
     */
    this.setUserAsResponsible = function(){
        if(_this.data.task.responsible.indexOf(_this.data.userId) === -1) _this.data.task.responsible.push(_this.data.userId);
    };
    
    this.setStartAsToday = function(){
        if(_this.data.task.start && _this.data.task.start){
            var _hours = _this.data.task.start.hours();
            var _minutes = _this.data.task.start.minutes();
        
            _this.data.task.start = new moment().utcOffset(0, true).hours(_hours).minutes(_minutes);
        } else{
            _this.data.task.start = new moment().utcOffset(0, true).hours(9).minutes(0);
        }
    };
    
    this.setStartAs9AM = function(){
        if(_this.data.task.start && _this.data.task.start.isValid()) _this.data.task.start.hours(9).minutes(0);
        else _this.setStartAsToday();
    };
    
    this.setFinishAsStart = function(){
        if(_this.data.task.start && _this.data.task.start.isValid()) _this.data.task.finish = new moment(_this.data.task.start).add(1, 'hour');
    };
    
    this.add1DayToFinish = function(){
        if(_this.data.task.finish.isValid()) _this.data.task.finish.add(1, 'day');
    };
    
    this.add1HourToFinish = function(){
        if(_this.data.task.finish && _this.data.task.finish.isValid()) _this.data.task.finish.add(1, 'hour');
        else if(_this.data.task.start && _this.data.task.start.isValid()) _this.data.task.finish = new moment(_this.data.task.start).add(1, 'hour');
    };
}]);
