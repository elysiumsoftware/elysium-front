elysium.controller('DeleteTaskModalController', ['_Data', 'TaskService', '$rootScope', '$scope', function(_Data, TaskService, $rootScope, $scope){
    var _this = this;
    
    console.debug(_Data);
    
    this.data = {
        task: _Data.task
    };
    
    
    this.deleteTask = function(){
        TaskService.deleteTask(_this.data.task).then(function(task){
            $rootScope.$broadcast('tasks:task:delete', task);
            $scope.$hide();
        });
    }
}]);
