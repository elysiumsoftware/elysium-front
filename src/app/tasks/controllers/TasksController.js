elysium.controller
       ('TasksController', ['TaskService', 'ModalService', 'uiCalendarConfig', '$scope', 'ToastService', '$rootScope',
       function(TaskService, ModalService, uiCalendarConfig, $scope, ToastService, $rootScope)
{
    var _this = this;
    
    
    this.data = {
        calendar: {
            lang: 'pt-br',
            defaultView: 'month',
            timezone: 'UTC',
            now: new moment().utcOffset(0, true),
            editable: false,
            theme: false,
            eventLimit: true,
            fixedWeekCount: false,
            eventOrder: 'start',
            timeFormat: 'H:mm',
            eventStartEditable: true,
            nextDayThreshold: '00:00:00',
            header:{
                left: 'prev',
                center: 'title',
                right: 'today next'
            },
            eventLimitText: function(count){
                if(count === 1) return '+1 tarefa';
                else return '+' + count + ' tarefas';
            },
            events: function(start, end, timezone, callback){
                TaskService.getTasksByRange(start.toISOString(), end.toISOString()).then(function(tasks){
                    angular.forEach(tasks, function(task){
                        task.end = task.finish;
                
                        task.className = [task.priority.toLowerCase()];
                        if(!task.pending) task.className.push('done');
                    });
            
                    callback(tasks);
                });
            },
            dayClick: function(date){
                ModalService.setModal({
                    locals: {
                        _Data: {
                            date: date
                        }
                    },
                    templateUrl: 'app/tasks/views/modals/create-task.html',
                    controller: 'CreateTaskModalController as CreateTaskModal',
                    backdrop: 'static'
                });
            },
            eventClick: function(event){
                ModalService.setModal({
                    locals: {
                        _Data: {
                            task: event.id
                        }
                    },
                    templateUrl: 'app/tasks/views/modals/task-details.html',
                    controller: 'TaskDetailsModalController as TaskDetailsModal'
                });
            },
            loading: function(isLoading){
                if(isLoading){
                    if(uiCalendarConfig.calendars.MainCalendar) jQuery(uiCalendarConfig.calendars.MainCalendar[0]).addClass('loading-events');
                } else{
                    if(uiCalendarConfig.calendars.MainCalendar) jQuery(uiCalendarConfig.calendars.MainCalendar[0]).removeClass('loading-events');}
            },
            eventDrop: function(event, delta, revertFunc, jsEvent){
                TaskService.reschedule(event, event.start, event.end).then(function(task){
                    $rootScope.$broadcast('tasks:task:drag', task);
                }).catch(function(error){
                    ToastService.showError('Algo deu errado ao mudar a data de uma tarefa', error);
                    revertFunc();
                });
            }
        }
    };
    
    $scope.$on('tasks:task:create', function(){
        uiCalendarConfig.calendars.MainCalendar.fullCalendar('refetchEvents');
    });
    $scope.$on('tasks:task:delete', function(){
        uiCalendarConfig.calendars.MainCalendar.fullCalendar('refetchEvents');
    });
    $scope.$on('tasks:task:update', function(){
        uiCalendarConfig.calendars.MainCalendar.fullCalendar('refetchEvents');
    });
    
    
    this.createNewTaskModal = function(date){
        ModalService.setModal({
            locals: {
                _Data: {
                    date: date
                }
            },
            templateUrl: 'app/tasks/views/modals/create-task.html',
            controller: 'CreateTaskModalController as CreateTaskModal',
            backdrop: 'static'
        });
    }
}]);
