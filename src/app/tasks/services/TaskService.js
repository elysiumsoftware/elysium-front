elysium.service('TaskService', ['APIService', '$q', 'ToastService', 'TaskFactory', function(APIService, $q, ToastService, TaskFactory){
    
    this.getTask = function(id){
        return APIService.GET('tasks/' + id).then(function(response){
            return new TaskFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar uma tarefa', error);
            return $q.reject(error);
        });
    };
    
    
    this.getTasksByRange = function(start, end){
        return APIService.GET('tasks/range/' + start + '/' + end).then(function(response){
            var _taskList = [];
            
            angular.forEach(response.data, function(task){
                _taskList.push(new TaskFactory(task))
            });
            
            return _taskList;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de tarefas', error);
            return $q.reject(error);
        });
    };
    
    
    this.createTask = function(task){
        return APIService.POST('tasks', task).then(function(response){
            return new TaskFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar uma tarefa', error);
            return $q.reject(error);
        });
    };
    
    this.updateTask = function(task, data){
        return APIService.PATCH('tasks/' + task.id, data).then(function(response){
            return new TaskFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar uma tarefa', error);
            return $q.reject(error);
        });
    };

    this.reschedule = function(task, start, finish){
        return APIService.POST('tasks/' + task.id + '/reschedule', {start: start, finish: finish}).then(function(response){
            return new TaskFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar uma tarefa', error);
            return $q.reject(error);
        });
    };

    this.setPending = function(task){
        return APIService.POST('tasks/' + task.id + '/pending', {}).catch(function(error){
            ToastService.showError('Algo deu errado ao marcar uma tarefa como pendente', error);
            return $q.reject(error);
        });
    };

    this.setComplete = function(task){
        return APIService.POST('tasks/' + task.id + '/complete', {}).catch(function(error){
            ToastService.showError('Algo deu errado ao marcar uma tarefa como concluída', error);
            return $q.reject(error);
        });
    };
    
    
    this.deleteTask = function(task){
        return APIService.DELETE('tasks/' + task.id).then(function(){
            return task;
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao apagar uma tarefa', error);
            return $q.reject(error);
        });
    }
    
    
}]);
