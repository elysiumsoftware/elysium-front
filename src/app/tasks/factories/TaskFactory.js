elysium.factory('TaskFactory', ['UserFactory', 'ProcessFactory', 'AppealFactory', 'IncidentFactory', function(UserFactory, ProcessFactory, AppealFactory, IncidentFactory){
    var Task = function(data){
        this.id = data.id;
        this.access_lvl = data.access_lvl;
        this.is_public = data.is_public;
        this.created_at = new moment(data.created_at);
        this.update_date = new moment(data.update_date);
        
        this.title = data.title || null;
        this.type = data.type || null;
        this.start  = data.start ? new moment(data.start) : null;
        this.finish  = data.finish ? new moment(data.finish) : null;
        this.conclusion  = data.conclusion ? new moment(data.conclusion) : null;
        this.location  = data.location  || null;
        this.pending  = data.pending  || null;
        this.priority  = data.priority  || null;
        this.description  = data.description  || null;
        
        this.responsible = (function(){
            if(typeof(data.responsible === 'number')) return data.responsible;
            else if(typeof data.responsible === 'object'){
                var _userList = [];
                angular.forEach(data.responsible, function(user){
                    _userList.push(new UserFactory(user));
                });
                return _userList;
            }
            else return null;
        })();
    
        this.process = (function(){
            if(typeof(data.process === 'number')) return data.process;
            else if(typeof data.process === 'object') return new UserFactory(data.process);
            else return null;
        })();
    
        this.incident = (function(){
            if(typeof(data.incident === 'number')) return data.incident;
            else if(typeof data.incident === 'object') return new UserFactory(data.incident);
            else return null;
        })();
    
        this.appeal = (function(){
            if(typeof(data.appeal === 'number')) return data.appeal;
            else if(typeof data.appeal === 'object') return new UserFactory(data.appeal);
            else return null;
        })();
    };
    
    
    return Task;
}]);
