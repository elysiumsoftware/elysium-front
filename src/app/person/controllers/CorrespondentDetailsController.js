elysium.controller('CorrespondentDetailsController', ['$stateParams', 'PersonService', 'ModalService', '$state', '$scope', function($stateParams, PersonService, ModalService, $state, $scope){
    var _this = this;
    
    
    this.data = {
        correspondent: {}
    };
    
    if($stateParams.correspondent) _this.data.correspondent = $stateParams.correspondent;
    else{
        PersonService.getCorrespondent($stateParams.id).then(function(correspondent){
            _this.data.correspondent = correspondent;
        });
    }
    
    $scope.$on('app:person:delete', function(){
        $state.go('app.person.list');
    });
    
    
    this.openDeleteModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    person: _this.data.correspondent,
                    type: 'correspondent'
                }
            },
            templateUrl: 'app/person/views/modal/delete.html',
            controller: 'DeletePersonModalController as DeletePersonModal',
            autoClose: true
        });
    }
}]);
