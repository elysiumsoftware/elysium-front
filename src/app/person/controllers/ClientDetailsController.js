elysium.controller('ClientDetailsController', ['$stateParams', 'PersonService', 'ModalService', '$state', '$scope', function($stateParams, PersonService, ModalService, $state, $scope){
    var _this = this;
    
    
    this.data = {
        client: {}
    };
    
    if($stateParams.client) _this.data.client = $stateParams.client;
    else{
        PersonService.getClient($stateParams.id).then(function(client){
            _this.data.client = client;
        });
    }
    
    $scope.$on('app:person:delete', function(){
        $state.go('app.person.list');
    });
    
    
    this.openDeleteModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    person: _this.data.client,
                    type: 'client'
                }
            },
            templateUrl: 'app/person/views/modal/delete.html',
            controller: 'DeletePersonModalController as DeletePersonModal',
            autoClose: true
        });
    }
}]);
