elysium.controller('AdverseLawyerDetailsController', ['$stateParams', 'PersonService', 'ModalService', '$state', '$scope', function($stateParams, PersonService, ModalService, $state, $scope){
    var _this = this;
    
    
    this.data = {
        adverse_lawyer: {}
    };
    
    if($stateParams.adverse_lawyer) _this.data.adverse_lawyer = $stateParams.adverse_lawyer;
    else{
        PersonService.getAdverseLawyer($stateParams.id).then(function(adverse_lawyer){
            _this.data.adverse_lawyer = adverse_lawyer;
        });
    }
    
    $scope.$on('app:person:delete', function(){
        $state.go('app.person.list');
    });
    
    
    this.openDeleteModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    person: _this.data.adverse_lawyer,
                    type: 'adverse_lawyer'
                }
            },
            templateUrl: 'app/person/views/modal/delete.html',
            controller: 'DeletePersonModalController as DeletePersonModal',
            autoClose: true
        });
    }
}]);
