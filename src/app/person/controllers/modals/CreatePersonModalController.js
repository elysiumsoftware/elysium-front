elysium.controller('CreatePersonModalController', ['_Data', 'PersonService', '$scope', function(_Data, PersonService, $scope){
    var _this = this;
    
    
    _this.data = {
        person: {}
    };
    
    
    _this.createPerson = function(){
        if(_Data.type === 'client') PersonService.createClient(_this.data.person).then(function(response){callback(response)});
        else if(_Data.type === 'adverse') PersonService.createAdverse(_this.data.person).then(function(response){callback(response)});
        else if(_Data.type === 'adverse_lawyer') PersonService.createAdverseLawyer(_this.data.person).then(function(response){callback(response)});
    };
    
    _this.getType = function(){
        if(_Data.type === 'client') return 'cliente';
        else if(_Data.type === 'adverse') return 'adverso';
        else if(_Data.type === 'adverse_lawyer') return 'advogado adverso';
    };
    
    function callback(response){
        _Data.callback(response, _Data.type);
        $scope.$hide();
    }
}]);
