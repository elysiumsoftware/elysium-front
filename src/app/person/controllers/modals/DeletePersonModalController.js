elysium.controller('DeletePersonModalController', ['_Data', 'PersonService', '$rootScope', '$scope', function(_Data, PersonService, $rootScope, $scope){
    var _this = this;
    
    
    this.data = {
        person: _Data.person,
        personType: _Data.type || _Data.person.type
    };
    
    if(_this.data.personType === 'client') _this.data.type = 'cliente';
    else if(_this.data.personType === 'adverse') _this.data.type = 'adverso';
    else if(_this.data.personType === 'adverse_lawyer' || _this.data.personType === 'adverselawyer') _this.data.type = 'adv. adverso';
    else if(_this.data.personType === 'correspondent') _this.data.type = 'correspondente';
    
    
    this.delete = function(){
        if(_this.data.personType === 'client'){
            PersonService.deleteClient(_this.data.person).then(function(){
                $rootScope.$broadcast('app:person:delete', _this.data.person);
                $scope.$hide();
            });
        } else if(_this.data.personType === 'adverse'){
            PersonService.deleteAdverse(_this.data.person).then(function(){
                $rootScope.$broadcast('app:person:delete', _this.data.person);
                $scope.$hide();
            });
        } else if(_this.data.personType === 'adverse_lawyer' || _this.data.personType === 'adverselawyer'){
            PersonService.deleteAdverseLawyer(_this.data.person).then(function(){
                $rootScope.$broadcast('app:person:delete', _this.data.person);
                $scope.$hide();
            });
        } else if(_this.data.personType === 'correspondent'){
            PersonService.deleteCorrespondent(this.data.person).then(function(){
                $rootScope.$broadcast('app:person:delete', _this.data.person);
                $scope.$hide();
            });
        }
    }
}]);
