elysium.controller('PersonController', ['$state', '$rootScope', function($state, $rootScope){
    var _this = this;
    
    
    this.personFilter = '';
    
    
    this.changeFilter = function(type){
        _this.personFilter = type;
        
        if(!$state.is('app.person.list')) $state.go('app.person.list');
    };
    
    this.openCreatePersonForm = function(type){
        if($state.is('app.person.create')) $rootScope.$broadcast('app:person:create:changeType', type);
        else $state.go('app.person.create', {type: type})
    };
}]);
