elysium.controller('AdverseDetailsController', ['$stateParams', 'PersonService', 'ModalService', '$state', '$scope', function($stateParams, PersonService, ModalService, $state, $scope){
    var _this = this;
    
    
    this.data = {
        adverse: {}
    };
    
    if($stateParams.adverse) _this.data.adverse = $stateParams.adverse;
    else{
        PersonService.getAdverse($stateParams.id).then(function(adverse){
            _this.data.adverse = adverse;
        });
    }
    
    $scope.$on('app:person:delete', function(){
        $state.go('app.person.list');
    });
    
    
    this.openDeleteModal = function(){
        ModalService.setModal({
            locals: {
                _Data: {
                    person: _this.data.adverse,
                    type: 'adverse'
                }
            },
            templateUrl: 'app/person/views/modal/delete.html',
            controller: 'DeletePersonModalController as DeletePersonModal',
            autoClose: true
        });
    }
}]);
