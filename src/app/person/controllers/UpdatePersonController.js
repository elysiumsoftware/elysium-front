elysium.controller('UpdatePersonController', ['PersonService', '$stateParams', '$state', function(PersonService, $stateParams, $state){
    var _this = this;
    
    
    this.data = {
        personType: $stateParams.type,
        states: [
            'Acre',
            'Alagoas',
            'Amazonas',
            'Amapá',
            'Bahia',
            'Ceará',
            'Distrito Federal',
            'Espírito Santo',
            'Goiás',
            'Maranhão',
            'Mato Grosso',
            'Mato Grosso do Sul',
            'Minas Gerais',
            'Pará',
            'Paraíba',
            'Paraná',
            'Pernambuco',
            'Piauí',
            'Rio de Janeiro',
            'Rio Grande do Norte',
            'Rondônia',
            'Rio Grande do Sul',
            'Roraima',
            'Santa Catarina',
            'Sergipe',
            'São Paulo',
            'Tocantins'
        ],
        countries: [
            'África do Sul',
            'Albânia',
            'Alemanha',
            'Andorra',
            'Angola',
            'Anguilla',
            'Antigua',
            'Arábia Saudita',
            'Argentina',
            'Armênia',
            'Aruba',
            'Austrália',
            'Áustria',
            'Azerbaijão',
            'Bahamas',
            'Bahrein',
            'Bangladesh',
            'Barbados',
            'Bélgica',
            'Benin',
            'Bermudas',
            'Botsuana',
            'Brasil',
            'Brunei',
            'Bulgária',
            'Burkina Faso',
            'botão',
            'Cabo Verde',
            'Camarões',
            'Camboja',
            'Canadá',
            'Cazaquistão',
            'Chade',
            'Chile',
            'China',
            'Cidade do Vaticano',
            'Colômbia',
            'Congo',
            'Coreia do Sul',
            'Costa do Marfim',
            'Costa Rica',
            'Croácia',
            'Dinamarca',
            'Djibuti',
            'Dominica',
            'EUA',
            'Egito',
            'El Salvador',
            'Emirados Árabes',
            'Equador',
            'Eritreia',
            'Escócia',
            'Eslováquia',
            'Eslovênia',
            'Espanha',
            'Estônia',
            'Etiópia',
            'Fiji',
            'Filipinas',
            'Finlândia',
            'França',
            'Gabão',
            'Gâmbia',
            'Gana',
            'Geórgia',
            'Gibraltar',
            'Granada',
            'Grécia',
            'Guadalupe',
            'Guam',
            'Guatemala',
            'Guiana',
            'Guiana Francesa',
            'Guiné-bissau',
            'Haiti',
            'Holanda',
            'Honduras',
            'Hong Kong',
            'Hungria',
            'Iêmen',
            'Ilhas Cayman',
            'Ilhas Cook',
            'Ilhas Curaçao',
            'Ilhas Marshall',
            'Ilhas Turks & Caicos',
            'Ilhas Virgens (brit.)',
            'Ilhas Virgens (amer.)',
            'Ilhas Wallis e Futuna',
            'Índia',
            'Indonésia',
            'Inglaterra',
            'Irlanda',
            'Islândia',
            'Israel',
            'Itália',
            'Jamaica',
            'Japão',
            'Jordânia',
            'Kuwait',
            'Latvia',
            'Líbano',
            'Liechtenstein',
            'Lituânia',
            'Luxemburgo',
            'Macau',
            'Macedônia',
            'Madagascar',
            'Malásia',
            'Malaui',
            'Mali',
            'Malta',
            'Marrocos',
            'Martinica',
            'Mauritânia',
            'Mauritius',
            'México',
            'Moldova',
            'Mônaco',
            'Montserrat',
            'Nepal',
            'Nicarágua',
            'Niger',
            'Nigéria',
            'Noruega',
            'Nova Caledônia',
            'Nova Zelândia',
            'Omã',
            'Palau',
            'Panamá',
            'Papua-nova Guiné',
            'Paquistão',
            'Peru',
            'Polinésia Francesa',
            'Polônia',
            'Porto Rico',
            'Portugal',
            'Qatar',
            'Quênia',
            'Rep. Dominicana',
            'Rep. Tcheca',
            'Reunion',
            'Romênia',
            'Ruanda',
            'Rússia',
            'Saipan',
            'Samoa Americana',
            'Senegal',
            'Serra Leone',
            'Seychelles',
            'Singapura',
            'Síria',
            'Sri Lanka',
            'St. Kitts & Nevis',
            'St. Lúcia',
            'St. Vincent',
            'Sudão',
            'Suécia',
            'Suíça',
            'Suriname',
            'Tailândia',
            'Taiwan',
            'Tanzânia',
            'Togo',
            'Trinidad & Tobago',
            'Tunísia',
            'Turquia',
            'Ucrânia',
            'Uganda',
            'Uruguai',
            'Venezuela',
            'Vietnã',
            'Zaire',
            'Zâmbia',
            'Zimbábue'
        ]
    };
    
    if(!$stateParams.person && !$stateParams.type) $state.go('app.person.list');
    
    if(!$stateParams.person){
        if($stateParams.type === 'client'){
            PersonService.getClient($stateParams.id).then(function(client){
                _this.data.client = client;
            });
        } else if($stateParams.type === 'adverse'){
            PersonService.getAdverse($stateParams.id).then(function(adverse){
                _this.data.adverse = adverse;
            });
        } else if($stateParams.type === 'adverse_lawyer'){
            PersonService.getAdverseLawyer($stateParams.id).then(function(adverse_lawyer){
                _this.data.adverse_lawyer = adverse_lawyer;
            });
        } else if($stateParams.type === 'correspondent'){
            PersonService.getCorrespondent($stateParams.id).then(function(correspondent){
                _this.data.correspondent = correspondent;
            });
        }
    } else if($stateParams.person){
        if($stateParams.type === 'client') _this.data.client = $stateParams.person;
        else if($stateParams.type === 'adverse') _this.data.adverse = $stateParams.person;
        else if($stateParams.type === 'adverse_lawyer') _this.data.adverse_lawyer = $stateParams.person;
        else if($stateParams.type === 'correspondent') _this.data.correspondent = $stateParams.person;
    }
    
    
    this.updateClient = function(){
        var _data = angular.copy(_this.data.client);
        
        angular.forEach(_data, function(value, key){
            if(value === null) _data[key] = '';
        });
        
        PersonService.updateClient(_this.data.client.id, _data).then(function(client){
            $state.go('app.person.client', {id: client.id, client: client});
        });
    };
    
    this.updateAdverse = function(){
        var _data = angular.copy(_this.data.adverse);
    
        angular.forEach(_data, function(value, key){
            if(value === null) _data[key] = '';
        });
        
        PersonService.updateAdverse(_this.data.adverse.id, _data).then(function(adverse){
            $state.go('app.person.adverse', {id: adverse.id, adverse: adverse});
        });
    };
    
    this.updateAdverseLawyer = function(){
        var _data = angular.copy(_this.data.adverse_lawyer);
    
        angular.forEach(_data, function(value, key){
            if(value === null) _data[key] = '';
        });
        
        PersonService.updateAdverseLawyer(_this.data.adverse_lawyer.id, _data).then(function(adverse_lawyer){
            $state.go('app.person.adverselawyer', {id: adverse_lawyer.id, adverse_lawyer: adverse_lawyer});
        });
    };
    
    this.updateCorrespondent = function(){
        var _data = angular.copy(_this.data.correspondent);
    
        angular.forEach(_data, function(value, key){
            if(value === null) _data[key] = '';
        });
        
        PersonService.updateCorrespondent(_this.data.correspondent.id, _data).then(function(correspondent){
            $state.go('app.person.correspondent', {id: correspondent.id, correspondent: correspondent});
        });
    };
}]);
