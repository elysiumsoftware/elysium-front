elysium.controller('PersonListController',
    ['PersonService', '$state', 'ModalService', '$scope',
    function(PersonService, $state, ModalService, $scope)
{
    var _this = this;
    
    
    this.listFilters = {};
    
    PersonService.getEverybody().then(function(response){
        _this.personList = response;
    });
    
    $scope.$on('app:person:delete', function(event, person){
        _this.personList.splice(_this.personList.indexOf(person), 1);
    });
    
    
    this.openDetails = function(person){
        if(person.type === 'client') $state.go('app.person.client', {id: person.id});
        else if(person.type === 'adverse') $state.go('app.person.adverse', {id: person.id});
        else if(person.type === 'adverselawyer') $state.go('app.person.adverselawyer', {id: person.id});
        else if(person.type === 'correspondent') $state.go('app.person.correspondent', {id: person.id});
    };
    
    this.openDeleteModal = function(person){
        ModalService.setModal({
            locals: {
                _Data: {
                    person: person
                }
            },
            templateUrl: 'app/person/views/modal/delete.html',
            controller: 'DeletePersonModalController as DeletePersonModal',
            autoClose: true
        });
    }
}]);
