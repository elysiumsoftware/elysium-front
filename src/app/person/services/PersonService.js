elysium.service('PersonService', ['APIService', 'ToastService', '$q', 'ClientFactory', 'AdverseFactory', 'AdverseLawyerFactory', 'CorrespondentFactory', function(APIService, ToastService, $q, ClientFactory, AdverseFactory, AdverseLawyerFactory, CorrespondentFactory){
    this.getEverybody = function(){
        return APIService.GET('people').then(function(response){
            return $q.when(response.data);
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de pessoas', error);
            return $q.reject(error);
        });
    };
    
    /**
     * Client functions
     */

    this.getClient = function(id){
        return APIService.GET('people/clients/' + id).then(function(response){
            return new ClientFactory(response.data);
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar um cliente', error);
            return $q.reject(error);
        });
    };
    
    /**
     * Returns all clients
     * @return Array<ClientFactory>
     */
    this.getClients = function(){
        return APIService.GET('people/clients').then(function(response){
            var clientArray = [];
            
            angular.forEach(response.data, function(client){
                clientArray.push(new ClientFactory(client));
            });
            
            return clientArray;
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de clientes', error);
            return $q.reject(error);
        });
    };
    
    this.createClient = function(client){
        return APIService.POST('people/clients', client).then(function(response){
            return new ClientFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar um novo cliente', error);
            return $q.reject(error);
        });
    };
    
    this.updateClient = function(id, data){
        return APIService.PATCH('people/clients/' + id, data).then(function(response){
            return new ClientFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar um cliente', error);
            return $q.reject(error);
        });
    };
    
    this.deleteClient = function(client){
        return APIService.DELETE('people/clients/' + client.id).then(function(){
            return $q.when(client);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir um cliente', error);
            return $q.reject(error);
        });
    };
    
    
    this.getAdverse = function(id){
        return APIService.GET('people/adverses/' + id).then(function(response){
            return new AdverseFactory(response.data);
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar um adverso', error);
            return $q.reject(error);
        });
    };
    
    this.getAdverses = function(){
        return APIService.GET('people/adverses').then(function(response){
            var adverseArray = [];
            
            angular.forEach(response.data, function(adverse){
                adverseArray.push(new AdverseFactory(adverse));
            });
            
            return adverseArray;
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de clientes', error);
            return $q.reject(error);
        });
    };
    
    this.createAdverse = function(adverse){
        return APIService.POST('people/adverses', adverse).then(function(response){
            return new AdverseFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar um novo adverso', error);
            return $q.reject(error);
        });
    };
    
    this.updateAdverse = function(id, data){
        return APIService.PATCH('people/adverses/' + id, data).then(function(response){
            return new AdverseFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar um adverso', error);
            return $q.reject(error);
        });
    };
    
    this.deleteAdverse = function(adverse){
        return APIService.DELETE('people/adverses/' + adverse.id).then(function(){
            return $q.when(adverse);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir um adverso', error);
            return $q.reject(error);
        });
    };
    
    
    this.getAdverseLawyer = function(id){
        return APIService.GET('people/adverse-lawyers/' + id).then(function(response){
            return new AdverseLawyerFactory(response.data);
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar um advogado adverso', error);
            return $q.reject(error);
        });
    };
    
    this.getAdverseLawyers = function(){
        return APIService.GET('people/adverse-lawyers').then(function(response){
            var adverseArray = [];
        
            angular.forEach(response.data, function(adverse){
                adverseArray.push(new AdverseFactory(adverse));
            });
        
            return adverseArray;
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar a lista de clientes', error);
            return $q.reject(error);
        });
    };
    
    this.createAdverseLawyer = function(adverseLawyer){
        return APIService.POST('people/adverse-lawyers', adverseLawyer).then(function(response){
            return new AdverseLawyerFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar um novo advogado adverso', error);
            return $q.reject(error);
        });
    };
    
    this.updateAdverseLawyer = function(id, data){
        return APIService.PATCH('people/adverse-lawyers/' + id, data).then(function(response){
            return new AdverseLawyerFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar um advogado adverso', error);
            return $q.reject(error);
        });
    };
    
    this.deleteAdverseLawyer = function(adverse_lawyer){
        return APIService.DELETE('people/adverse-lawyers/' + adverse_lawyer.id).then(function(){
            return $q.when(adverse_lawyer);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir um advogado adverso', error);
            return $q.reject(error);
        });
    };
    
    
    this.getCorrespondent = function(id){
        return APIService.GET('people/correspondents/' + id).then(function(response){
            return new CorrespondentFactory(response.data);
        }, function(error){
            ToastService.showError('Algo deu errado ao carregar um correspondente', error);
            return $q.reject(error);
        });
    };
    
    this.createCorrespondent = function(correspondent){
        return APIService.POST('people/correspondents', correspondent).then(function(response){
            return new CorrespondentFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao criar um novo correspondente', error);
            return $q.reject(error);
        });
    };
    
    this.updateCorrespondent = function(id, data){
        return APIService.PATCH('people/correspondents/' + id, data).then(function(response){
            return new CorrespondentFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao atualizar um correspondente', error);
            return $q.reject(error);
        });
    };
    
    this.deleteCorrespondent = function(correspondent){
        return APIService.DELETE('people/correspondents/' + correspondent.id).then(function(){
            return $q.when(correspondent);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao excluir um correspondente', error);
            return $q.reject(error);
        });
    };
}]);
