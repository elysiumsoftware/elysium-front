elysium.factory('AdverseLawyerFactory', [function(){
    var AdverseLawyer = function(data){
        this.id = data.id;
        this.created_at = data.created_at;
        this.update_date = data.update_date;
        this.name = data.name;
        
        this.oab = data.oab || null;
        this.telephone = data.telephone || null;
        this.email = data.email || null;
        this.office_name = data.office_name || null;
        this.comments = data.comments || null;
    };
    
    
    return AdverseLawyer;
}]);
