elysium.factory('CorrespondentFactory', [function(){
    var Correspondent = function(data){
        this.id = data.id;
        this.created_at = data.created_at;
        this.update_date = data.update_date;
        this.name = data.name;
        
        this.role = data.role || null;
        this.fantasy_name = data.fantasy_name || null;
        this.cnpj = data.cnpj || null;
        this.telephone = data.telephone || null;
        this.email = data.email || null;
        this.site = data.site || null;
        this.street = data.street || null;
        this.number = data.number || null;
        this.complement = data.complement || null;
        this.neighbourhood = data.neighbourhood || null;
        this.city = data.city || null;
        this.state = data.state || null;
        this.country = data.country || null;
        this.cep = data.cep || null;
        this.comments = data.comments|| null;
    };
    
    
    return Correspondent;
}]);
