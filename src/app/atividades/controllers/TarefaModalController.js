/**
 * Created by lucas on 09/12/2016.
 */

elysium.controller('TarefaModalController', ['ModelService', 'uiCalendarConfig', '$rootScope', function(ModelService, uiCalendarConfig, $rootScope){
    var _this = this;
    
    _this.tarefa = null;
    _this.tarefaEdit = null;
    _this.usuarios = null;
    _this.processos = null;
    _this.recursos = null;
    _this.incidentes = null;
    _this.currentTab = 'detalhes';
    
    // Selectize configuration objects
    _this.usuariosConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name','email']
    };
    _this.processosConfig = {
        valueField: 'id',
        labelField: 'title',
        highlight: false,
        searchField: ['title'],
        maxItems: 1
    };
    _this.appealsConfig = {
        valueField: 'id',
        labelField: 'appeal_type',
        highlight: false,
        searchField: ['appeal_type'],
        maxItems: 1
    };
    _this.incidentsConfig = {
        valueField: 'id',
        labelField: 'incident_type',
        highlight: false,
        searchField: ['incident_type'],
        maxItems: 1
    };
    
    var emitUpdate = function(){
        if(uiCalendarConfig.calendars.atividades) uiCalendarConfig.calendars.atividades.fullCalendar('refetchEvents');
        $rootScope.$emit('atividades:atualizarTarefas');
    };
    
    
    /**
     * Initializes the Tarefa data
     */
    ModelService.getTarefa().then(function(tarefa){
        _this.tarefa = tarefa;
    });
    
    
    /**
     * Changes the current tab on the view
     * @param {string} tab - The tab to change to
     */
    _this.changeTab = function(tab){
        _this.currentTab = tab;
    };
    
    /**
     * Generates a Google Maps embed URL using the provided address or business place name
     * @param {string} local - An address or business place name
     * @returns {string} - An embeddable URL
     */
    _this.embedMap = function(local){
        return "https://www.google.com/maps/embed/v1/place?key=AIzaSyDTGplVSOkcPjQocnoVhVDXptRiD4h3A0s&q=" + local;
    };
    
    _this.prepareEdit = function(){
        // Loads data to build the required fields
        if(!_this.usuarios){
            ModelService.getUsuarios().then(function(usuarios){
                _this.usuarios = usuarios;
            })
        }
        if(!_this.processos){
            ModelService.getProcessos().then(function(processos){
                _this.processos = processos;
            })
        }
        ModelService.getIncidentes(_this.tarefa.process.id).then(function(incidentes){
            _this.incidentes = incidentes;
        });
        ModelService.getRecursos(_this.tarefa.process.id).then(function(recursos){
            _this.recursos = recursos;
        });
        
        // Initializes edit object
        _this.tarefaEdit = angular.copy(_this.tarefa);
        
        // Initializes responsaveis
        if(_this.tarefa.responsible.length){
            var _responsaveis = [];
            for(i = 0; i <  _this.tarefa.responsible.length;i++){
                _responsaveis.push(_this.tarefa.responsible[i].id);
            }
            _this.tarefaEdit.responsible = _responsaveis;
        } else _this.tarefaEdit.responsible = [];
        
        // Initializes processo, incidentes and recursos
        _this.tarefaEdit.process = _this.tarefa.process.id;
        _this.tarefaEdit.incident = _this.tarefa.incident ? [_this.tarefa.incident.id] : [];
        _this.tarefaEdit.appeal = _this.tarefa.appeal ? [_this.tarefa.appeal.id] : [];
        
        // Transition to edit tab
        _this.changeTab('editar');
    };
    
    _this.updateTarefa = function(){
        if(!_this.tarefaEdit.incident || !_this.tarefaEdit.incident.length) _this.tarefaEdit.incident = null;
        if(!_this.tarefaEdit.appeal || !_this.tarefaEdit.appeal.length) _this.tarefaEdit.appeal = null;
        if(!_this.tarefaEdit.responsible || !_this.tarefaEdit.responsible.length) _this.tarefaEdit.responsible = [ModelService.getItem('user:id')];
        if(!_this.tarefaEdit.priority) _this.tarefaEdit.priority = 'NORMAL';
        if(!_this.tarefaEdit.type) _this.tarefaEdit.type = 'Compromisso';
        if(_this.tarefaEdit.incident) _this.tarefaEdit.incident = parseInt(_this.tarefaEdit.incident);
        if(_this.tarefaEdit.appeal) _this.tarefaEdit.appeal = parseInt(_this.tarefaEdit.appeal);
        
        ModelService.updateTarefa(_this.tarefaEdit.id, _this.tarefaEdit).then(function(){
            ModelService.getTarefa(_this.tarefa.id).then(function(tarefa){
                _this.tarefa = tarefa;
                _this.changeTab('detalhes');
                $rootScope.$broadcast('atividade:tarefa:update');
            });
            emitUpdate();
        });
    };
    
    _this.deleteTarefa = function(){
        ModelService.deleteTarefa(_this.tarefa.id).then(function(){
            emitUpdate();
            $rootScope.hideModal();
        });
    };
    
    _this.changeStatus = function(status){
        ModelService.updateTarefa(_this.tarefa.id, {pending: status}).then(function(){
            ModelService.getTarefa(_this.tarefa.id).then(function(tarefa){
                _this.tarefa = tarefa;
                $rootScope.$broadcast('atividade:tarefa:update');
            });
            emitUpdate();
        });
    };
    
    _this.refreshProcesso = function(){
        _this.tarefaEdit.recurso = false;
        _this.tarefaEdit.incidente = false;
        ModelService.getIncidentes(_this.tarefaEdit.process).then(function(incidentes){
            _this.incidentes = incidentes;
        });
        ModelService.getRecursos(_this.tarefaEdit.process).then(function(recursos){
            _this.recursos = recursos;
        });
    };
}]);
