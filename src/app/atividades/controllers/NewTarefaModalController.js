/**
 * Created by lucas on 11/12/2016.
 */

elysium.controller('NewTarefaModalController', ['ModelService', 'uiCalendarConfig', '$rootScope', '$scope', '_Config', 'ToastService', function(ModelService, uiCalendarConfig, $rootScope, $scope, _Config, ToastService){
    var _this = this;
    
    _this.tarefa = {};
    if(_Config.date){
        console.log('rs');
        _this.tarefa.start = _Config.date.hours(9).toJSON();
        _this.tarefa.finish = _Config.date.hours(10).toJSON();
    } else{
        console.log('oi');
        _this.tarefa.start = moment().local().hours(9).minutes(0).add(1, 'day').toJSON();
        _this.tarefa.finish = moment().local().hours(10).minutes(0).add(1, 'day').toJSON();
    }
    
    
    if(_Config.processo){
        _this.tarefa.process = String(_Config.processo);
        ModelService.getIncidentes(_this.tarefa.process).then(function(incidentes){
            _this.incidentes = incidentes;
        });
        ModelService.getRecursos(_this.tarefa.process).then(function(recursos){
            _this.recursos = recursos;
        });
    }
    else _this.tarefa.process = [];
    
    _this.usuarios = null;
    _this.processos = null;
    _this.recursos = null;
    _this.incidentes = null;
    
    // Selectize configuration objects
    _this.usuariosConfig = {
        valueField: 'id',
        labelField: 'name',
        highlight: false,
        searchField: ['name','email']
    };
    _this.processosConfig = {
        valueField: 'id',
        labelField: 'title',
        highlight: false,
        searchField: ['title'],
        maxItems: 1
    };
    _this.appealsConfig = {
        valueField: 'id',
        labelField: 'appeal_type',
        highlight: false,
        searchField: ['appeal_type'],
        maxItems: 1
    };
    _this.incidentsConfig = {
        valueField: 'id',
        labelField: 'incident_type',
        highlight: false,
        searchField: ['incident_type'],
        maxItems: 1
    };
    
    var emitUpdate = function(){
        if(uiCalendarConfig.calendars.atividades) uiCalendarConfig.calendars.atividades.fullCalendar('refetchEvents');
        $rootScope.$emit('atividades:atualizarTarefas');
    };
    
    
    ModelService.getUsuarios().then(function(usuarios){
        _this.usuarios = usuarios;
    });
    ModelService.getProcessos().then(function(processos){
        _this.processos = processos;
    });
    
    
    _this.createTarefa = function(){
        if(!_this.tarefa.incident || !_this.tarefa.incident.length) delete _this.tarefa.incident;
        if(!_this.tarefa.appeal || !_this.tarefa.appeal.length) delete _this.tarefa.appeal;
        if(!_this.tarefa.responsible || !_this.tarefa.responsible.length) _this.tarefa.responsible = [ModelService.getItem('user:id')];
        if(!_this.tarefa.priority || !_this.tarefa.priority.length) _this.tarefa.priority = 'NORMAL';
        if(!_this.tarefa.type || !_this.tarefa.type.length) _this.tarefa.type = 'Compromisso';
        if(_this.tarefa.incident) _this.tarefa.incident = parseInt(_this.tarefa.incident);
        if(_this.tarefa.appeal) _this.tarefa.appeal = parseInt(_this.tarefa.appeal);
        _this.tarefa.pending = true;
        
        ModelService.createTarefa(_this.tarefa).then(function(tarefa){
            if(tarefa.responsible.indexOf(parseInt(ModelService.getItem('user:id'))) === -1){
                $rootScope.hideModal();
            } else{
                emitUpdate();
                $rootScope.$broadcast('atividade:tarefa:create');
                $rootScope.hideModal();
            }
        });
    };
    
    _this.refreshProcesso = function(){
        _this.tarefa.appeal = false;
        _this.tarefa.incident = false;
        _this.incidentes = null;
        _this.recursos = null;
        if(_this.tarefa.process){
            ModelService.getIncidentes(_this.tarefa.process).then(function(incidentes){
                _this.incidentes = incidentes;
            });
            ModelService.getRecursos(_this.tarefa.process).then(function(recursos){
                _this.recursos = recursos;
            });
        }
    };
}]);
