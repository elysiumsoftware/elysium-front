elysium.controller('AtividadesCtrl', ['ElysiumAPI', '$scope', '$state', '$modal', '$rootScope', 'uiCalendarConfig', 'ModalService', 'ModelService', function(ElysiumAPI, $scope, $state, $modal, $rootScope, uiCalendarConfig, ModalService, ModelService){
	var _this = this;

	// Retorna a função que carrega os eventos no calendário
	_this.eventos = [{
        events: function(start, end, timezone, callback) {
            ElysiumAPI.get("tasks/range/"+start.toISOString()+"/"+end.toISOString(), 'process', null, true).then(function(response){
                _this.dados = response.data;
                var eventosArray = [];
                for(var i = 0; i < _this.dados.length; i++){
                    var prioridadeClass = [];
                    switch(_this.dados[i].priority){
                        case 'LOW':
                            prioridadeClass.push("agenda-prioridade-baixa"); break;
                        case 'NORMAL':
                            prioridadeClass.push("agenda-prioridade-normal"); break;
                        case 'HIGH':
                            prioridadeClass.push("agenda-prioridade-alta"); break;
                        default:
                            break;
                    }
                    if(!_this.dados[i].pending) prioridadeClass.push('agenda-situacao-concluido');
                    var evento = {
                        title: _this.dados[i].title,
                        start: _this.dados[i].start,
                        end: _this.dados[i].finish,
                        id: _this.dados[i].id,
                        situacao: _this.dados[i].pending,
                        className: prioridadeClass
                    };
                    eventosArray.push(evento);
                }
                callback(eventosArray);
            },function(response){
                ElysiumAPI.showError('Algo deu errado ao carregar suas atividades', response);
            });
        }
    }];

	// Configurações e funções do calendário
	_this.uiConfig = {
		calendar:{
			lang: 'pt-br',
			defaultView: 'month',
			timezone: 'local',
			editable: false,
			theme: false,
			eventLimit: 5,
			fixedWeekCount: false,
			eventOrder: '-title',
			header:{
				left: 'prev',
				center: 'title',
				right: 'today next'
			},
			eventClick: function(event, jsEvent){
                ModelService.setTarefa(event.id);
                ModalService.setModal({title: event.title, content: '', templateUrl: 'templates/modal/atividades-tarefa.html'});
			},
			dayClick: function(date, jsEvent){
				$scope.carregarDependecias();
				_this.novaTarefa = {
					start: date.local().hours(9).toJSON(),
					finish: date.local().hours(10).toJSON()
				};
                ModalService.setModal({locals: {_Config: {date: date.local()}}, title: 'Nova Tarefa', content: '', backdrop: 'static', controller: 'NewTarefaModalController as NewTarefa', templateUrl: 'templates/modal/atividades-adicionar-tarefa.html'});
			}
		}
	};
	
	$scope.novaTarefa = {};

	// O nome da view padrão
	_this.view = 'month';
    
	// Muda a View atual do calendário
	$scope.changeView = function(viewName){
		_this.view = viewName;
		uiCalendarConfig.calendars.atividades.fullCalendar('changeView', viewName);
	};
	
	
	

	// Carrega as dependências para a criação/edição de tarefas
	$scope.carregarDependecias = function(){
		_this.incidentesDoProcesso = [];
		_this.recursosDoProcesso = [];
		if(!_this.usuarios){
			ElysiumAPI.getUrl('users/users/?all=true').then(function(response){
		        _this.usuarios = response.data;
			},function(response){
				ElysiumAPI.showError('Algo deu errado ao carregar a lista de usuários', response);
			});
		}
		if(!_this.processos){
			ElysiumAPI.getUrl('processes/').then(function(response){
		        _this.processos = response.data;
			},function(response){
				ElysiumAPI.showError('Algo deu errado ao carregar a lista de processos', response);
			});
		}
	};

	// Carrega os recursos e incidentes do processo selecionado na criação de tarefas
	$scope.carregarRecursosEIncidentes = function(view){
		// Esvazia as listas de incidentes e recursos
		_this.incidentesDoProcesso = [];
		_this.recursosDoProcesso = [];

		// View é o nome da view ou o id do processo a ser carregado (default)
		switch (view) {
			case "edicao":
				if(!_this.editarTarefa.process) return;
				_this.editarTarefa.incident = false;
				_this.editarTarefa.appeal = false;
				var processoId = _this.editarTarefa.process;
				break;
			case "adicao":
				if(!_this.novaTarefa.process) return;
				_this.novaTarefa.incident = false;
				_this.novaTarefa.appeal = false;
				var processoId = _this.novaTarefa.process;
				break;
			default:
				processoId = view;
				break;
		}

		ElysiumAPI.get('processes/'+ processoId + '/incidents', 'process').then(function(response){
			_this.incidentesDoProcesso = response.data;
		},function(response){
			ElysiumAPI.showError('Algo deu errado ao carregar a lista de incidentes', response);
		});
		ElysiumAPI.get('processes/'+ processoId + '/appeals', 'process').then(function(response){
			_this.recursosDoProcesso = response.data;
		},function(response){
			ElysiumAPI.showError('Algo deu errado ao carregar a lista de recursos', response);
		});
	};

	// Abre o modal de adição de tarefas com o dia de hoje
	$scope.prepararAdicao = function(){
		$scope.carregarDependecias();
		_this.novaTarefa = {
			data_de_inicio: moment().local().add(1,'d').hours(9).minutes(0).seconds(0).milliseconds(0).toJSON(),
			data_de_termino: moment().local().add(1,'d').hours(10).minutes(0).seconds(0).milliseconds(0).toJSON()
		};
        ModalService.setModal({locals: {_Config: {}}, title: 'Nova Tarefa', content: '', backdrop: 'static', controller: 'NewTarefaModalController as NewTarefa', templateUrl: 'templates/modal/atividades-adicionar-tarefa.html'});
	};

	// Cria uma tarefa
	$scope.criarTarefa = function(){
		if(!_this.novaTarefa.prioridade.length) _this.novaTarefa.prioridade = 'NORMAL';
		if(!_this.novaTarefa.recurso) _this.novaTarefa.recurso = null;
		if(!_this.novaTarefa.incidente) _this.novaTarefa.incidente = null;

		if(!_this.novaTarefa.responsaveis.length) _this.novaTarefa.responsaveis = [ModelService.getItem('user:id')];

		if(!_this.novaTarefa.tipo.length) _this.novaTarefa.tipo = "Compromisso";

		var dados = {
			title: _this.novaTarefa.assunto,
			type: _this.novaTarefa.tipo,
			start: _this.novaTarefa.data_de_inicio,
			finish: _this.novaTarefa.data_de_termino,
			location: _this.novaTarefa.local,
			priority: _this.novaTarefa.prioridade,
			description: _this.novaTarefa.descricao,
			responsible: _this.novaTarefa.responsaveis,
			process: _this.novaTarefa.processo,
			appeal: _this.novaTarefa.recurso,
			incident: _this.novaTarefa.incidente,
			pending: true
		};

		ElysiumAPI.post("tasks", 'process', dados).then(function(response){
			$scope.novaTarefaModal.hide();
			uiCalendarConfig.calendars.atividades.fullCalendar('refetchEvents');
			$rootScope.$emit('atividades:atualizarTarefas');
		}, function(response){
			ElysiumAPI.showError('Algo deu errado ao criar essa tarefa', response)
		});
	};
       
}]);
