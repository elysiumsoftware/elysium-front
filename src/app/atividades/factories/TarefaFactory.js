elysium.factory('TarefaFactory', [function(){
    var Tarefa = function(data){
        this.id = data.id;
        this.assunto = data.title;
        this.creation_date = new Date(data.created_at);
        this.update_date = new Date(data.update_date);
        this.data_de_inicio = new Date(data.start);
        this.data_de_termino = new Date(data.finish);
        this.processo = data.process;
        this.responsaveis = data.responsible;
        this.incidente = data.incident;
        this.recurso = data.appeal;
        this.is_public = data.is_public;
        
        this.descricao = data.description || null;
        this.local = data.location || null;
        this.prioridade = data.priority || null;
        this.situacao = data.pending || null;
        this.tipo = data.type || null;
        
        // Sets is_pending
        this.is_concluded = !!data.pending;
    };
    
    
    Tarefa.prototype.isPending = function(){
        return this.situacao;
    };
    
    
    return Tarefa;
}]);
