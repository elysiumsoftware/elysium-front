elysium.service('TarefaService', ['APIService', '$q', 'ToastService', 'TarefaFactory', function(APIService, $q, ToastService, TarefaFactory){
    var _this = this;
    
    
    _this.getTarefa = function(id){
        return APIService.GET('tarefas/tarefas/' + id).then(function(response){
            return new TarefaFactory(response.data);
        }).catch(function(error){
            ToastService.showError('Algo deu errado ao carregar uma tarefa.', error);
            return $q.reject();
        });
    }
}]);
